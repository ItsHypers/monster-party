#it #Kurusu Route

label kurusu:


mc "Kurusu deve estar na sala!"

"Segurando a minha mochila pelos ombros, corri até a sala do Kurusu. Quando cheguei, ele estava terminando de arrumar seu material."

mc "Kukki~!"

"Eu o chamei, acenando."
show k normal 
k "[povname]."

"Ele se virou, com um sorrisinho."

k "Que bom te ver. Ainda não foi pra casa?"

mc "Não... Na verdade estava esperando por você."

"Eu dou um sorrisinho me apoiando na porta, esperando ele terminar de se aprontar."

k "Por mim? Mas que honra. *ri*"

mc "Para. *ri* Eu queria passar um tempo com você~"

mc "Isso é, se não for incômodo..."

k "Não, não é incômodo."

"Kurusu passou o braço pelo meu pescoço, ainda sorrindo."

k "Então que tal irmos para o pátio? Com certeza não vão nos incomodar lá."

mc "Ooh~ O seu cantinho secreto? Vamos~!"

"Kurusu tira o braço do meu ombro, deixando a área com uma sensação de vazio."

"Nós nos dirigimos pelo pátio. O intervalo sempre junta os alunos em todas as áreas do colégio."

"Passando pelo ginásio, conseguimos ouvir o grito de vários alunos"

"O time de basquete deve estar jogando."

k "Heh. Deve ser divertido..."

mc "O que, Kukki?"

k "Jogar... Praticar esportes com o pessoal no ginásio."

"Kurusu suspira e, ao ver a minha expressão de preocupação, solta um risinho"

k "Não é nada, [povname]. Não precisa se preocupar tanto~"

mc "Eu não disse nada~"

"Ele tem o costume de se preocupar demais comigo e não deixar que eu faça o mesmo."

"É um pouco frustrante, mas não quero abusar"

"Ele já me deixa ter contato com ele, e se ele estiver bem com isso, já é o suficiente pra mim."

"Chegando numa clareira, Kurusu sinaliza para um caminho semi-morto no meio dos arbustos."

"Ele me disse que as vezes que vem pra cá, acaba sem querer machucando um ou dois ramos..."

"Enquanto ele entra no arbusto, eu faço um carinho gentil na parte seca. Ele parece notar, mas não dá muita bola."

"Passando pelo arbusto, ele dá lugar para um caminho fechado com algumas flores mistas desabrochando."

mc "Eu sempre fico impressionada com esse lugar... É muito lindo."

"Eu sorrio, apreciando a beleza do lugar. É quieto demais para um colégio."

k "Minhas melhores sonecas foram aqui, é um lugar muito bom pra dormir..."

k "Embora as flores tenham um cheiro um pouco forte pro meu gosto."

"Eu me abaixo pra sentir o aroma de uma das flores. Ela é pequena e, embora grande parte delas sejam brancas, haviam algumas roxas, rosas e até algumas laranjas por perto. É doce, tipo mel."

k "Álisso."

"Kurusu sorri, sentando no chão e observando a cena."

"Ele tem essa mania de prestar atenção a detalhes. Sempre observando, sempre quieto. Sempre admirando."

"Rubor cobre as minhas bochechas, vendo como ele me encara dessa forma. Tão bobo~"

mc "São lindas..."

k "São sim, mas o cheiro..."

mc "Eu não acho... É bem agradável na verdade~"

k "...Pena eu não poder tocar muito nelas."

"Eu me aproximo dele, olhando para suas mãos."

mc "Você se refere ao..."

k "Ao sangue da minha mãe, sim."

"Oh. Kurusu é filho da Senhora Morte. Ela mata tudo o que toca, e ele como herdeiro..."

mc "Você andava treinando controlar isso, não estava? Como tá indo? Alguma melhora?"

k "Eu... Consigo tocar algumas coisas antes de apodrecer elas... Mas são breves segundos."

"Kurusu suspira, levantando e pegando uma Álisso do chão."

#CG do Kurusu segurando uma flor e mostrando pra MC, dando fade pra uma segunda CG onde a flor tá apodrecida TADINHO DO MEU NENÉM </3

mc "Kurusu..."

"Kurusu suspira novamente e senta do meu lado, fazendo carinho em uma das minhas mechas de cabelo."

k "Tá tudo bem [povname]. Um dia eu terei controle dessa situação, e você será a primeira a sentir o meu abraço apertado."

"Eu sorrio, abraçando ele por alguns instantes."

mc "Hihi~ Eu vou estar mais que honrada em aceitar Kukki."

k "Huhu~"

"Eu desfaço o abraço, me ajeitando na grama. Ele parece um pouco nervoso com o que eu fiz, mas isso é normal..."

k "Então, como vai a busca pelo par ideal?"

"Eu consigo ouvir ele apertando o tecido da roupa, evitando olhar nos meus olhos."

mc "Você não vai mesmo pensar em ir comigo, né?..."

k "Digamos que eu prefiro ser sua última opção, não a primeira."

"Kurusu sorri, ainda evitando de me olhar diretamente."

"Ele deve estar nervoso com o assunto, por ter mencionado sem que eu tocasse no assunto..."

"Eu lembro de ter combinado com as meninas de começar o plano hoje, ele deve ter ouvido isso"

"Mas acho que ele não espera que eu tenha escolhido ele"

"Ele é o meu melhor amigo, não teria graça ir sem ele."

mc "Eu gosto de você, Kukki. Gostaria muito que fosse... Pensa um pouquinho nisso vai, por mim..."

"A minha pressão psicológica deve funcionar nele, já que eu consigo ver as orelhas dele ganhando cor."

mc "Awn. Tá vermelhinho é?~"

k "Cala a boca. Não foi nada..."

"Ele vira o rosto tentando esconder a vermelhidez, mas a sua pele é tão pálida que a cor preenche o lugar inexistente da melanina."

k "Eu... Eu vou pensar, okay? Mas não prometo nada..."

mc "Já é um começo. Obrigada."

k "De nada."

"Eu suspiro, me jogando de costas na grama e olhando pro céu."

"A calmaria e o céu claro e livre de nuvens me fazem perceber o sono que eu estou, e um bocejo acaba saindo."

mc "H-hnng~ Acho que vou tirar um cochilo. Me acompanha?"

"Kurusu deita com mais gentileza, passando os braços pela parte de trás da cabeça."

k "Claro~"

"Aos poucos dá pra se perceber os pássaros assobiando uns pros outros, borboletas passando de uma árvore pra outra."

"A serenidade que esse lugar transmite parece vinda de outra dimensão... Fico feliz do Kurusu ter um lugar só pra ele."

mc "Hihi... Tão tranquilo~"

"Kurusu cobre os olhos com o braço, escurecendo a sua visão."

"Me virando na direção dele eu percebo o quão quieto Kurusu é. Ele sempre cuida de mim, tenta se abrir pros meus amigos e é gentil com as coisas."

"Eu queria passar mais tempo com ele... A correria da vida adulta logo vai começar."

"E o meu medo de não ter mais ele do meu lado..."

"Eu não quero nem pensar nisso."

k "Eu consigo ouvir os seus pensamentos, [povname]. Descansa um pouquinho."

"Kurusu diz, imóvel. Obediente, eu coloco as mãos na minha barriga, aos poucos esvaziando os meus pensamentos..."

"...Até cair no sono."

"Kurusu ao ouvir a minha respiração pesada, tira o braço do rosto."

"Ele se apoia em um braço, me admirando."

#CG de ambos deitados na grama, Kurusu olhando para a MC enquanto ela dorme gentilmente q gay aaaa

k "Bom descanso, anjinho..."

"Ele dá um beijinho rápido na minha testa, virando para cair lentamente no sono junto comigo."
hide k normal

"..."

"Eu acordo, Kurusu me olhando com o seu sorriso de sempre."

"Me levanto e estico o corpo, já soltando um bocejo longo."

mc "Y-yaawn~! Bom dia Kukki!"

show k normal 
"Kurusu ri, sentando e cruzando as pernas."

k "Bom dia [povname]. Descansou bem?"

mc "Parece que eu dormi por dois dias!"

"Kurusu levanta, botando a mochila nas costas."

k "Então... Vamos?"

mc "Vamos! Ainda não comi nada, e acho que o sinal não bateu."

k "Não, não bateu. E provavelmente a Skelly já deve ter enlouquecido por não encontrar a gente lá."

mc "É... Ela é um pouco fora da casinha."

"Nós dois rimos, imaginando como ela reagiria se nós não aparecessemos durante o dia"

"Ainda mais sabendo que eu decidi fazer com que o Kurusu me acompanhasse na festa."

k "Ela parecia bem animada pra te ajudar nesse seu plano."

mc "Sim... Eu preciso por ele em prática logo~"

k "...Boa sorte com isso."

mc "Obrigada, vou precisar."

"Eu dou um risinho, Kurusu enfiando as mãos nos bolsos da calça."

"Nós fazemos o caminho de volta pro prédio do colégio e eventualmente nós voltamos para as nossas salas de aula separadas."

"Chegando em casa eu jogo a minha mochila em cima da cama e deito nela."

mc "Uff! Que cansaço~"
hide k normal

"Eu pego o meu celular e mando um oi para o nosso grupinho secreto. Skelly criou porque queria poder reclamar do namorado sem culpa."

#Segue animação de texto onde elas perguntam como foi o dia e etc foda-se

"Hora de dormir. Amanhã vai ser um longo dia!"


"Dia 2 - ultimo dia"


































































