﻿define a1 = Character("Aluna", image="a1", color="#c8ffc8")
define a2 = Character("Aluno", image="a2", color="#c8ffc8")
define st = Character("Professor Stein", image="st", color="#c8ffc8")
define sk = Character("Skelly", image="sk", color="#c8ffc8")
define me = Character("Merrii", image="me", color="#c8ffc8")
define k = Character("Kurusu", image="k", color="#c8ffc8")
define mc = Character("[povname]", image="mc", color="#c8ffc8")
define ky = Character("Kyte", image="ky", color="#c8ffc8")
define a = Character("Anorak", image="a", color="#c8ffc8")
define who = Character("Aluno desconhecido", image="who", color="#c8ffc8")

image room1 = "room1.png"

default gentil = False
default rude = False

label start:


"Olá, bem vinde ao Monster Party. Essa é uma DEMO e conta com 2 mini-rotas. O jogo ainda está em desenvolvimento, críticas e feedback são muito bem vindos."
"Por favor, comece dando um nome para a MC."
"Deixe em branco se quiser o nome Default."


python:
    povname = renpy.input("Qual é o seu nome?")
    povname = povname.strip()

    if not povname:
         povname = "Hana"

"Seu nome é [povname], aproveite essa DEMO."

"~PRÓLOGO~"

"Manhã de Segunda, primeiro período."

show st normal

st "Certo, todos sentados? Então, como vocês sabem o ano letivo está para chegar ao fim, e a escola Lt. Sandalwood vai sediar o nosso baile anual."

hide st normal

"Stein começou a andar desengonçado e lentamente pela sala..."

"Os alunos o acompanhavam com os olhos enquanto ele continuava a falar."

"Alguns alunos conseguem lidar bem com a presença do professor na sala"

"Mas outros... Eu tenho certeza que nem tanto."

"Professor Stein não é exatamente uma pessoa ruim, ou assustadora..."

"Mas o seu jeito desengonçado, a sua altura... Tudo é um pouquinho intimidante."

show st normal

st "Vocês já tem ideia de par? É um evento muito importante, pois fecha o ciclo escolar de vocês."

"Ah! Realmente, o final do ano tá proximo..."
 
a2 "Eh, nem sei se vou conseguir mesmo sair daqui, professor."

st "Calma... As provas finais não são tão ruins assim!"

a1 "Pra você é fácil, você quem fez."

st "U-Uh, não é bem assim! Olha, o importante é sempre revisar e-"

mc "Hm..."

"Eu sempre acabo divagando nas aulas do professor Stein, e agora que ele mencionou o baile..."

"Eu não lembrei que precisava de um par."
 
"A Skelly provavelmente vai com o namorado, a Merrii já deve ter alguém. E o Kurusu não vai querer ir á festa..."
 
"Não quero ir sozinha. Preciso encontrar alguém logo."

mc "Mas é complicado, não tenho interesse em ninguém além dos meus amigos... Ugh."

st "[povname]?"

mc "Eu podia pedir pra Skelly me arranjar um par, mas eu não quero depender dela pra isso."

st "[povname], tudo bem?"

mc "E se eu conseguisse convencer o Kurusu a vir comigo? O problema é que ele não gosta desse tipo de socialização... O que eu vou fazer?"

st "[povname]."

mc "Na verdade ele não gosta de nenhum tipo de socialização. ha... ha..."

"Stein se aproximou, me dando um peteleco na testa; o devaneio no qual estava perdida — e aparentemente falando sozinha — se dissipando, dando lugar à dor incômoda na minha cabeça por causa do peteleco que me assustou."

mc "!!"

st "[povname]!"

"Stein elevou sua voz, colocando uma mão na cintura, me observando."

mc "A-AI! O que foi, p-professor Stein!?"

st "Eu estava falando com você. Não prestou atenção?"
 
mc "Desculpa, me perdi um pouco..."

"Stein sorriu, fazendo meu coração dar uma batida mais forte."

"Embora seja um sorriso um pouco estranho... É genuíno."
 
st "Não fique pra baixo. Hoje é o seu dia de arrumar a classe, não esquece ok? Traz a chave para mim quando terminar, vou estar na sala dos professores."

mc "C-certo."
hide st normal with dissolve

"Eu preciso me organizar logo, duvido que tenha alguém sem par até esse ponto."

"O sinal para o intervalo bateu, é melhor eu me preparar pra descer."

"O pessoal já deve estar indo pra cafeteria, os corredores ficavam mais espaçosos e vazios por conta disso..."

"Enquanto passava pelo corredor, passei pela porta da sala de aula do Kurusu."

"Dei uma espiadinha pela sala."

mc "Ué. Cadê ele? Já foi embora...?"

#CG do Kurusu se materializando atrás dela, com uma mão esqueleto tocando o ombro dela
show k normal with dissolve
k "Me procurando?"

"Um arrepio se espalhou em minha espinha sentindo algo presente apoiando-se em meu ombro, me virando para trás."

mc "Ah! Kukki!"  #with punch

"Eu solto um suspiro, dando um sorrisinho sem graça."

mc "N-não faz mais isso!"

"Kurusu sorri, tirando a mão do meu ombro e cruzando os braços enquanto ria do meu susto."

k "Desculpa, [povname]... O que você queria?"

mc "A-ahm.... Porque não vem almoçar comigo na lanchonete?"
 
k "Tudo bem. Esqueceu o dinheiro de novo?"

"Isso é vergonhoso. Eu esqueci o dinheiro pro almoço tantas vezes assim?"

"Tenho que prestar mais atenção no que faço durante o dia."

mc "N-não, seu idiota. Só queria passar um tempo com você."

k "Certo, certo..."

mc "Mas se quiser me comprar um bolinho, eu aceito!"

"Com um sorriso bobo no rosto, Kurusu deu um tapa leve nas minhas costas, brincando."

k "Porque você ~só quer passar um tempo comigo~, certo?"

"Eu gosto da amizade que temos. Desde pequenos, Kurusu é gentil comigo. Eu espero que isso nunca mude..."

"Ele sempre gostou de me mostrar bichinhos que encontrava. Lagartinhas, besourinhos, borboletas de todas as cores... Mas nunca pôde tocar, por causa dos poderes da sua mãe."

"Quando percebi que estava absorvida em pensamentos novamente, já tínhamos chegado na lanchonete."
   
"- Na lanchonete -"

"Kurusu me dá um toque no braço, direcionando meu corpo para dar um passo para frente. Olho para ele e o mesmo aponta para uma mesa vazia da lanchonete."

k "Vá se sentar. Eu pego o almoço e o seu bolinho, me espere lá."

mc "Oh. Obrigada Kukki!"

"Ele dá um ~tchauzinho~ com a mão, virando-se em direção á lanchonete."

hide k normal with dissolve

"Eu sorrio, observando ele fazer o trajeto."

"Kurusu sempre foi mais aberto comigo, isso me deixa feliz."

"Ele não brincava com as outras crianças porque tinha medo de tocar nelas, mas nunca foi assim comigo. Eu nunca soube o por quê. Talvez por me conhecer melhor? Por se sentir mais confortável?"

"Saber que eu entendia o problema dele? Eu imagino que sim."

"Nossos pais sempre foram muito próximos, e quando o humano da Morte se foi, minha mãe fez de tudo para ajudá-la..."

"O que resultou com Kurusu morando conosco por um tempo, por conta do trabalho da mãe de Kurusu."

"Foi bem divertido, ficávamos até tarde brincando e lendo quadrinhos."

"Claro que ele evita me tocar por muito tempo, mas ele gosta de ser meu amigo. E isso é o mais importante agora."

"Só queria que ele perdesse um pouco dessa tensão quando o assunto é socializar. Ir no baile vai ser uma ótima oportunidade pra gente passar um tempo
se divertindo."

"Eu entendo que ele é naturalmente tímido, mas..."

"Kurusu voltou com as duas bandejas, uma delas com o bolinho que ele havia prometido mais cedo."

show k normal with dissolve

k "Perdida nos pensamentos de novo, [povname]?"

"A timidez dele é um dos charminhos que o deixam único."

mc "Mais ou menos. As provas chegando sempre me deixam nervosa..."

k "Qualquer coisa você pode fazer as recuperações, não se preocupa."

mc "Prefiro não precisar delas..."

"Ele olha para mim com um sorriso de canto nos lábios."

k "Você vai conseguir, [povname]."

"Logo muda o foco para o seu sanduíche, mordendo com grande vontade."

"Eu me ajeito na cadeira, limpando a garganta levemente."

mc "Então, sobre o baile..."

"Kurusu arqueou uma sobrancelha e respirou fundo, revirando os olhos e mantendo-se a mastigar o alimento."

k  "Não vou."

mc "Ah, qual é! Por que não?"

k "Sabe que não gosto de multidões..."

mc "Mas gosta de mim, não gosta?"

"Kurusu ri, apoiando o rosto em uma das mãos. O pão segurado firmemente na outra mão com uma marca de mordida no meio."

k "Será que gosto?"

"Eu apoio o cotovelo na mesa imitando a pose do Kurusu, brincando com ele."

mc "Claro que eu gosto da [povname]! Eu prometi que ia casar com ela quando nós éramos crianças, e eu gosto de manter as minhas promessas." 

"Eu digo, e ele assiste a atuação com uma expressão tediosa mas ainda com um sorriso."

k "Você falando assim talvez eu acredite. Repete, vai que dá certo dessa vez."

mc "E ainda pedi para a lua ser nossa testemunha. Nunca vou esquecer aquela noite..."

"Kurusu ri baixinho com um leve rubor em suas bochechas."

k "Uhum. E o que mais? Também pedi pro Sol abençoar nosso futuro? É por isso que você não larga do meu pé assim?"

"Eu dou um sorrisinho, voltando a me sentar ereta no banquinho da cantina."
 
mc "Para, eu tô falando sério... Eu não quero ir sozinha Kukki, por favor!"
 
"Kurusu encolhe e desvia o olhar, corando nas extremidades das bochechas. Ele segurou o sanduíche com as duas mãos e o encarou por um momento."

k "Hum... Eu vou pensar a respeito."

mc "Obrigada, você é o melhor!"

"Kurusu deu mais uma mordida no sanduíche, o rubor desaparecendo aos pouco do seu rosto, voltando ao tom normal da sua pele cinzenta."

"Skelly e o namorado sentam na mesa, Kyte acenou e Skelly deu um beijo rápido na bochecha da [povname]."

hide k normal
show ky normal
show k normal at right
show sk normal at left


sk "Oi! Tudo bem, Kukki?"

k "Olá."

mc "Oi Skelly, oi Kyte!"

ky "Falaê. De boa?"

mc "Yep!"

sk "Então, baile! Você já foi convidada? Já sabe com que vestido ir?" 

mc "Não..."

sk "Ah, mas isso é ótimo. Chama a Merrii, nós vamos pra sua casa e te ajudamos com isso. Aposto que não irá se arrepender."

mc "Ah? Okay, mas sabe que a minha mãe não gosta de visitas surpresa, então se comporta."

sk "Ela já nos conhece. Não tem problema, né?"

"Kurusu suspira e coloca o capuz, terminando de comer o sanduíche."

mc "Não tem não... Então, você vai com o Kyte?"

sk "Claro! Meu amorzinho e eu adoramos dançar!"

"Skelly dá um beijinho na bochecha do Kyte, passando um braço pelo braço dele. Ele reage coçando a nuca com vergonha."

ky "Eu já curto mais pela comida mesmo."

"Skelly deu um tapinha no braço dele, rindo enquanto aperta o namorado."

sk "Ai, para. Eu sei que você gosta de sair comigo."

ky "Você sabe que sim, é divertido!"

"Kurusu solta uma risada baixa, me olhando e desviando o olhar para a mesa em seguida."

mc "Ah sim, eu convidei o Kurusu pro baile..."

sk "O Kukki? E você vai?"

"Skelly muda de posição, sentando de frente pro Kurusu. Os dois se encararam por alguns segundos."

"Ele se encolhe, mantendo uma certa distância, respondendo baixo."

k "Eu não sei ainda. Sabe como é..."

mc "Ai, eu não quero acabar tendo que ir sozinha~"

sk "Nem precisa! Merrii e eu arrumamos um par pra você."

k "H-hm."

"Kurusu se mexe na cadeira, desconfortável com o rumo que a conversa está seguindo."

mc "Mas eu não quero depender de vocês. Consigo me virar."

ky "Se eu fosse você, confiava na Skelly... Digo, ela tem bom gosto né. Acabou me escolhendo."

"Kyte ri, fazendo Skelly revirar os olhos com o comentário. Ela não consegue segurar uma risadinha, dando um tapinha de leve no braço do namorado."

sk "Cala a boca, bobão. Vamos [povname], vai ser divertido."

mc "Tá, tá."

"O sinal bate a tempo de eu terminar o meu almoço. O tempo passou rápido desta vez, devo ter me distraído enquanto conversava com eles."

sk "Então [povname], me espera na entrada do colégio. Vamos pegar a Merrii e ir pra sua casa."

ky "E eu não posso ir não?"

sk "Não, você vai pra casa. Precisa estudar pra sua prova, lembra?"

ky "Ah, é..."

ky "Kurusu, quer jogar hoje?"

k "Claro."

"Skelly revirou os olhos, vendo que seu namorado parecia ~extremamente~ atento às provas que viriam."

sk "Então, eu preciso ir. Tenho aula de Rituais agora e não posso entrar atrasada. Até!"

mc "Tchauzinho!"

k "Bye."

ky "Até mais, chuchu!"

hide k normal
hide ky normal
hide sk normal

"Nos levantamos das cadeiras e cada um segue em direção á sua sala."

"Chegando no corredor, alguém acaba esbarrando em mim. Meu corpo cai no chão e eu acabo ficando desnorteada."

mc "A-ai..! Cuidado!" #with vpunch 
show who normal
who "O-oh... Desculpa, você está bem?"

"Ele estende uma mão, me ajudando a levantar"

"Eu percebo que ele está todo enfaixado... Não deve ser por acidente, então ele deve ser uma múmia."

mc "Obrigada... Você se machucou?"

who "Não, eu... A aula!"
hide who normal

"Ele solta a mão que segurava a minha e corre em direção ás salas. Deve ser aula de Rituais também..."

mc "Heheh. Espero que ele chegue a tempo."

"- Sala de aula -"    
show st normal
st "Não se esqueçam de fazer um resumo da matéria de hoje. Amanhã temos um teste e esse conteúdo vai cair. Podem ir, bom descanso pra vocês."
hide st normal
"Os alunos começam a sair aos poucos, alguns em grupos e outros sozinhos. E os murmúrios de conversas se afastavam com o tempo."

"Quando terminei de arrumar minhas coisas, O professor Stein me chamou a atenção."
show st normal
st "[povname]? Você se esqueceu do que eu pedi hoje mais cedo?"

mc "Eh? Ah, é verdade! Desculpa professor, eu ando com a cabeça muito ocupada."

st "Pensando em meninos, hmm? Na sua idade eu também era assim."

"Ele se apoia em uma das mesas, dando um sorrisinho, me observando terminar de organizar o material."

mc "Ah? Não! Não é isso não! Eu só.... Espera, você também era assim?"

st "É... Eu sempre fui meio careta. Embora eu tenha dado tudo de mim, meus relacionamentos não duraram muito."

"Ele ri baixinho, cruzando os braços e fazendo um pequeno ~não~ com a cabeça"

mc "Ah, sinto muito por isso. Mas não é por esse motivo que eu estou distraída."

st "Certo... Então se precisar conversar já sabe, pode chamar o Stein sem problemas~"

mc "Hihih~ Pode deixar professor Stein~"

"Stein levanta da mesa, passando as mãos levemente no jaleco, ajeitando."

st "Bom, eu vou pra sala checar se está tudo preparado para a prova de amanhã. Você vai ficar bem?"

"Eu dou um sorrisinho, arrastando minha cadeira para perto da mesa."

mc "Vou ficar sim, não se preocupa! Logo mais eu te entrego a chave."

st "Certo... Até, [povname]."
hide st normal

"Stein sai da sala e eu fico sozinha enquanto arrumava as coisas"

"Era estranho quando os alunos não estavam presentes, mas também parecia que eu possuía uma sala inteira para mim."

mc "Droga... Eu marquei de encontrar as meninas agora."

mc "Vou mandar uma mensagem pra Skelly."
 
"Eu envio uma mensagem no nosso grupo pessoal, avisando que eu havia ficado responsável pela limpeza de hoje."

"Elas respondem dizendo que vão vir me ajudar com a sala, assim voltamos mais cedo para casa."

mc "Certo, elas devem chegar logo..."
   
"Após alguns minutos elas chegam, e nós nos dividimos entre as tarefas restantes."

"Terminamos em questão de minutos, e logo estávamos prontas para ir para casa."
show me normal at right
show sk normal at left

me "Uuf! Tudo pronto então?"

sk "Até que não demorou tanto!"

mc "Ainda bem né. Vamos? Preciso entregar a chave pro professor Stein."

me "Ahh... Nós te esperamos do lado de fora da sala."

sk "E deixar a [povname] sozinha com o Stein? Você não bate bem né, Merrii?"

mc "Sozinha? Como assim?"

sk "É... Ele é estranho."

mc "Estranho?"

sk "Sim, você não acha? Ele bebe líquido de bateria como se fosse um suco de caixinha!"
  
menu:
    
    "Na verdade não.": 
        $gentil = True
        sk "Você não acha?"   
        mc "É o jeito dele, Skelly. Não fala assim..."
        sk "Ok, mas você vai lá. A gente te espera aqui!"
        mc "Tá. Não vou demorar."
            
    "Ahm... Ele é um pouquinho estranho.":
        $rude = True
        sk "Né? Mas você vai mesmo assim?"
        mc "Sim. Eu preciso entregar a chave pra ele."
        sk "Ok, sem problema."
        mc "Não vou demorar, só um segundo."
        
      
hide sk normal
hide me normal
"Eu bati na porta, de pé em frente à sala que esperava ser aberta."

"Enquanto espero ser atendida, ouço o barulho de coisas caindo, alguém batendo em algo..."

"Fico confusa, mas tento ignorar e esperar."

"Stein abre a porta sem o seu jaleco de professor, segurando uma das suas mãos, caída, na outra."

"Tentando se apoiar na porta, seu óculos basicamente caindo do seu rosto..."

#CG do Stein

"É uma cena cômica e ao mesmo tempo preocupante."
show st normal
st "Ah, [povname]! Você já terminou de arrumar a sala?"

mc "Sim, aqui está a chave."

st "Oh, obrigado." 

"Stein pega a chave, guardando no bolso da calça."

if gentil: 
    
    st "Você precisa de mais alguma coisa?"
    
    mc "Ah...? N-não, é só que-"
    
    "Eu não consigo tirar o olhar da mão decepada que o professor Stein está segurando. Ela se mexe como normalmente, mas é tão... Surreal."
    
    st "Hmm? Ah! A minha mão te assustou?"
    
    "Stein sorri, balançando a mão decepada, brincando com a própria situação."
    
    mc "N-não, é que... Você está sempre usando o jaleco, então é raro ver você sem ele. É uma camisa muito fofa, sabia?"
    
    "Eu sorrio, desviando a atenção para a camisa dele."
    
    st "Muito gentil da sua parte, mas elogios não vão te dar pontos na prova~"
    
    "Eu cruzo os braços, estufando o peito."
    
    mc "Sabe que não preciso."
    
    "Ele dá uma risadinha, guardando a mão multilada."
    
    mc "Então, já vou indo... Minhas amigas estão me esperando."
    
    st "Sim... Espera! Um presentinho pra você." 
    
    "Stein bota a mão restante no bolso e tira um docinho, oferecendo."
    
    st "Aqui. Por ter sido uma boa menina e me ajudado hoje."
    
    "Eu pego o doce, examinando ele com cuidado. A embalagem é fofa."
    
    mc "Você... Sempre carrega isso no bolso?"
    
    st "Eu gosto. Me ajuda a concentrar no trabalho, já que é feito de açúcar."
    
    mc "Obrigada. Então, até amanhã professor, não fique até muito tarde."
    
    st "O mesmo para você. Sei que não precisa revisar tanto, mas não exagere. Durma cedo."
    
    mc "Okay, até!"
    
    "Eu me despeço dele e guardo o docinho no bolso da minha roupa. Talvez me ajude durante as revisões."
    
    "Eu me dirijo para a minha sala, onde Skelly e Merrii me esperavam."
    
    
if rude:    
    mc "Então eu já vou indo, preciso revisar."
    
    st "Não exagere hoje, sim? Coma bem na janta e durma bastante, você não precisa revisar tanto como alguns da sua turma, então descanse."
    
    mc "Certo, até amanhã."
    
    st "Até!"
    
    "Ele se vira e fecha a porta, voltando aos seus afazeres. Eu me dirijo para a minha sala, onde Skelly e Merrii me esperavam."
hide st normal




mc "Certo. Estou pronta, vamos?"

jump sala    

label sala:

"-Casa da [povname], no quarto-"

show sk normal at left
sk "Ah~! Não aguento mais essa matéria."
show me normal at right
me "Você não devia estudar a mesma coisa por tanto tempo, só vai se aborrecer com a matéria."

mc "Bom, eu já vi o que precisava. Se quiserem, podemos fazer outra coisa."

sk "Ok!"

"Skelly se levanta do chão em que havia deitado, sentando perto de mim."

sk "Seu par pro baile, [povname]. Já decidiu?"

me "[povname] ainda não tem um par?"

mc "Ainda não... Você já tem, Merrii?"

me "Claro. Fui pedida assim que o diretor anunciou a festa. Por algum motivo..."

sk "Tsc. Bom. Quais são as opções?"

mc "Hmm... Deixa eu ver..."

#CG pra mostrar a base e depois o chibi do Kurusu

mc "Ah! Tem o Kurusu, mas ele falou que ia decidir mais tarde..."

sk "Kukki? Nem pensar. Ele vai passar o baile inteiro reclamando de estar ali."
 
me "Mais alguém em mente?"

mc "Não... Sabe que não converso com muita gente."

sk "Pois devia. Vai que você acha alguém legal, né?"
 
me "Bom... Tem aquele menino que fica te olhando na biblioteca."

#CG do chibi do Anorak

mc "Que menino??"

sk "Vai me dizer que você nunca viu ele, [povname]?"

mc "Não? Como ele é?"

me "É um rapaz com faixas, de olho puxado..."

#CG yay

mc "Ah, eu esbarrei com ele no corredor hoje!"

sk "Você só dá bola fora hein [povname]..."

mc "Fica quieta~ Mas porquê ele fica me encarando?"

sk "Olha, eu sinceramente não sei. Parece que ele quer falar contigo ou alguma coisa assim."

me "É um pouquinho estranho, mas ele deve ser tímido..."

sk "Cê devia começar por ele, já que ele tem interesse~"

mc "Hmm... Então acho que amanhã eu tento. Ah, mas e se ele já tiver um par?"

sk "Bom... Nesse caso, acho que é melhor ter mais opções."

me "[povname], tem algum rapaz na sua sala que te interessa?"

mc "Eu não tenho muita certeza... Não conheço eles o suficiente."

sk "Você não precisa conhecer eles. Só precisa ser alguém que você se sinta confortável indo."

mc "Humm... Bom, tem o Gomi. Ele é um Oni, senta no fundo da sala."

#chibi do Gomi

mc "Ele é legal, sempre me pede material emprestado antes da aula começar."

me "Ele é esquecido? Que fofinho."

mc "Uhum. Os professores sempre chamam a atenção por ele ficar bagunçando e pregando peças, mas fora isso, ele parece ser legal."

sk "Ah, então você gosta dele por ser engraçadinho?"

mc "Não diria gostar... Ele só parece ser amigável."

me "Ok... Mais algum que te chame a atenção?"

sk "Ah! Ah! Que tal o Hanael!? Ele é super gente fina, e é amigo do meu nenê. Quem sabe ele não arranja alguma coisa pra você?"

#Chibi do Hanael

me "Eu acho ele meio bobinho, mas se a [povname] gostar..."

mc "É, quem sabe. Se ele for divertido..."

sk "Bom... Mais alguém?"

me "Eu conheço um rapaz, mas... Ele é meio tímido."

mc "E quem seria ele?"

me "É um fantasma, Damien. Mas fora da classe é meio difícil encontrar ele..."

#chibi do Damien

me "Porque ele gosta de ficar invisível pra fugir das pessoas."

mc "Nossa... Mas então porque sugeriu ele? Provavelmente ele nem vai querer ir."

me "Porque achei que vocês dois ficariam fofinhos juntos~ "

sk "Merrii, para. Foco."

me "Mas eu falei sério! Acho que a [povname] devia tentar se ela achar ele interessante."

mc "E-eu vou ver... De repente ele é um cara bacana né?"

"Essa conversa toda está me deixando com vergonha. E se eles não gostarem de mim?"

"E se eu acabar tendo que ir sozinha para o baile??"

me "Sim, sim! Também pode ajudar ele a se abrir mais com as pessoas."

mc "Entendi. Bem, acho que esse é um número razoável de opções."

sk "Ah bom, então você vai começar amanhã?"

mc "Vou sim. A festa tá chegando preciso de alguém logo."

me "Então é melhor nós irmos dormir! Temos provas amanhã cedinho."

sk "Certo!"

"Skelly se levantou e imitou a pose de um super-herói, com as mãos na cintura e o olhar fixo no teto."

sk "Pra cama então, senhoritas!"

mc "Boa noite!"

#chibi das 3 dormindo com pijaminhas pq EU AMO ELAS AAA TU É GAY DEMAIS

"- Segunda de manhã, casa da [povname] -"

"Eu e a Skelly estamos sentadas na mesa, esperando o café ficar pronto."

"Merrii gosta de cozinhar pra gente. Além de ser uma boa comida, é até melhor para nós que somos preguiçosas~"

sk "Bom, você precisa começar hoje, [povname]. Ainda tem que comprar a roupa que combine com o seu par."

mc "Roupa que combine...?"

sk "Sim, você quer agradá-lo, não quer?"

mc "Errr... Acho que sim...?"

"Skelly se entusiasma demais quando o assunto se trata de festas."

sk "Você tem que estar impecável! Merrii e eu vamos te ajudar com isso, ok? Você só precisa se preocupar em arranjar um par."

mc "Ceerto!"

"Merrii se aproxima trazendo os pratos com cuidado, um em cada mão para cada uma de nós."

me "Aqui está. Comam logo meninas, não queremos chegar atrasadas!"

sk "Não precisa nem falar."

hide sk normal
hide me normal

"Nós comemos animadas, preocupadas com o horário."

"- Após as provas -"

show st normal 
st "Certo! Acabou o tempo. Por favor, guardem seus materiais. A representante vai coletar as provas, o resto está dispensado pelo resto do dia."

mc "Ok, é a minha chance. Hora de escolher com quem passar um tempo!"
hide st normal

menu:
    "Quem eu devo escolher para ser meu parceiro?"
    
    "O menino enfaixado":
        $ anorak = True

        jump anorak

    "Kurusu":
        $ kurusu = True
        "Kurusu é meu amigo desde que eu me lembro, vai ser mais divertido ir com alguém que eu conheça."

        jump kurusu

#Aqui o jogador escolhe, pelo mapa, onde quer ir.

#"~ Fim do prólogo~"
                                                                        
                                                                       



return 
