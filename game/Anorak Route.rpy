#Anorak Route - Day 1

label anorak:


mc "Aquele rapaz parece ser gentil... Talvez ele aceite."

mc "Merrii disse que ele frequenta a biblioteca, vai ser um bom lugar para conversar."

mc "E eu preciso estudar pra prova, posso pegar o conteúdo por lá."

"Eu pego a minha mochila e vou em direção á biblioteca, passando pelos corredores de sala de aula."

"~Biblioteca~"

"Procurando o assunto na estante, percebo que a coleção é vasta demais. Vai demorar um tempo até achar o que preciso..."

"Não é rígido demais uma escola ter estantes organizadas em ordem alfabética?..."

"Eu não teria tanta disposição pra organizar desse jeito. Talvez conseguisse com a ajuda das minhas amigas..."

"Ou iríamos conversar o dia todo e deixar a bagunça completamente de lado."

mc "Não é hora de sair pensando em organizações de estante, [povname]. Eu preciso procurar aquele maldito livro para estudar ou estou encrencada no teste." 

show who normal
who "Precisa de alguma ajuda?" 

"Uma voz sussurrada atrás de mim me paralisou por alguns segundos. Pensei que estaria sozinha nesse corredor!"

"Eu falei comigo mesma em voz alta? Urgh! Eu preciso parar com esse costume ou as pessoas vão achar que eu sou maluca."

"Me virei para encontrar a pessoa que me fez a pergunta. Um garoto segurando um livro de capa marrom e título escrito em detalhes dourados me encarava sério, esperando uma suposta resposta."

"Eu devo estar demorando demais para falar com ele, julgando pelo jeito como ele me olha."

mc "Uh- n-na verdade não. Mas obrigada pela ajuda..."

"Ele assentiu e se virou em direção à mesa perto de outras estantes, sentando em uma das cadeiras."

"E com a mesma expressão que me respondeu calado, abriu o livro direto na metade, como se estivesse marcando mentalmente a página que havia parado."

"Coincidentemente, o assunto do livro do rapaz parecia ser o que eu precisava."

"Procurando por um livro parecido nas estantes, achei um semelhante. E abrindo aproximadamente na parte em que ele estava, me deparei com o assunto."

mc "Hmm..."

"Ele parecia concentrado no que estava lendo, como se fosse uma obrigação ler aquilo. Talvez o assunto do teste dele não seja tão interessante assim."

mc "Sobre o que é esse seu livro??"

"O menino deu um saltinho da cadeira e suas faixas se esticaram como se também tivessem se assustado"

"Ele fez um sinal de silêncio em frente a sua boca, um -shh- mudo com certa irritação."

who "Estamos em uma biblioteca." #with vpunch

mc "Desculpa, não quis te assustar."

"O garoto suspirou, desapontado com a minha atitude. Ele parece prezar muito pelo silêncio da biblioteca..."

"Eu consegui ouvir um risinho baixo vindo da direção dele, e ele pareceu virar de leve o rosto como se quisesse esconder o sorriso"

"Eu não poderia ver de qualquer forma, já que ele é todo enfaixado."

"Mesmo assim... Foi bem fofo."

"Eu desviei a minha atenção para o livro do meu vizinho de mesa. Meus olhos passeiam pelas letras minúsculas da página grossa antes dele virar para a próxima página."

"Ele não parece ligar para a minha presença, lendo cada palavra do livro com atenção."

"A página em questão tinha a foto de um besouro que parecia ter chifres."

"Não estudamos na mesma classe, então o professor dele deve estar dando uma matéria diferente. Nunca chegamos a ver esses besourinhos."

"Confusa e com os pensamentos vindo um atrás do outro, não percebo quando ele deixa a atenção do livro e vira para me encarar."

who "Escaravelhos."

mc "O-oi?"

"Eu me afasto um pouco segurando o meu livro com mais força nas mãos, fingindo estar interessada nele"

who "Escaravelhos. É um livro de entomologia."

mc "Anta-... O quê?"

"Ele revirou os olhos, suspirando. Deve achar que eu não notei pelas bandagens em seu rosto."

who "Estudo de insetos."

mc "Oh... E você se interessa por isso?"

who "Um pouco. Preciso estudar isso avulso do conteúdo das aulas."

mc "Mesmo? Não está preocupado com as provas?"

who "Não muito... O conteúdo do meu pai é 'mais importante', segundo ele."

"Ele soltou mais um suspiro e trocou de página com certo desdém."

"Eu fixo o olhar nas palavras e figuras. O texto parou de ser sobre a parte anatômica dos besouros, mudando para feitiços e magias usando eles e a energia deles."

mc "Parece ser interessante..."

who "E é. Mas o contexto nem tanto."

mc "Como assim?"

"O rapaz fecha o livro, voltando sua atenção diretamente para mim."

who "Eu sou filho do faraó Pekhrari. Sou Anorak, o príncipe herdeiro."
hide who normal
show a normal
a "Então tenho que estudar a cultura do meu povo - do meu reino, para poder governar."

a "Governar aos moldes do meu pai, que acha correto o que faz."

mc "E você não parece achar tanto..."

a "Sim e não. Acho o modo como ele rege antiquado demais."

mc "Em questão de tecnologia ou cultural?"

a "Ambos. Ele não aceita que os tempos são outros, e que uma mão menos rígida seria mais adequada."

mc "Hmm... Mas você pode mudar isso quando for rei, não?"

"Ele cruza os braços, pensativo."

"Parando pra analisar ele, por baixo dessas ataduras ele parece ser uma pessoa bem interessante"

"O cabelo azul é parcialmente escondido pelas faixas, e as orelhas levemente pontudas..."

"Os olhos vermelhos, fixos no livro, pensativo."

"Anorak é bem fofo. Espero que ele aceite ir comigo no baile."

"Sua voz sai um pouco baixa, atrapalhando os meus pensamentos e acabando com o meu monólogo interno."

a "Eu não... Tenho certeza se quero ser governante."

mc "É uma decisão bem importante..."

a "É sim. E eu não tenho maturidade pra pensar nisso ainda."

"Anorak suspira, voltando a prestar atenção no livro."

a "Você deveria voltar a ler o seu livro. As provas são amanhã."

mc "Ah, a prova! Não lembrava!"

a "Shh. Fale baixinho."

mc "S-sim..."
hide a normal
"Com o rosto corado por elevar minha voz novamente, eu abro o livro pra esconder a minha vergonha."

"Anorak sorri por baixo das faixas, focando em seu estudo. As ataduras escondem o sorriso, mas seus olhos não."

"Com o passar do tempo percebi alguns olhares em minha direção, vindas do rapaz ao meu lado. Não sei dizer se eram para mim ou para o meu livro..."

"Eventualmente eu acabo me concentrando demais para trocar alguma palavra com ele"

"E assim ficamos até a biblioteca fechar. Nos damos um breve ~tchauzinho~ e cada um vai para a sua casa." 


#Anorak Route - Day 2 

"Hoje cheguei mais cedo no colégio. Eu esperava que por ser dia de prova, os corredores ficassem mais cheios"

"Então achei que seria uma boa idéia revisar antes das aulas começarem."

"Minha sala de aula é a melhor opção. Alguns alunos costumam se desesperar e correr para a biblioteca, na esperança de que algum aluno com boas notas consiga os ensinar nos ultimos segundos restantes."

"O conteúdo é bem básico... Me impressionou que o rapaz de ontem estivesse tão preso assim na leitura."

"Insetos. Não é exatamente o que eu esperava estudar nesse semestre, mas o Stein disse que eles são super importantes para nós."

"E o livro não é como os outros da biblioteca. Não são folhas e mais folhas dizendo chatices."

"Eu acho que posso acabar gostando do assunto."

"Enquanto eu caminho pelo corredor eu lembro daquele daquele rapaz. Ele não é exatamente quieto, mas é bem misterioso."

"Quero muito que ele aceite ir comigo no baile..."

"É um pouco improvável ele não ter par, por ser príncipe. Ele já deve ter alguém para ir."

"Um esbarro no meu braço balança os livros das minhas mãos e quase os derruba, me fazendo perder completamente a linha de pensamento."

"Enquanto eu recobrava o equilíbrio perdido após a pessoa esbarrar em mim, ela se aproximava, fazendo um gesto cauteloso"

"Ela segura meu braço, ajudando a me manter em pé junto com os livros. Eu reconheço essa mão"

"Meus olhos reconhecem algumas bandagens que cobrem o corpo da pessoa a minha frente"

"De todas as pessoas para passar por mim, justo ele."

"Justo quem eu queria ver agora."
show a normal
a "Oh. Perdão, não quis incomodar você."

"Vendo o quão próximos estamos logo ele se recolhe, cruzando os braços"

"Nos encontramos novamente com ele quase me matando."

mc "Não foi nada, não. Eu tô bem."

"Eu forço um sorrisinho, escondendo o susto do baque repentino"

"Ele suspirou aliviado, fazendo parecer que quase me derrubou de um lugar alto."

a "Ótimo, isso é bom. Eu deveria ter mais cuidado..."

"Anorak sorri por baixo das suas bandagens. Só consigo ver seus olhos por trás delas, diminuindo até chegar em uma linha fina."

"Ele é bem fofo."

"Imagino o que teria atrás dessas bandagens todas..."

"Anorak permanece imóvel, mas agora está olhando para algo nas minhas mãos, observando."

"Acho que reconheceu o que eu estava lendo."

a "O livro que eu peguei ontem?"

"Ele me olha nos olhos, me fazendo tremer de leve."

"Ele não é nem um pouco ameaçador, mas quando me olha diretamente..."

"Os olhos vermelhos dele são penetrantes."

mc "S-sim... O teste é daqui a pouco."

a "Hm. Boa sorte, acho que vai precisar."

"Ele não parece ser de muita conversa. Não é que ele tente ser seco, é só..."

"Talvez não saiba como conversar?"

"Talvez seja tímido?"

mc "Obrigada. É bom um pouco de sorte, mas vai ser moleza. Entomologia é divertido de ler."

"Os olhos do rapaz brilharam ao ouvir o que eu disse. Parecia interessado pelo caminho que a conversa estava tomando."

a "Você não parecia conhecer muito de entomologia ontem. Esse assunto te interessa?"

mc "É fascinante conhecer os bichinhos que existem por aí."

mc "E escaravelhos são bonitinhos e intrigantes."

a "Escaravelhos são sagrados para a minha cultura..."

"Ele sorriu mais uma vez, tentando esconder os olhos fechados com o cabelo. Meu coração palpitou forte, e eu não pude deixar de esconder um rubor."

"Eu aperto um pouco mais o livro, tentando me tirar desse transe."

a "Que tolice..."

"Meu rosto esquentou. Ele estava falando de mim? O que ele quis dizer?"

"Eu estar parada na sua frente não ajuda. Eu devo estar parecendo uma idiota."

"Ele balançou a cabeça, levando uma das suas mãos para a parte de trás da sua cabeça e coçando de leve."

a "...Como pude esquecer de me apresentar devidamente a você?"

mc "Ah. Era isso."

a "Isso?"

mc "N-nada. Eu pensei que você tinha me chamado de tola."

a "Você é tola."

mc "Ah."

"O sangue subiu tão rápido ao meu rosto, deixando ele completamente vermelho."

"Anorak solta um risinho baixo, estendendo a mão para mim."

a "Eu me chamo Anorak."

"Eu assenti, retribuindo com um sorriso gentil, segurando em sua mão."

mc "Eu sou a [povname]..."

a "Eu sei o seu nome. Mesmo assim, é um prazer, [povname]."

mc "Você sabe o meu nome?"

a "É... Eu ouço as suas amigas te chamando assim e... Bom, te vejo depois das provas?"

mc "É verdade, o teste! Preciso ir, mais tarde a gente se encontra!"

"Eu retomo meu caminho sem esperar por uma resposta. O Stein não ia me deixar entrar se chegasse atrasada."

"Eu me viro para trás, já com uma certa distância do rapaz."

mc "Quem sabe não podemos conversar mais sobre os insetinhos algum outro dia?"

"Anorak arqueou uma sobrancelha, um leve vermelho surgiu em suas bochechas e a sua expressão fazia parecer curioso sobre o que falei."

a "Está me convidando para alguma coisa?"

"...Realmente soou como um convite para um encontro."

"E é exatamente o que eu quero com ele. Quero convidar para o baile!"

mc "Me encontra no pátio depois das provas. Talvez eu tenha te convidado mesmo."

a "Você não tem certeza, mesmo pedindo?"

mc "..."

"Eu desviei o olhar, não sabendo como responder."

mc "N-não..."

"Ele riu, assentindo com a cabeça"

a "Certo. Até mais tarde, Núbia."

"Anorak acenou e continuou o seu caminho."

"Ele me chamou de quê? N-núbia?"

"Deve significar alguma coisa na língua dele. Espero que não seja algo ruim ou bobo..."

mc "A prova!"

"Correr para a sala é a minha única opção agora. Stein já deve estar entrando."

"Agora não tenho só a prova para me preocupar..."

"Eu convidei mesmo ele pra sair!"

"Não foi um pedido concreto, então vou ter que esperar após as provas para ter certeza da resposta."

"Espero que seja positiva..."

"~Após a prova~"

"Sai da sala ás pressas. Mal consegui arrumar meu material e já estava no corredor."

"Eu preciso saber da resposta dele. Preciso saber se ele não estava me fazendo de boba..."

"Corri até o pátio, passando por várias pessoas. Ele já deve estar vindo."

"Sentado em um banco, observando as pessoas passarem. Lá estava Anorak."

mc "Okay... Respira fundo e vai lá."

"Eu seguro minha bolsa com mais força, camInhando até ele."

"Ele parecia em transe, perdido em pensamentos. Não notou quando me aproximei, sentando em seu lado."

mc "Oi Anorak. Tudo bem?"

"Anorak se virou para mim, me olhando por um tempo antes de ter alguma reação."

a "Oh. [povname]! Você veio!"

mc "Heheh. Você estava distraído."

a "Estava pensando enquanto esperava você..."

mc "Eu te fiz esperar muito?"

"Eu me ajeito no banco, encolhendo as pernas para perto uma da outra."

a "Não. Na verdade nem vi o tempo passar. O que me fez ficar assim foi o seu pedido hoje mais cedo..."

mc "A-ah... Eu te chamei pra sair né?..."

a "Aquilo foi sincero? Você quer mesmo sair comigo?"

mc "H-hmm... Digo, sim?"

"Ele se vira em minha direção e me encara nos olhos"

"Com os seus olhos sendo a única coisa aparente em seu rosto, eles ficam um pouco intimidadores de perto."

"Anorak parece estar procurando por alguma coisa no meu rosto. Parece desconfiado, ou com medo..."

"Ficamos assim por alguns segundos, até ele desviar o olhar e puxar uma das faixas do seu braço direito."

a "Tudo bem. Se você quer... Podemos nos ver."

mc "Mesmo? Quando?"

"Eu esboço um sorriso, me apoiando com as mãos no banco e me aproximando dele"

"Anorak não consegue esconder o pulinho, evitando me olhar."

a "A-amanhã... Se estiver tudo bem pra você."

mc "Por mim ótimo! Me dá seu número e eu te mando uma mensagem mais tarde~" 

a "C-certo..."

"Ele pega o meu celular e digita os números, salvando seu contato."

"Durante todo o processo ele evitou ao máximo olhar para mim"

"Eu consigo ver o vermelho do rosto dele se expandir para as orelhas pontudas."

a "Eu... Eu preciso ir indo."

"Anorak se levanta, estendendo a mão para mim. Seu olhar fixo em mim, ainda com as bochechas coradas."

mc "Oh... Obrigada."

"Eu pego sua mão, levantando do banco."

"Ele sorri pra mim, levemente deixando minha mão escapar da dele."

a "Te vejo amanhã, [povname]?"

mc "S-sim... Amanhã..."
hide a normal

"Eu sorrio de volta, vendo ele se dirigir até a entrada do colégio e desaparecer em uma charrete"

"Logo eu chego em casa e jogo minhas coisas na cama. Puxando o celular para conversar no chat com as meninas, vejo o seu contato salvo no meu celular."

"Ele salvou com um coraçãozinho..."

"Não consigo conter um sorriso."

"Depois de conversar com elas, as meninas me encorajaram a chamar ele para conversar."

"Combinamos de nos encontrar na frente do museu amanhã. É um lugar calmo e com várias coisas interessantes para vermos."

"Tenho a impressão de que não vou conseguir dormir hoje."









 




















