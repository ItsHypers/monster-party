﻿#-------------------------------------------------------------------------------
# This sets up a few function calls and the Class GalleryItem
#-------------------------------------------------------------------------------
init python:

    def MaxScale(img, minwidth = config.screen_width, minheight = config.screen_height):
        # image_size is a function that slows the load of the game.
        # Once we have proper images with right size we can pull these functions.
        # I think. - Kylatia
        currwidth, currheight = renpy.image_size(img)
        xscale = float(minwidth) / currwidth
        yscale = float(minheight) / currheight

        if xscale > yscale:
            maxscale = xscale
        else:
            maxscale = yscale

        return im.FactorScale(img, maxscale, maxscale)

    def MinScale (img, maxwidth = config.screen_width, maxheight = config.screen_height):
        # image_size is a function that slows the load of the game.
        # Once we have proper images with right size we can pull these functions.
        # I think. - Kylatia

        currwidth, currheight = renpy.image_size(img)
        xscale = float(maxwidth) / currwidth
        yscale = float(maxheight) / currheight

        if xscale < yscale:
            minscale = xscale
        else:
            minscale = yscale

        return im.FactorScale(img, minscale, minscale)
#-------------------------------------------------------------------------------
# This is the maxnumber per page of the Gallery.
    maxnumberx = 2
    maxnumbery = 2
# This is sets the Thumbnail Sizes
    maxthumbx = config.screen_width / ( maxnumberx + 1)
    maxthumby = config.screen_height / (maxnumbery + 1)

# This is how many Images pre page
    maxperpage = maxnumberx * maxnumbery
# This is what page we are on.  0 is garenteed, but not any others
    gallery_page = 0
    closeup_page = 0
#-------------------------------------------------------------------------------
    class GalleryItem:
        def __init__ (self, name, images, thumb, locked = "lockedthumb"):
            self.name = name
            self.images = images
            self.thumb = thumb
            self.locked = locked
            self.refresh_lock()

        def num_images(self):
            return len(self.images)

        def refresh_lock(self):
            self.num_unlocked = 0
            lockme = False
            for img in self.images:
                if not renpy.seen_image(img):
                    lockme = True
                else:
                    self.num_unlocked += 1
            self.is_locked = lockme
#-------------------------------------------------------------------------------
# Quick and dirty fill the array of pictures with thumbnails.  With a 1 for 1.
# We can allow grouping of images. This needs to be "hand crafted"
# as we figure out what exactly we want in our gallery.
#-------------------------------------------------------------------------------
    numGalleryItems = 17
    gallery_items = [
            GalleryItem("Image%02d" % i, [ "gal%02d" % i ], "thumb%02d" % i)
                for i in range(1, numGalleryItems + 1)
        ]
        
