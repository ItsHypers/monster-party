﻿# TODO: Translation updated at 2020-07-15 00:25

translate italian strings:

    # renpy/common/00accessibility.rpy:28
    old "Self-voicing disabled."
    new ""

    # renpy/common/00accessibility.rpy:29
    old "Clipboard voicing enabled. "
    new ""

    # renpy/common/00accessibility.rpy:30
    old "Self-voicing enabled. "
    new ""

    # renpy/common/00accessibility.rpy:32
    old "bar"
    new ""

    # renpy/common/00accessibility.rpy:33
    old "selected"
    new ""

    # renpy/common/00accessibility.rpy:34
    old "viewport"
    new ""

    # renpy/common/00accessibility.rpy:35
    old "horizontal scroll"
    new ""

    # renpy/common/00accessibility.rpy:36
    old "vertical scroll"
    new ""

    # renpy/common/00accessibility.rpy:37
    old "activate"
    new ""

    # renpy/common/00accessibility.rpy:38
    old "deactivate"
    new ""

    # renpy/common/00accessibility.rpy:39
    old "increase"
    new ""

    # renpy/common/00accessibility.rpy:40
    old "decrease"
    new ""

    # renpy/common/00accessibility.rpy:128
    old "Font Override"
    new ""

    # renpy/common/00accessibility.rpy:132
    old "Default"
    new ""

    # renpy/common/00accessibility.rpy:136
    old "DejaVu Sans"
    new ""

    # renpy/common/00accessibility.rpy:140
    old "Opendyslexic"
    new ""

    # renpy/common/00accessibility.rpy:146
    old "Text Size Scaling"
    new ""

    # renpy/common/00accessibility.rpy:152
    old "Reset"
    new ""

    # renpy/common/00accessibility.rpy:158
    old "Line Spacing Scaling"
    new ""

    # renpy/common/00accessibility.rpy:171
    old "Self-Voicing"
    new ""

    # renpy/common/00accessibility.rpy:175
    old "Off"
    new ""

    # renpy/common/00accessibility.rpy:179
    old "Text-to-speech"
    new ""

    # renpy/common/00accessibility.rpy:183
    old "Clipboard"
    new ""

    # renpy/common/00accessibility.rpy:187
    old "Debug"
    new ""

    # renpy/common/00accessibility.rpy:193
    old "Self-Voicing Volume Drop"
    new ""

    # renpy/common/00accessibility.rpy:202
    old "The options on this menu are intended to improve accessibility. They may not work with all games, and some combinations of options may render the game unplayable. This is not an issue with the game or engine. For the best results when changing fonts, try to keep the text size the same as it originally was."
    new ""

    # renpy/common/00action_file.rpy:26
    old "{#weekday}Monday"
    new ""

    # renpy/common/00action_file.rpy:26
    old "{#weekday}Tuesday"
    new ""

    # renpy/common/00action_file.rpy:26
    old "{#weekday}Wednesday"
    new ""

    # renpy/common/00action_file.rpy:26
    old "{#weekday}Thursday"
    new ""

    # renpy/common/00action_file.rpy:26
    old "{#weekday}Friday"
    new ""

    # renpy/common/00action_file.rpy:26
    old "{#weekday}Saturday"
    new ""

    # renpy/common/00action_file.rpy:26
    old "{#weekday}Sunday"
    new ""

    # renpy/common/00action_file.rpy:37
    old "{#weekday_short}Mon"
    new ""

    # renpy/common/00action_file.rpy:37
    old "{#weekday_short}Tue"
    new ""

    # renpy/common/00action_file.rpy:37
    old "{#weekday_short}Wed"
    new ""

    # renpy/common/00action_file.rpy:37
    old "{#weekday_short}Thu"
    new ""

    # renpy/common/00action_file.rpy:37
    old "{#weekday_short}Fri"
    new ""

    # renpy/common/00action_file.rpy:37
    old "{#weekday_short}Sat"
    new ""

    # renpy/common/00action_file.rpy:37
    old "{#weekday_short}Sun"
    new ""

    # renpy/common/00action_file.rpy:47
    old "{#month}January"
    new ""

    # renpy/common/00action_file.rpy:47
    old "{#month}February"
    new ""

    # renpy/common/00action_file.rpy:47
    old "{#month}March"
    new ""

    # renpy/common/00action_file.rpy:47
    old "{#month}April"
    new ""

    # renpy/common/00action_file.rpy:47
    old "{#month}May"
    new ""

    # renpy/common/00action_file.rpy:47
    old "{#month}June"
    new ""

    # renpy/common/00action_file.rpy:47
    old "{#month}July"
    new ""

    # renpy/common/00action_file.rpy:47
    old "{#month}August"
    new ""

    # renpy/common/00action_file.rpy:47
    old "{#month}September"
    new ""

    # renpy/common/00action_file.rpy:47
    old "{#month}October"
    new ""

    # renpy/common/00action_file.rpy:47
    old "{#month}November"
    new ""

    # renpy/common/00action_file.rpy:47
    old "{#month}December"
    new ""

    # renpy/common/00action_file.rpy:63
    old "{#month_short}Jan"
    new ""

    # renpy/common/00action_file.rpy:63
    old "{#month_short}Feb"
    new ""

    # renpy/common/00action_file.rpy:63
    old "{#month_short}Mar"
    new ""

    # renpy/common/00action_file.rpy:63
    old "{#month_short}Apr"
    new ""

    # renpy/common/00action_file.rpy:63
    old "{#month_short}May"
    new ""

    # renpy/common/00action_file.rpy:63
    old "{#month_short}Jun"
    new ""

    # renpy/common/00action_file.rpy:63
    old "{#month_short}Jul"
    new ""

    # renpy/common/00action_file.rpy:63
    old "{#month_short}Aug"
    new ""

    # renpy/common/00action_file.rpy:63
    old "{#month_short}Sep"
    new ""

    # renpy/common/00action_file.rpy:63
    old "{#month_short}Oct"
    new ""

    # renpy/common/00action_file.rpy:63
    old "{#month_short}Nov"
    new ""

    # renpy/common/00action_file.rpy:63
    old "{#month_short}Dec"
    new ""

    # renpy/common/00action_file.rpy:240
    old "%b %d, %H:%M"
    new ""

    # renpy/common/00action_file.rpy:353
    old "Save slot %s: [text]"
    new ""

    # renpy/common/00action_file.rpy:434
    old "Load slot %s: [text]"
    new ""

    # renpy/common/00action_file.rpy:487
    old "Delete slot [text]"
    new ""

    # renpy/common/00action_file.rpy:569
    old "File page auto"
    new ""

    # renpy/common/00action_file.rpy:571
    old "File page quick"
    new ""

    # renpy/common/00action_file.rpy:573
    old "File page [text]"
    new ""

    # renpy/common/00action_file.rpy:772
    old "Next file page."
    new ""

    # renpy/common/00action_file.rpy:845
    old "Previous file page."
    new ""

    # renpy/common/00action_file.rpy:906
    old "Quick save complete."
    new ""

    # renpy/common/00action_file.rpy:924
    old "Quick save."
    new ""

    # renpy/common/00action_file.rpy:943
    old "Quick load."
    new ""

    # renpy/common/00action_other.rpy:375
    old "Language [text]"
    new ""

    # renpy/common/00director.rpy:708
    old "The interactive director is not enabled here."
    new ""

    # renpy/common/00director.rpy:1481
    old "⬆"
    new ""

    # renpy/common/00director.rpy:1487
    old "⬇"
    new ""

    # renpy/common/00director.rpy:1551
    old "Done"
    new ""

    # renpy/common/00director.rpy:1561
    old "(statement)"
    new ""

    # renpy/common/00director.rpy:1562
    old "(tag)"
    new ""

    # renpy/common/00director.rpy:1563
    old "(attributes)"
    new ""

    # renpy/common/00director.rpy:1564
    old "(transform)"
    new ""

    # renpy/common/00director.rpy:1589
    old "(transition)"
    new ""

    # renpy/common/00director.rpy:1601
    old "(channel)"
    new ""

    # renpy/common/00director.rpy:1602
    old "(filename)"
    new ""

    # renpy/common/00director.rpy:1631
    old "Change"
    new ""

    # renpy/common/00director.rpy:1633
    old "Add"
    new ""

    # renpy/common/00director.rpy:1636
    old "Cancel"
    new ""

    # renpy/common/00director.rpy:1639
    old "Remove"
    new ""

    # renpy/common/00director.rpy:1674
    old "Statement:"
    new ""

    # renpy/common/00director.rpy:1695
    old "Tag:"
    new ""

    # renpy/common/00director.rpy:1711
    old "Attributes:"
    new ""

    # renpy/common/00director.rpy:1729
    old "Transforms:"
    new ""

    # renpy/common/00director.rpy:1748
    old "Behind:"
    new ""

    # renpy/common/00director.rpy:1767
    old "Transition:"
    new ""

    # renpy/common/00director.rpy:1785
    old "Channel:"
    new ""

    # renpy/common/00director.rpy:1803
    old "Audio Filename:"
    new ""

    # renpy/common/00gui.rpy:374
    old "Are you sure?"
    new ""

    # renpy/common/00gui.rpy:375
    old "Are you sure you want to delete this save?"
    new ""

    # renpy/common/00gui.rpy:376
    old "Are you sure you want to overwrite your save?"
    new ""

    # renpy/common/00gui.rpy:377
    old "Loading will lose unsaved progress.\nAre you sure you want to do this?"
    new ""

    # renpy/common/00gui.rpy:378
    old "Are you sure you want to quit?"
    new ""

    # renpy/common/00gui.rpy:379
    old "Are you sure you want to return to the main menu?\nThis will lose unsaved progress."
    new ""

    # renpy/common/00gui.rpy:380
    old "Are you sure you want to end the replay?"
    new ""

    # renpy/common/00gui.rpy:381
    old "Are you sure you want to begin skipping?"
    new ""

    # renpy/common/00gui.rpy:382
    old "Are you sure you want to skip to the next choice?"
    new ""

    # renpy/common/00gui.rpy:383
    old "Are you sure you want to skip unseen dialogue to the next choice?"
    new ""

    # renpy/common/00keymap.rpy:267
    old "Failed to save screenshot as %s."
    new ""

    # renpy/common/00keymap.rpy:279
    old "Saved screenshot as %s."
    new ""

    # renpy/common/00library.rpy:195
    old "Skip Mode"
    new ""

    # renpy/common/00library.rpy:281
    old "This program contains free software under a number of licenses, including the MIT License and GNU Lesser General Public License. A complete list of software, including links to full source code, can be found {a=https://www.renpy.org/l/license}here{/a}."
    new ""

    # renpy/common/00preferences.rpy:236
    old "display"
    new ""

    # renpy/common/00preferences.rpy:248
    old "transitions"
    new ""

    # renpy/common/00preferences.rpy:257
    old "skip transitions"
    new ""

    # renpy/common/00preferences.rpy:259
    old "video sprites"
    new ""

    # renpy/common/00preferences.rpy:268
    old "show empty window"
    new ""

    # renpy/common/00preferences.rpy:277
    old "text speed"
    new ""

    # renpy/common/00preferences.rpy:285
    old "joystick"
    new ""

    # renpy/common/00preferences.rpy:285
    old "joystick..."
    new ""

    # renpy/common/00preferences.rpy:292
    old "skip"
    new ""

    # renpy/common/00preferences.rpy:295
    old "skip unseen [text]"
    new ""

    # renpy/common/00preferences.rpy:300
    old "skip unseen text"
    new ""

    # renpy/common/00preferences.rpy:302
    old "begin skipping"
    new ""

    # renpy/common/00preferences.rpy:306
    old "after choices"
    new ""

    # renpy/common/00preferences.rpy:313
    old "skip after choices"
    new ""

    # renpy/common/00preferences.rpy:315
    old "auto-forward time"
    new ""

    # renpy/common/00preferences.rpy:329
    old "auto-forward"
    new ""

    # renpy/common/00preferences.rpy:336
    old "Auto forward"
    new ""

    # renpy/common/00preferences.rpy:339
    old "auto-forward after click"
    new ""

    # renpy/common/00preferences.rpy:348
    old "automatic move"
    new ""

    # renpy/common/00preferences.rpy:357
    old "wait for voice"
    new ""

    # renpy/common/00preferences.rpy:366
    old "voice sustain"
    new ""

    # renpy/common/00preferences.rpy:375
    old "self voicing"
    new ""

    # renpy/common/00preferences.rpy:384
    old "self voicing volume drop"
    new ""

    # renpy/common/00preferences.rpy:392
    old "clipboard voicing"
    new ""

    # renpy/common/00preferences.rpy:401
    old "debug voicing"
    new ""

    # renpy/common/00preferences.rpy:410
    old "emphasize audio"
    new ""

    # renpy/common/00preferences.rpy:419
    old "rollback side"
    new ""

    # renpy/common/00preferences.rpy:429
    old "gl powersave"
    new ""

    # renpy/common/00preferences.rpy:435
    old "gl framerate"
    new ""

    # renpy/common/00preferences.rpy:438
    old "gl tearing"
    new ""

    # renpy/common/00preferences.rpy:441
    old "font transform"
    new ""

    # renpy/common/00preferences.rpy:444
    old "font size"
    new ""

    # renpy/common/00preferences.rpy:452
    old "font line spacing"
    new ""

    # renpy/common/00preferences.rpy:471
    old "music volume"
    new ""

    # renpy/common/00preferences.rpy:472
    old "sound volume"
    new ""

    # renpy/common/00preferences.rpy:473
    old "voice volume"
    new ""

    # renpy/common/00preferences.rpy:474
    old "mute music"
    new ""

    # renpy/common/00preferences.rpy:475
    old "mute sound"
    new ""

    # renpy/common/00preferences.rpy:476
    old "mute voice"
    new ""

    # renpy/common/00preferences.rpy:477
    old "mute all"
    new ""

    # renpy/common/00preferences.rpy:558
    old "Clipboard voicing enabled. Press 'shift+C' to disable."
    new ""

    # renpy/common/00preferences.rpy:560
    old "Self-voicing would say \"[renpy.display.tts.last]\". Press 'alt+shift+V' to disable."
    new ""

    # renpy/common/00preferences.rpy:562
    old "Self-voicing enabled. Press 'v' to disable."
    new ""

    # renpy/common/_compat/gamemenu.rpym:198
    old "Empty Slot."
    new ""

    # renpy/common/_compat/gamemenu.rpym:355
    old "Previous"
    new ""

    # renpy/common/_compat/gamemenu.rpym:362
    old "Next"
    new ""

    # renpy/common/_compat/preferences.rpym:428
    old "Joystick Mapping"
    new ""

    # renpy/common/_developer/developer.rpym:38
    old "Developer Menu"
    new ""

    # renpy/common/_developer/developer.rpym:43
    old "Interactive Director (D)"
    new ""

    # renpy/common/_developer/developer.rpym:45
    old "Reload Game (Shift+R)"
    new ""

    # renpy/common/_developer/developer.rpym:47
    old "Console (Shift+O)"
    new ""

    # renpy/common/_developer/developer.rpym:49
    old "Variable Viewer"
    new ""

    # renpy/common/_developer/developer.rpym:51
    old "Image Location Picker"
    new ""

    # renpy/common/_developer/developer.rpym:53
    old "Filename List"
    new ""

    # renpy/common/_developer/developer.rpym:57
    old "Show Image Load Log (F4)"
    new ""

    # renpy/common/_developer/developer.rpym:60
    old "Hide Image Load Log (F4)"
    new ""

    # renpy/common/_developer/developer.rpym:63
    old "Image Attributes"
    new ""

    # renpy/common/_developer/developer.rpym:90
    old "[name] [attributes] (hidden)"
    new ""

    # renpy/common/_developer/developer.rpym:94
    old "[name] [attributes]"
    new ""

    # renpy/common/_developer/developer.rpym:143
    old "Nothing to inspect."
    new ""

    # renpy/common/_developer/developer.rpym:154
    old "Hide deleted"
    new ""

    # renpy/common/_developer/developer.rpym:154
    old "Show deleted"
    new ""

    # renpy/common/_developer/developer.rpym:278
    old "Return to the developer menu"
    new ""

    # renpy/common/_developer/developer.rpym:443
    old "Rectangle: %r"
    new ""

    # renpy/common/_developer/developer.rpym:448
    old "Mouse position: %r"
    new ""

    # renpy/common/_developer/developer.rpym:453
    old "Right-click or escape to quit."
    new ""

    # renpy/common/_developer/developer.rpym:485
    old "Rectangle copied to clipboard."
    new ""

    # renpy/common/_developer/developer.rpym:488
    old "Position copied to clipboard."
    new ""

    # renpy/common/_developer/developer.rpym:506
    old "Type to filter: "
    new ""

    # renpy/common/_developer/developer.rpym:634
    old "Textures: [tex_count] ([tex_size_mb:.1f] MB)"
    new ""

    # renpy/common/_developer/developer.rpym:638
    old "Image cache: [cache_pct:.1f]% ([cache_size_mb:.1f] MB)"
    new ""

    # renpy/common/_developer/developer.rpym:648
    old "✔ "
    new ""

    # renpy/common/_developer/developer.rpym:651
    old "✘ "
    new ""

    # renpy/common/_developer/developer.rpym:656
    old "\n{color=#cfc}✔ predicted image (good){/color}\n{color=#fcc}✘ unpredicted image (bad){/color}\n{color=#fff}Drag to move.{/color}"
    new ""

    # renpy/common/_developer/inspector.rpym:38
    old "Displayable Inspector"
    new ""

    # renpy/common/_developer/inspector.rpym:61
    old "Size"
    new ""

    # renpy/common/_developer/inspector.rpym:65
    old "Style"
    new ""

    # renpy/common/_developer/inspector.rpym:71
    old "Location"
    new ""

    # renpy/common/_developer/inspector.rpym:122
    old "Inspecting Styles of [displayable_name!q]"
    new ""

    # renpy/common/_developer/inspector.rpym:139
    old "displayable:"
    new ""

    # renpy/common/_developer/inspector.rpym:145
    old "        (no properties affect the displayable)"
    new ""

    # renpy/common/_developer/inspector.rpym:147
    old "        (default properties omitted)"
    new ""

    # renpy/common/_developer/inspector.rpym:185
    old "<repr() failed>"
    new ""

    # renpy/common/_layout/classic_load_save.rpym:170
    old "a"
    new ""

    # renpy/common/_layout/classic_load_save.rpym:179
    old "q"
    new ""

    # renpy/common/00iap.rpy:217
    old "Contacting App Store\nPlease Wait..."
    new ""

    # renpy/common/00updater.rpy:376
    old "The Ren'Py Updater is not supported on mobile devices."
    new ""

    # renpy/common/00updater.rpy:495
    old "An error is being simulated."
    new ""

    # renpy/common/00updater.rpy:679
    old "Either this project does not support updating, or the update status file was deleted."
    new ""

    # renpy/common/00updater.rpy:693
    old "This account does not have permission to perform an update."
    new ""

    # renpy/common/00updater.rpy:696
    old "This account does not have permission to write the update log."
    new ""

    # renpy/common/00updater.rpy:723
    old "Could not verify update signature."
    new ""

    # renpy/common/00updater.rpy:998
    old "The update file was not downloaded."
    new ""

    # renpy/common/00updater.rpy:1016
    old "The update file does not have the correct digest - it may have been corrupted."
    new ""

    # renpy/common/00updater.rpy:1072
    old "While unpacking {}, unknown type {}."
    new ""

    # renpy/common/00updater.rpy:1440
    old "Updater"
    new ""

    # renpy/common/00updater.rpy:1447
    old "An error has occured:"
    new ""

    # renpy/common/00updater.rpy:1449
    old "Checking for updates."
    new ""

    # renpy/common/00updater.rpy:1451
    old "This program is up to date."
    new ""

    # renpy/common/00updater.rpy:1453
    old "[u.version] is available. Do you want to install it?"
    new ""

    # renpy/common/00updater.rpy:1455
    old "Preparing to download the updates."
    new ""

    # renpy/common/00updater.rpy:1457
    old "Downloading the updates."
    new ""

    # renpy/common/00updater.rpy:1459
    old "Unpacking the updates."
    new ""

    # renpy/common/00updater.rpy:1461
    old "Finishing up."
    new ""

    # renpy/common/00updater.rpy:1463
    old "The updates have been installed. The program will restart."
    new ""

    # renpy/common/00updater.rpy:1465
    old "The updates have been installed."
    new ""

    # renpy/common/00updater.rpy:1467
    old "The updates were cancelled."
    new ""

    # renpy/common/00updater.rpy:1482
    old "Proceed"
    new ""

    # renpy/common/00gallery.rpy:590
    old "Image [index] of [count] locked."
    new ""

    # renpy/common/00gallery.rpy:610
    old "prev"
    new ""

    # renpy/common/00gallery.rpy:611
    old "next"
    new ""

    # renpy/common/00gallery.rpy:612
    old "slideshow"
    new ""

    # renpy/common/00gallery.rpy:613
    old "return"
    new ""

    # renpy/common/00gltest.rpy:66
    old "Renderer"
    new ""

    # renpy/common/00gltest.rpy:70
    old "Automatically Choose"
    new ""

    # renpy/common/00gltest.rpy:77
    old "Force ANGLE Renderer"
    new ""

    # renpy/common/00gltest.rpy:81
    old "Force GL Renderer"
    new ""

    # renpy/common/00gltest.rpy:85
    old "Force GLES Renderer"
    new ""

    # renpy/common/00gltest.rpy:90
    old "Force ANGLE2 Renderer"
    new ""

    # renpy/common/00gltest.rpy:94
    old "Force GL2 Renderer"
    new ""

    # renpy/common/00gltest.rpy:98
    old "Force GLES2 Renderer"
    new ""

    # renpy/common/00gltest.rpy:109
    old "Enable"
    new ""

    # renpy/common/00gltest.rpy:128
    old "Powersave"
    new ""

    # renpy/common/00gltest.rpy:142
    old "Framerate"
    new ""

    # renpy/common/00gltest.rpy:146
    old "Screen"
    new ""

    # renpy/common/00gltest.rpy:150
    old "60"
    new ""

    # renpy/common/00gltest.rpy:154
    old "30"
    new ""

    # renpy/common/00gltest.rpy:160
    old "Tearing"
    new ""

    # renpy/common/00gltest.rpy:176
    old "Changes will take effect the next time this program is run."
    new ""

    # renpy/common/00gltest.rpy:208
    old "Performance Warning"
    new ""

    # renpy/common/00gltest.rpy:213
    old "This computer is using software rendering."
    new ""

    # renpy/common/00gltest.rpy:215
    old "This computer has a problem displaying graphics: [problem]."
    new ""

    # renpy/common/00gltest.rpy:219
    old "Its graphics drivers may be out of date or not operating correctly. This can lead to slow or incorrect graphics display."
    new ""

    # renpy/common/00gltest.rpy:223
    old "Continue, Show this warning again"
    new ""

    # renpy/common/00gltest.rpy:227
    old "Continue, Don't show warning again"
    new ""

    # renpy/common/00gamepad.rpy:32
    old "Select Gamepad to Calibrate"
    new ""

    # renpy/common/00gamepad.rpy:35
    old "No Gamepads Available"
    new ""

    # renpy/common/00gamepad.rpy:54
    old "Calibrating [name] ([i]/[total])"
    new ""

    # renpy/common/00gamepad.rpy:58
    old "Press or move the [control!r] [kind]."
    new ""

    # renpy/common/00gamepad.rpy:66
    old "Skip (A)"
    new ""

    # renpy/common/00gamepad.rpy:69
    old "Back (B)"
    new ""

    # renpy/common/_errorhandling.rpym:540
    old "Open"
    new ""

    # renpy/common/_errorhandling.rpym:542
    old "Opens the traceback.txt file in a text editor."
    new ""

    # renpy/common/_errorhandling.rpym:544
    old "Copy BBCode"
    new ""

    # renpy/common/_errorhandling.rpym:546
    old "Copies the traceback.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."
    new ""

    # renpy/common/_errorhandling.rpym:548
    old "Copy Markdown"
    new ""

    # renpy/common/_errorhandling.rpym:550
    old "Copies the traceback.txt file to the clipboard as Markdown for Discord."
    new ""

    # renpy/common/_errorhandling.rpym:579
    old "An exception has occurred."
    new ""

    # renpy/common/_errorhandling.rpym:602
    old "Rollback"
    new ""

    # renpy/common/_errorhandling.rpym:604
    old "Attempts a roll back to a prior time, allowing you to save or choose a different choice."
    new ""

    # renpy/common/_errorhandling.rpym:607
    old "Ignore"
    new ""

    # renpy/common/_errorhandling.rpym:611
    old "Ignores the exception, allowing you to continue."
    new ""

    # renpy/common/_errorhandling.rpym:613
    old "Ignores the exception, allowing you to continue. This often leads to additional errors."
    new ""

    # renpy/common/_errorhandling.rpym:617
    old "Reload"
    new ""

    # renpy/common/_errorhandling.rpym:619
    old "Reloads the game from disk, saving and restoring game state if possible."
    new ""

    # renpy/common/_errorhandling.rpym:622
    old "Console"
    new ""

    # renpy/common/_errorhandling.rpym:624
    old "Opens a console to allow debugging the problem."
    new ""

    # renpy/common/_errorhandling.rpym:637
    old "Quits the game."
    new ""

    # renpy/common/_errorhandling.rpym:658
    old "Parsing the script failed."
    new ""

    # renpy/common/_errorhandling.rpym:684
    old "Opens the errors.txt file in a text editor."
    new ""

    # renpy/common/_errorhandling.rpym:688
    old "Copies the errors.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."
    new ""

    # renpy/common/_errorhandling.rpym:692
    old "Copies the errors.txt file to the clipboard as Markdown for Discord."
    new ""

