﻿# TODO: Translation updated at 2020-07-15 00:25

# game/Anorak Route.rpy:6
translate italian anorak_290fbf8c:

    # mc "Aquele rapaz parece ser gentil... Talvez ele aceite."
    mc ""

# game/Anorak Route.rpy:8
translate italian anorak_bde8af61:

    # mc "Merrii disse que ele frequenta a biblioteca, vai ser um bom lugar para conversar."
    mc ""

# game/Anorak Route.rpy:10
translate italian anorak_663e60d4:

    # mc "E eu preciso estudar pra prova, posso pegar o conteúdo por lá."
    mc ""

# game/Anorak Route.rpy:12
translate italian anorak_ac920b0d:

    # "Eu pego a minha mochila e vou em direção á biblioteca, passando pelos corredores de sala de aula."
    ""

# game/Anorak Route.rpy:14
translate italian anorak_913be8dc:

    # "~Biblioteca~"
    ""

# game/Anorak Route.rpy:16
translate italian anorak_985eeddb:

    # "Procurando o assunto na estante, percebo que a coleção é vasta demais. Vai demorar um tempo até achar o que preciso..."
    ""

# game/Anorak Route.rpy:18
translate italian anorak_ab8a76b3:

    # "Não é rígido demais uma escola ter estantes organizadas em ordem alfabética?..."
    ""

# game/Anorak Route.rpy:20
translate italian anorak_4a8eec86:

    # "Eu não teria tanta disposição pra organizar desse jeito. Talvez conseguisse com a ajuda das minhas amigas..."
    ""

# game/Anorak Route.rpy:22
translate italian anorak_26085bbc:

    # "Ou iríamos conversar o dia todo e deixar a bagunça completamente de lado."
    ""

# game/Anorak Route.rpy:24
translate italian anorak_bbbdffeb:

    # mc "Não é hora de sair pensando em organizações de estante, [povname]. Eu preciso procurar aquele maldito livro para estudar ou estou encrencada no teste."
    mc ""

# game/Anorak Route.rpy:26
translate italian anorak_98fdafc7:

    # who "Precisa de alguma ajuda?"
    who ""

# game/Anorak Route.rpy:28
translate italian anorak_140416ce:

    # "Uma voz sussurrada atrás de mim me paralisou por alguns segundos. Pensei que estaria sozinha nesse corredor!"
    ""

# game/Anorak Route.rpy:30
translate italian anorak_906fddc6:

    # "Eu falei comigo mesma em voz alta? Urgh! Eu preciso parar com esse costume ou as pessoas vão achar que eu sou maluca."
    ""

# game/Anorak Route.rpy:32
translate italian anorak_9d7363dd:

    # "Me virei para encontrar a pessoa que me fez a pergunta. Um garoto segurando um livro de capa marrom e título escrito em detalhes dourados me encarava sério, esperando uma suposta resposta."
    ""

# game/Anorak Route.rpy:34
translate italian anorak_ef665cb2:

    # "Eu devo estar demorando demais para falar com ele, julgando pelo jeito como ele me olha."
    ""

# game/Anorak Route.rpy:36
translate italian anorak_9ce77d5e:

    # mc "Uh- n-na verdade não. Mas obrigada pela ajuda..."
    mc ""

# game/Anorak Route.rpy:38
translate italian anorak_fe178959:

    # "Ele assentiu e se virou em direção à mesa perto de outras estantes, sentando em uma das cadeiras."
    ""

# game/Anorak Route.rpy:40
translate italian anorak_d2ce7668:

    # "E com a mesma expressão que me respondeu calado, abriu o livro direto na metade, como se estivesse marcando mentalmente a página que havia parado."
    ""

# game/Anorak Route.rpy:42
translate italian anorak_c884cc98:

    # "Coincidentemente, o assunto do livro do rapaz parecia ser o que eu precisava."
    ""

# game/Anorak Route.rpy:44
translate italian anorak_f55d8639:

    # "Procurando por um livro parecido nas estantes, achei um semelhante. E abrindo aproximadamente na parte em que ele estava, me deparei com o assunto."
    ""

# game/Anorak Route.rpy:46
translate italian anorak_8fea2db2:

    # mc "Hmm..."
    mc ""

# game/Anorak Route.rpy:48
translate italian anorak_246f6057:

    # "Ele parecia concentrado no que estava lendo, como se fosse uma obrigação ler aquilo. Talvez o assunto do teste dele não seja tão interessante assim."
    ""

# game/Anorak Route.rpy:50
translate italian anorak_f1a5afb7:

    # mc "Sobre o que é esse seu livro??"
    mc ""

# game/Anorak Route.rpy:52
translate italian anorak_7b7cfff5:

    # "O menino deu um saltinho da cadeira e suas faixas se esticaram como se também tivessem se assustado"
    ""

# game/Anorak Route.rpy:54
translate italian anorak_b369f6c4:

    # "Ele fez um sinal de silêncio em frente a sua boca, um -shh- mudo com certa irritação."
    ""

# game/Anorak Route.rpy:56
translate italian anorak_fc94dcc3:

    # who "Estamos em uma biblioteca."
    who ""

# game/Anorak Route.rpy:58
translate italian anorak_bbd25e58:

    # mc "Desculpa, não quis te assustar."
    mc ""

# game/Anorak Route.rpy:60
translate italian anorak_bde8b7b9:

    # "O garoto suspirou, desapontado com a minha atitude. Ele parece prezar muito pelo silêncio da biblioteca..."
    ""

# game/Anorak Route.rpy:62
translate italian anorak_fd49ba0b:

    # "Eu consegui ouvir um risinho baixo vindo da direção dele, e ele pareceu virar de leve o rosto como se quisesse esconder o sorriso"
    ""

# game/Anorak Route.rpy:64
translate italian anorak_8085fc43:

    # "Eu não poderia ver de qualquer forma, já que ele é todo enfaixado."
    ""

# game/Anorak Route.rpy:66
translate italian anorak_89a54983:

    # "Mesmo assim... Foi bem fofo."
    ""

# game/Anorak Route.rpy:68
translate italian anorak_46549bf8:

    # "Eu desviei a minha atenção para o livro do meu vizinho de mesa. Meus olhos passeiam pelas letras minúsculas da página grossa antes dele virar para a próxima página."
    ""

# game/Anorak Route.rpy:70
translate italian anorak_bf7904da:

    # "Ele não parece ligar para a minha presença, lendo cada palavra do livro com atenção."
    ""

# game/Anorak Route.rpy:72
translate italian anorak_3bb61e16:

    # "A página em questão tinha a foto de um besouro que parecia ter chifres."
    ""

# game/Anorak Route.rpy:74
translate italian anorak_a791407d:

    # "Não estudamos na mesma classe, então o professor dele deve estar dando uma matéria diferente. Nunca chegamos a ver esses besourinhos."
    ""

# game/Anorak Route.rpy:76
translate italian anorak_17662086:

    # "Confusa e com os pensamentos vindo um atrás do outro, não percebo quando ele deixa a atenção do livro e vira para me encarar."
    ""

# game/Anorak Route.rpy:78
translate italian anorak_82ed955c:

    # who "Escaravelhos."
    who ""

# game/Anorak Route.rpy:80
translate italian anorak_17fefa2d:

    # mc "O-oi?"
    mc ""

# game/Anorak Route.rpy:82
translate italian anorak_43d7d69d:

    # "Eu me afasto um pouco segurando o meu livro com mais força nas mãos, fingindo estar interessada nele"
    ""

# game/Anorak Route.rpy:84
translate italian anorak_49940c65:

    # who "Escaravelhos. É um livro de entomologia."
    who ""

# game/Anorak Route.rpy:86
translate italian anorak_2739b6a8:

    # mc "Anta-... O quê?"
    mc ""

# game/Anorak Route.rpy:88
translate italian anorak_6e978eeb:

    # "Ele revirou os olhos, suspirando. Deve achar que eu não notei pelas bandagens em seu rosto."
    ""

# game/Anorak Route.rpy:90
translate italian anorak_22761be7:

    # who "Estudo de insetos."
    who ""

# game/Anorak Route.rpy:92
translate italian anorak_16c653ad:

    # mc "Oh... E você se interessa por isso?"
    mc ""

# game/Anorak Route.rpy:94
translate italian anorak_51bed45b:

    # who "Um pouco. Preciso estudar isso avulso do conteúdo das aulas."
    who ""

# game/Anorak Route.rpy:96
translate italian anorak_39de79dc:

    # mc "Mesmo? Não está preocupado com as provas?"
    mc ""

# game/Anorak Route.rpy:98
translate italian anorak_72dfa318:

    # who "Não muito... O conteúdo do meu pai é 'mais importante', segundo ele."
    who ""

# game/Anorak Route.rpy:100
translate italian anorak_1d987202:

    # "Ele soltou mais um suspiro e trocou de página com certo desdém."
    ""

# game/Anorak Route.rpy:102
translate italian anorak_1f668ddb:

    # "Eu fixo o olhar nas palavras e figuras. O texto parou de ser sobre a parte anatômica dos besouros, mudando para feitiços e magias usando eles e a energia deles."
    ""

# game/Anorak Route.rpy:104
translate italian anorak_7477a87d:

    # mc "Parece ser interessante..."
    mc ""

# game/Anorak Route.rpy:106
translate italian anorak_2287d8b5:

    # who "E é. Mas o contexto nem tanto."
    who ""

# game/Anorak Route.rpy:108
translate italian anorak_197ceb6c:

    # mc "Como assim?"
    mc ""

# game/Anorak Route.rpy:110
translate italian anorak_b786170a:

    # "O rapaz fecha o livro, voltando sua atenção diretamente para mim."
    ""

# game/Anorak Route.rpy:112
translate italian anorak_b594ae85:

    # who "Eu sou filho do faraó Pekhrari. Sou Anorak, o príncipe herdeiro."
    who ""

# game/Anorak Route.rpy:114
translate italian anorak_62aea6ce:

    # a "Então tenho que estudar a cultura do meu povo - do meu reino, para poder governar."
    a ""

# game/Anorak Route.rpy:116
translate italian anorak_69d5db65:

    # a "Governar aos moldes do meu pai, que acha correto o que faz."
    a ""

# game/Anorak Route.rpy:118
translate italian anorak_946640fd:

    # mc "E você não parece achar tanto..."
    mc ""

# game/Anorak Route.rpy:120
translate italian anorak_cc9aef27:

    # a "Sim e não. Acho o modo como ele rege antiquado demais."
    a ""

# game/Anorak Route.rpy:122
translate italian anorak_ebea01df:

    # mc "Em questão de tecnologia ou cultural?"
    mc ""

# game/Anorak Route.rpy:124
translate italian anorak_f9062f62:

    # a "Ambos. Ele não aceita que os tempos são outros, e que uma mão menos rígida seria mais adequada."
    a ""

# game/Anorak Route.rpy:126
translate italian anorak_d5eb6f7c:

    # mc "Hmm... Mas você pode mudar isso quando for rei, não?"
    mc ""

# game/Anorak Route.rpy:128
translate italian anorak_cb0aa4bc:

    # "Ele cruza os braços, pensativo."
    ""

# game/Anorak Route.rpy:130
translate italian anorak_e8a3ffa3:

    # "Parando pra analisar ele, por baixo dessas ataduras ele parece ser uma pessoa bem interessante"
    ""

# game/Anorak Route.rpy:132
translate italian anorak_91fc7f2a:

    # "O cabelo azul é parcialmente escondido pelas faixas, e as orelhas levemente pontudas..."
    ""

# game/Anorak Route.rpy:134
translate italian anorak_ddf0c0ab:

    # "Os olhos vermelhos, fixos no livro, pensativo."
    ""

# game/Anorak Route.rpy:136
translate italian anorak_fe55e98e:

    # "Anorak é bem fofo. Espero que ele aceite ir comigo no baile."
    ""

# game/Anorak Route.rpy:138
translate italian anorak_d531cd13:

    # "Sua voz sai um pouco baixa, atrapalhando os meus pensamentos e acabando com o meu monólogo interno."
    ""

# game/Anorak Route.rpy:140
translate italian anorak_b2749e31:

    # a "Eu não... Tenho certeza se quero ser governante."
    a ""

# game/Anorak Route.rpy:142
translate italian anorak_3c2c2f9e:

    # mc "É uma decisão bem importante..."
    mc ""

# game/Anorak Route.rpy:144
translate italian anorak_83236d6a:

    # a "É sim. E eu não tenho maturidade pra pensar nisso ainda."
    a ""

# game/Anorak Route.rpy:146
translate italian anorak_98343016:

    # "Anorak suspira, voltando a prestar atenção no livro."
    ""

# game/Anorak Route.rpy:148
translate italian anorak_da5d4305:

    # a "Você deveria voltar a ler o seu livro. As provas são amanhã."
    a ""

# game/Anorak Route.rpy:150
translate italian anorak_eab62c19:

    # mc "Ah, a prova! Não lembrava!"
    mc ""

# game/Anorak Route.rpy:152
translate italian anorak_b5114841:

    # a "Shh. Fale baixinho."
    a ""

# game/Anorak Route.rpy:154
translate italian anorak_02233057:

    # mc "S-sim..."
    mc ""

# game/Anorak Route.rpy:156
translate italian anorak_9c033fa3:

    # "Com o rosto corado por elevar minha voz novamente, eu abro o livro pra esconder a minha vergonha."
    ""

# game/Anorak Route.rpy:158
translate italian anorak_e9aab348:

    # "Anorak sorri por baixo das faixas, focando em seu estudo. As ataduras escondem o sorriso, mas seus olhos não."
    ""

# game/Anorak Route.rpy:160
translate italian anorak_9b8464d5:

    # "Com o passar do tempo percebi alguns olhares em minha direção, vindas do rapaz ao meu lado. Não sei dizer se eram para mim ou para o meu livro..."
    ""

# game/Anorak Route.rpy:162
translate italian anorak_67f6e559:

    # "Eventualmente eu acabo me concentrando demais para trocar alguma palavra com ele"
    ""

# game/Anorak Route.rpy:164
translate italian anorak_016e1e0f:

    # "E assim ficamos até a biblioteca fechar. Nos damos um breve ~tchauzinho~ e cada um vai para a sua casa."
    ""

# game/Anorak Route.rpy:169
translate italian anorak_f766bbc8:

    # "Hoje cheguei mais cedo no colégio. Eu esperava que por ser dia de prova, os corredores ficassem mais cheios"
    ""

# game/Anorak Route.rpy:171
translate italian anorak_5d587c9b:

    # "Então achei que seria uma boa idéia revisar antes das aulas começarem."
    ""

# game/Anorak Route.rpy:173
translate italian anorak_ffeb5714:

    # "Minha sala de aula é a melhor opção. Alguns alunos costumam se desesperar e correr para a biblioteca, na esperança de que algum aluno com boas notas consiga os ensinar nos ultimos segundos restantes."
    ""

# game/Anorak Route.rpy:175
translate italian anorak_a73c03e7:

    # "O conteúdo é bem básico... Me impressionou que o rapaz de ontem estivesse tão preso assim na leitura."
    ""

# game/Anorak Route.rpy:177
translate italian anorak_2ecff6b7:

    # "Insetos. Não é exatamente o que eu esperava estudar nesse semestre, mas o Stein disse que eles são super importantes para nós."
    ""

# game/Anorak Route.rpy:179
translate italian anorak_b953d900:

    # "E o livro não é como os outros da biblioteca. Não são folhas e mais folhas dizendo chatices."
    ""

# game/Anorak Route.rpy:181
translate italian anorak_b81eb25e:

    # "Eu acho que posso acabar gostando do assunto."
    ""

# game/Anorak Route.rpy:183
translate italian anorak_86b6caf2:

    # "Enquanto eu caminho pelo corredor eu lembro daquele daquele rapaz. Ele não é exatamente quieto, mas é bem misterioso."
    ""

# game/Anorak Route.rpy:185
translate italian anorak_92e7695e:

    # "Quero muito que ele aceite ir comigo no baile..."
    ""

# game/Anorak Route.rpy:187
translate italian anorak_c3c5314b:

    # "É um pouco improvável ele não ter par, por ser príncipe. Ele já deve ter alguém para ir."
    ""

# game/Anorak Route.rpy:189
translate italian anorak_9eef9951:

    # "Um esbarro no meu braço balança os livros das minhas mãos e quase os derruba, me fazendo perder completamente a linha de pensamento."
    ""

# game/Anorak Route.rpy:191
translate italian anorak_33f0f9ed:

    # "Enquanto eu recobrava o equilíbrio perdido após a pessoa esbarrar em mim, ela se aproximava, fazendo um gesto cauteloso"
    ""

# game/Anorak Route.rpy:193
translate italian anorak_95a62e22:

    # "Ela segura meu braço, ajudando a me manter em pé junto com os livros. Eu reconheço essa mão"
    ""

# game/Anorak Route.rpy:195
translate italian anorak_e94b431f:

    # "Meus olhos reconhecem algumas bandagens que cobrem o corpo da pessoa a minha frente"
    ""

# game/Anorak Route.rpy:197
translate italian anorak_c0109970:

    # "De todas as pessoas para passar por mim, justo ele."
    ""

# game/Anorak Route.rpy:199
translate italian anorak_f1ddece7:

    # "Justo quem eu queria ver agora."
    ""

# game/Anorak Route.rpy:201
translate italian anorak_e2e091d9:

    # a "Oh. Perdão, não quis incomodar você."
    a ""

# game/Anorak Route.rpy:203
translate italian anorak_9d067042:

    # "Vendo o quão próximos estamos logo ele se recolhe, cruzando os braços"
    ""

# game/Anorak Route.rpy:205
translate italian anorak_d9046150:

    # "Nos encontramos novamente com ele quase me matando."
    ""

# game/Anorak Route.rpy:207
translate italian anorak_d60436c1:

    # mc "Não foi nada, não. Eu tô bem."
    mc ""

# game/Anorak Route.rpy:209
translate italian anorak_d44c66a2:

    # "Eu forço um sorrisinho, escondendo o susto do baque repentino"
    ""

# game/Anorak Route.rpy:211
translate italian anorak_12085720:

    # "Ele suspirou aliviado, fazendo parecer que quase me derrubou de um lugar alto."
    ""

# game/Anorak Route.rpy:213
translate italian anorak_6c492594:

    # a "Ótimo, isso é bom. Eu deveria ter mais cuidado..."
    a ""

# game/Anorak Route.rpy:215
translate italian anorak_709e73a4:

    # "Anorak sorri por baixo das suas bandagens. Só consigo ver seus olhos por trás delas, diminuindo até chegar em uma linha fina."
    ""

# game/Anorak Route.rpy:217
translate italian anorak_bdf65664:

    # "Ele é bem fofo."
    ""

# game/Anorak Route.rpy:219
translate italian anorak_95c60c73:

    # "Imagino o que teria atrás dessas bandagens todas..."
    ""

# game/Anorak Route.rpy:221
translate italian anorak_351c99e9:

    # "Anorak permanece imóvel, mas agora está olhando para algo nas minhas mãos, observando."
    ""

# game/Anorak Route.rpy:223
translate italian anorak_33ff56fb:

    # "Acho que reconheceu o que eu estava lendo."
    ""

# game/Anorak Route.rpy:225
translate italian anorak_91a61340:

    # a "O livro que eu peguei ontem?"
    a ""

# game/Anorak Route.rpy:227
translate italian anorak_0e061dc5:

    # "Ele me olha nos olhos, me fazendo tremer de leve."
    ""

# game/Anorak Route.rpy:229
translate italian anorak_ec1f0c8b:

    # "Ele não é nem um pouco ameaçador, mas quando me olha diretamente..."
    ""

# game/Anorak Route.rpy:231
translate italian anorak_35520313:

    # "Os olhos vermelhos dele são penetrantes."
    ""

# game/Anorak Route.rpy:233
translate italian anorak_c47d7139:

    # mc "S-sim... O teste é daqui a pouco."
    mc ""

# game/Anorak Route.rpy:235
translate italian anorak_6560e2c1:

    # a "Hm. Boa sorte, acho que vai precisar."
    a ""

# game/Anorak Route.rpy:237
translate italian anorak_17ee6250:

    # "Ele não parece ser de muita conversa. Não é que ele tente ser seco, é só..."
    ""

# game/Anorak Route.rpy:239
translate italian anorak_1bc7fdac:

    # "Talvez não saiba como conversar?"
    ""

# game/Anorak Route.rpy:241
translate italian anorak_91f120e3:

    # "Talvez seja tímido?"
    ""

# game/Anorak Route.rpy:243
translate italian anorak_dcd00715:

    # mc "Obrigada. É bom um pouco de sorte, mas vai ser moleza. Entomologia é divertido de ler."
    mc ""

# game/Anorak Route.rpy:245
translate italian anorak_61154876:

    # "Os olhos do rapaz brilharam ao ouvir o que eu disse. Parecia interessado pelo caminho que a conversa estava tomando."
    ""

# game/Anorak Route.rpy:247
translate italian anorak_3661ab1f:

    # a "Você não parecia conhecer muito de entomologia ontem. Esse assunto te interessa?"
    a ""

# game/Anorak Route.rpy:249
translate italian anorak_dc4e2937:

    # mc "É fascinante conhecer os bichinhos que existem por aí."
    mc ""

# game/Anorak Route.rpy:251
translate italian anorak_e934674f:

    # mc "E escaravelhos são bonitinhos e intrigantes."
    mc ""

# game/Anorak Route.rpy:253
translate italian anorak_b602c25a:

    # a "Escaravelhos são sagrados para a minha cultura..."
    a ""

# game/Anorak Route.rpy:255
translate italian anorak_44b5bc04:

    # "Ele sorriu mais uma vez, tentando esconder os olhos fechados com o cabelo. Meu coração palpitou forte, e eu não pude deixar de esconder um rubor."
    ""

# game/Anorak Route.rpy:257
translate italian anorak_1185c188:

    # "Eu aperto um pouco mais o livro, tentando me tirar desse transe."
    ""

# game/Anorak Route.rpy:259
translate italian anorak_48c127ef:

    # a "Que tolice..."
    a ""

# game/Anorak Route.rpy:261
translate italian anorak_06109310:

    # "Meu rosto esquentou. Ele estava falando de mim? O que ele quis dizer?"
    ""

# game/Anorak Route.rpy:263
translate italian anorak_f6be6ee0:

    # "Eu estar parada na sua frente não ajuda. Eu devo estar parecendo uma idiota."
    ""

# game/Anorak Route.rpy:265
translate italian anorak_207f1514:

    # "Ele balançou a cabeça, levando uma das suas mãos para a parte de trás da sua cabeça e coçando de leve."
    ""

# game/Anorak Route.rpy:267
translate italian anorak_2e573ca4:

    # a "...Como pude esquecer de me apresentar devidamente a você?"
    a ""

# game/Anorak Route.rpy:269
translate italian anorak_5494679c:

    # mc "Ah. Era isso."
    mc ""

# game/Anorak Route.rpy:271
translate italian anorak_98cb8871:

    # a "Isso?"
    a ""

# game/Anorak Route.rpy:273
translate italian anorak_0ad4277d:

    # mc "N-nada. Eu pensei que você tinha me chamado de tola."
    mc ""

# game/Anorak Route.rpy:275
translate italian anorak_4309a204:

    # a "Você é tola."
    a ""

# game/Anorak Route.rpy:277
translate italian anorak_f2316b97:

    # mc "Ah."
    mc ""

# game/Anorak Route.rpy:279
translate italian anorak_00834dd2:

    # "O sangue subiu tão rápido ao meu rosto, deixando ele completamente vermelho."
    ""

# game/Anorak Route.rpy:281
translate italian anorak_fd71e1c8:

    # "Anorak solta um risinho baixo, estendendo a mão para mim."
    ""

# game/Anorak Route.rpy:283
translate italian anorak_5e410a11:

    # a "Eu me chamo Anorak."
    a ""

# game/Anorak Route.rpy:285
translate italian anorak_884619e7:

    # "Eu assenti, retribuindo com um sorriso gentil, segurando em sua mão."
    ""

# game/Anorak Route.rpy:287
translate italian anorak_5f019469:

    # mc "Eu sou a [povname]..."
    mc ""

# game/Anorak Route.rpy:289
translate italian anorak_009a5fb7:

    # a "Eu sei o seu nome. Mesmo assim, é um prazer, [povname]."
    a ""

# game/Anorak Route.rpy:291
translate italian anorak_eabfef28:

    # mc "Você sabe o meu nome?"
    mc ""

# game/Anorak Route.rpy:293
translate italian anorak_2c362dce:

    # a "É... Eu ouço as suas amigas te chamando assim e... Bom, te vejo depois das provas?"
    a ""

# game/Anorak Route.rpy:295
translate italian anorak_0b90639f:

    # mc "É verdade, o teste! Preciso ir, mais tarde a gente se encontra!"
    mc ""

# game/Anorak Route.rpy:297
translate italian anorak_c1838fce:

    # "Eu retomo meu caminho sem esperar por uma resposta. O Stein não ia me deixar entrar se chegasse atrasada."
    ""

# game/Anorak Route.rpy:299
translate italian anorak_2951b546:

    # "Eu me viro para trás, já com uma certa distância do rapaz."
    ""

# game/Anorak Route.rpy:301
translate italian anorak_fd84ba83:

    # mc "Quem sabe não podemos conversar mais sobre os insetinhos algum outro dia?"
    mc ""

# game/Anorak Route.rpy:303
translate italian anorak_ef344212:

    # "Anorak arqueou uma sobrancelha, um leve vermelho surgiu em suas bochechas e a sua expressão fazia parecer curioso sobre o que falei."
    ""

# game/Anorak Route.rpy:305
translate italian anorak_8b7a8c59:

    # a "Está me convidando para alguma coisa?"
    a ""

# game/Anorak Route.rpy:307
translate italian anorak_2b5ecb3e:

    # "...Realmente soou como um convite para um encontro."
    ""

# game/Anorak Route.rpy:309
translate italian anorak_158b9ad6:

    # "E é exatamente o que eu quero com ele. Quero convidar para o baile!"
    ""

# game/Anorak Route.rpy:311
translate italian anorak_47eb0949:

    # mc "Me encontra no pátio depois das provas. Talvez eu tenha te convidado mesmo."
    mc ""

# game/Anorak Route.rpy:313
translate italian anorak_fbed8607:

    # a "Você não tem certeza, mesmo pedindo?"
    a ""

# game/Anorak Route.rpy:315
translate italian anorak_2b35b6fe:

    # mc "..."
    mc ""

# game/Anorak Route.rpy:317
translate italian anorak_45dc34ec:

    # "Eu desviei o olhar, não sabendo como responder."
    ""

# game/Anorak Route.rpy:319
translate italian anorak_39831405:

    # mc "N-não..."
    mc ""

# game/Anorak Route.rpy:321
translate italian anorak_8f503702:

    # "Ele riu, assentindo com a cabeça"
    ""

# game/Anorak Route.rpy:323
translate italian anorak_72c918a2:

    # a "Certo. Até mais tarde, Núbia."
    a ""

# game/Anorak Route.rpy:325
translate italian anorak_e1235eea:

    # "Anorak acenou e continuou o seu caminho."
    ""

# game/Anorak Route.rpy:327
translate italian anorak_e97e3028:

    # "Ele me chamou de quê? N-núbia?"
    ""

# game/Anorak Route.rpy:329
translate italian anorak_2ade4026:

    # "Deve significar alguma coisa na língua dele. Espero que não seja algo ruim ou bobo..."
    ""

# game/Anorak Route.rpy:331
translate italian anorak_92fa0760:

    # mc "A prova!"
    mc ""

# game/Anorak Route.rpy:333
translate italian anorak_8a7ca2a2:

    # "Correr para a sala é a minha única opção agora. Stein já deve estar entrando."
    ""

# game/Anorak Route.rpy:335
translate italian anorak_d57fc71f:

    # "Agora não tenho só a prova para me preocupar..."
    ""

# game/Anorak Route.rpy:337
translate italian anorak_48452b8b:

    # "Eu convidei mesmo ele pra sair!"
    ""

# game/Anorak Route.rpy:339
translate italian anorak_5e925dc3:

    # "Não foi um pedido concreto, então vou ter que esperar após as provas para ter certeza da resposta."
    ""

# game/Anorak Route.rpy:341
translate italian anorak_1756713c:

    # "Espero que seja positiva..."
    ""

# game/Anorak Route.rpy:343
translate italian anorak_d696615a:

    # "~Após a prova~"
    ""

# game/Anorak Route.rpy:345
translate italian anorak_8c2cb179:

    # "Sai da sala ás pressas. Mal consegui arrumar meu material e já estava no corredor."
    ""

# game/Anorak Route.rpy:347
translate italian anorak_ca3d367a:

    # "Eu preciso saber da resposta dele. Preciso saber se ele não estava me fazendo de boba..."
    ""

# game/Anorak Route.rpy:349
translate italian anorak_f1c42ea5:

    # "Corri até o pátio, passando por várias pessoas. Ele já deve estar vindo."
    ""

# game/Anorak Route.rpy:351
translate italian anorak_612053bd:

    # "Sentado em um banco, observando as pessoas passarem. Lá estava Anorak."
    ""

# game/Anorak Route.rpy:353
translate italian anorak_71e98572:

    # mc "Okay... Respira fundo e vai lá."
    mc ""

# game/Anorak Route.rpy:355
translate italian anorak_0b66864d:

    # "Eu seguro minha bolsa com mais força, camInhando até ele."
    ""

# game/Anorak Route.rpy:357
translate italian anorak_d328a2c6:

    # "Ele parecia em transe, perdido em pensamentos. Não notou quando me aproximei, sentando em seu lado."
    ""

# game/Anorak Route.rpy:359
translate italian anorak_bbb7ef69:

    # mc "Oi Anorak. Tudo bem?"
    mc ""

# game/Anorak Route.rpy:361
translate italian anorak_971dd478:

    # "Anorak se virou para mim, me olhando por um tempo antes de ter alguma reação."
    ""

# game/Anorak Route.rpy:363
translate italian anorak_b2d39858:

    # a "Oh. [povname]! Você veio!"
    a ""

# game/Anorak Route.rpy:365
translate italian anorak_1ff9340d:

    # mc "Heheh. Você estava distraído."
    mc ""

# game/Anorak Route.rpy:367
translate italian anorak_284b1182:

    # a "Estava pensando enquanto esperava você..."
    a ""

# game/Anorak Route.rpy:369
translate italian anorak_c6ed6e2d:

    # mc "Eu te fiz esperar muito?"
    mc ""

# game/Anorak Route.rpy:371
translate italian anorak_7c9592e1:

    # "Eu me ajeito no banco, encolhendo as pernas para perto uma da outra."
    ""

# game/Anorak Route.rpy:373
translate italian anorak_1afa2027:

    # a "Não. Na verdade nem vi o tempo passar. O que me fez ficar assim foi o seu pedido hoje mais cedo..."
    a ""

# game/Anorak Route.rpy:375
translate italian anorak_cdbab0da:

    # mc "A-ah... Eu te chamei pra sair né?..."
    mc ""

# game/Anorak Route.rpy:377
translate italian anorak_edabd01d:

    # a "Aquilo foi sincero? Você quer mesmo sair comigo?"
    a ""

# game/Anorak Route.rpy:379
translate italian anorak_4b7ec490:

    # mc "H-hmm... Digo, sim?"
    mc ""

# game/Anorak Route.rpy:381
translate italian anorak_cf21492b:

    # "Ele se vira em minha direção e me encara nos olhos"
    ""

# game/Anorak Route.rpy:383
translate italian anorak_deaa7461:

    # "Com os seus olhos sendo a única coisa aparente em seu rosto, eles ficam um pouco intimidadores de perto."
    ""

# game/Anorak Route.rpy:385
translate italian anorak_708adccd:

    # "Anorak parece estar procurando por alguma coisa no meu rosto. Parece desconfiado, ou com medo..."
    ""

# game/Anorak Route.rpy:387
translate italian anorak_9beb3911:

    # "Ficamos assim por alguns segundos, até ele desviar o olhar e puxar uma das faixas do seu braço direito."
    ""

# game/Anorak Route.rpy:389
translate italian anorak_ac4ba934:

    # a "Tudo bem. Se você quer... Podemos nos ver."
    a ""

# game/Anorak Route.rpy:391
translate italian anorak_f206e72d:

    # mc "Mesmo? Quando?"
    mc ""

# game/Anorak Route.rpy:393
translate italian anorak_18c9e640:

    # "Eu esboço um sorriso, me apoiando com as mãos no banco e me aproximando dele"
    ""

# game/Anorak Route.rpy:395
translate italian anorak_784b19b4:

    # "Anorak não consegue esconder o pulinho, evitando me olhar."
    ""

# game/Anorak Route.rpy:397
translate italian anorak_2173be0b:

    # a "A-amanhã... Se estiver tudo bem pra você."
    a ""

# game/Anorak Route.rpy:399
translate italian anorak_f4bac734:

    # mc "Por mim ótimo! Me dá seu número e eu te mando uma mensagem mais tarde~"
    mc ""

# game/Anorak Route.rpy:401
translate italian anorak_08173652:

    # a "C-certo..."
    a ""

# game/Anorak Route.rpy:403
translate italian anorak_c8528cd1:

    # "Ele pega o meu celular e digita os números, salvando seu contato."
    ""

# game/Anorak Route.rpy:405
translate italian anorak_ca1d3163:

    # "Durante todo o processo ele evitou ao máximo olhar para mim"
    ""

# game/Anorak Route.rpy:407
translate italian anorak_57c5eb50:

    # "Eu consigo ver o vermelho do rosto dele se expandir para as orelhas pontudas."
    ""

# game/Anorak Route.rpy:409
translate italian anorak_d328e2e8:

    # a "Eu... Eu preciso ir indo."
    a ""

# game/Anorak Route.rpy:411
translate italian anorak_a64552f3:

    # "Anorak se levanta, estendendo a mão para mim. Seu olhar fixo em mim, ainda com as bochechas coradas."
    ""

# game/Anorak Route.rpy:413
translate italian anorak_a89741b7:

    # mc "Oh... Obrigada."
    mc ""

# game/Anorak Route.rpy:415
translate italian anorak_c2d68a2b:

    # "Eu pego sua mão, levantando do banco."
    ""

# game/Anorak Route.rpy:417
translate italian anorak_5b226089:

    # "Ele sorri pra mim, levemente deixando minha mão escapar da dele."
    ""

# game/Anorak Route.rpy:419
translate italian anorak_f32582da:

    # a "Te vejo amanhã, [povname]?"
    a ""

# game/Anorak Route.rpy:421
translate italian anorak_6002d209:

    # mc "S-sim... Amanhã..."
    mc ""

# game/Anorak Route.rpy:423
translate italian anorak_02062524:

    # "Eu sorrio de volta, vendo ele se dirigir até a entrada do colégio e desaparecer em uma charrete"
    ""

# game/Anorak Route.rpy:425
translate italian anorak_81ff86b9:

    # "Logo eu chego em casa e jogo minhas coisas na cama. Puxando o celular para conversar no chat com as meninas, vejo o seu contato salvo no meu celular."
    ""

# game/Anorak Route.rpy:427
translate italian anorak_78e41693:

    # "Ele salvou com um coraçãozinho..."
    ""

# game/Anorak Route.rpy:429
translate italian anorak_28633a35:

    # "Não consigo conter um sorriso."
    ""

# game/Anorak Route.rpy:431
translate italian anorak_77ac12e1:

    # "Depois de conversar com elas, as meninas me encorajaram a chamar ele para conversar."
    ""

# game/Anorak Route.rpy:433
translate italian anorak_722e430e:

    # "Combinamos de nos encontrar na frente do museu amanhã. É um lugar calmo e com várias coisas interessantes para vermos."
    ""

# game/Anorak Route.rpy:435
translate italian anorak_0826a9bd:

    # "Tenho a impressão de que não vou conseguir dormir hoje."
    ""

