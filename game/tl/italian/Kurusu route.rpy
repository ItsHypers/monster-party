﻿# TODO: Translation updated at 2020-07-15 00:25

# game/Kurusu route.rpy:6
translate italian kurusu_7f65ea29:

    # mc "Kurusu deve estar na sala!"
    mc ""

# game/Kurusu route.rpy:8
translate italian kurusu_68a4541e:

    # "Segurando a minha mochila pelos ombros, corri até a sala do Kurusu. Quando cheguei, ele estava terminando de arrumar seu material."
    ""

# game/Kurusu route.rpy:10
translate italian kurusu_33b56670:

    # mc "Kukki~!"
    mc ""

# game/Kurusu route.rpy:12
translate italian kurusu_518d1d5f:

    # "Eu o chamei, acenando."
    ""

# game/Kurusu route.rpy:14
translate italian kurusu_f9e813a7:

    # k "[povname]."
    k ""

# game/Kurusu route.rpy:16
translate italian kurusu_d77a72b9:

    # "Ele se virou, com um sorrisinho."
    ""

# game/Kurusu route.rpy:18
translate italian kurusu_483eeb1c:

    # k "Que bom te ver. Ainda não foi pra casa?"
    k ""

# game/Kurusu route.rpy:20
translate italian kurusu_6f61d9d6:

    # mc "Não... Na verdade estava esperando por você."
    mc ""

# game/Kurusu route.rpy:22
translate italian kurusu_e055fc22:

    # "Eu dou um sorrisinho me apoiando na porta, esperando ele terminar de se aprontar."
    ""

# game/Kurusu route.rpy:24
translate italian kurusu_a0112c45:

    # k "Por mim? Mas que honra. *ri*"
    k ""

# game/Kurusu route.rpy:26
translate italian kurusu_f7ff1f3a:

    # mc "Para. *ri* Eu queria passar um tempo com você~"
    mc ""

# game/Kurusu route.rpy:28
translate italian kurusu_3099ed46:

    # mc "Isso é, se não for incômodo..."
    mc ""

# game/Kurusu route.rpy:30
translate italian kurusu_292e8cca:

    # k "Não, não é incômodo."
    k ""

# game/Kurusu route.rpy:32
translate italian kurusu_96416e89:

    # "Kurusu passou o braço pelo meu pescoço, ainda sorrindo."
    ""

# game/Kurusu route.rpy:34
translate italian kurusu_f27cb8c1:

    # k "Então que tal irmos para o pátio? Com certeza não vão nos incomodar lá."
    k ""

# game/Kurusu route.rpy:36
translate italian kurusu_f0324398:

    # mc "Ooh~ O seu cantinho secreto? Vamos~!"
    mc ""

# game/Kurusu route.rpy:38
translate italian kurusu_00a71ec0:

    # "Kurusu tira o braço do meu ombro, deixando a área com uma sensação de vazio."
    ""

# game/Kurusu route.rpy:40
translate italian kurusu_f2fc0084:

    # "Nós nos dirigimos pelo pátio. O intervalo sempre junta os alunos em todas as áreas do colégio."
    ""

# game/Kurusu route.rpy:42
translate italian kurusu_4e3ad19b:

    # "Passando pelo ginásio, conseguimos ouvir o grito de vários alunos"
    ""

# game/Kurusu route.rpy:44
translate italian kurusu_5b039826:

    # "O time de basquete deve estar jogando."
    ""

# game/Kurusu route.rpy:46
translate italian kurusu_11372a93:

    # k "Heh. Deve ser divertido..."
    k ""

# game/Kurusu route.rpy:48
translate italian kurusu_2dea122d:

    # mc "O que, Kukki?"
    mc ""

# game/Kurusu route.rpy:50
translate italian kurusu_3022fb80:

    # k "Jogar... Praticar esportes com o pessoal no ginásio."
    k ""

# game/Kurusu route.rpy:52
translate italian kurusu_125c2134:

    # "Kurusu suspira e, ao ver a minha expressão de preocupação, solta um risinho"
    ""

# game/Kurusu route.rpy:54
translate italian kurusu_4a2354c1:

    # k "Não é nada, [povname]. Não precisa se preocupar tanto~"
    k ""

# game/Kurusu route.rpy:56
translate italian kurusu_0a541e00:

    # mc "Eu não disse nada~"
    mc ""

# game/Kurusu route.rpy:58
translate italian kurusu_652db694:

    # "Ele tem o costume de se preocupar demais comigo e não deixar que eu faça o mesmo."
    ""

# game/Kurusu route.rpy:60
translate italian kurusu_5ac93450:

    # "É um pouco frustrante, mas não quero abusar"
    ""

# game/Kurusu route.rpy:62
translate italian kurusu_e2b8efcf:

    # "Ele já me deixa ter contato com ele, e se ele estiver bem com isso, já é o suficiente pra mim."
    ""

# game/Kurusu route.rpy:64
translate italian kurusu_a5e6eb94:

    # "Chegando numa clareira, Kurusu sinaliza para um caminho semi-morto no meio dos arbustos."
    ""

# game/Kurusu route.rpy:66
translate italian kurusu_0a10b940:

    # "Ele me disse que as vezes que vem pra cá, acaba sem querer machucando um ou dois ramos..."
    ""

# game/Kurusu route.rpy:68
translate italian kurusu_52d4a297:

    # "Enquanto ele entra no arbusto, eu faço um carinho gentil na parte seca. Ele parece notar, mas não dá muita bola."
    ""

# game/Kurusu route.rpy:70
translate italian kurusu_e073ee22:

    # "Passando pelo arbusto, ele dá lugar para um caminho fechado com algumas flores mistas desabrochando."
    ""

# game/Kurusu route.rpy:72
translate italian kurusu_0bccb5e0:

    # mc "Eu sempre fico impressionada com esse lugar... É muito lindo."
    mc ""

# game/Kurusu route.rpy:74
translate italian kurusu_8ce377e8:

    # "Eu sorrio, apreciando a beleza do lugar. É quieto demais para um colégio."
    ""

# game/Kurusu route.rpy:76
translate italian kurusu_7335a99b:

    # k "Minhas melhores sonecas foram aqui, é um lugar muito bom pra dormir..."
    k ""

# game/Kurusu route.rpy:78
translate italian kurusu_0d75d13d:

    # k "Embora as flores tenham um cheiro um pouco forte pro meu gosto."
    k ""

# game/Kurusu route.rpy:80
translate italian kurusu_49d3045d:

    # "Eu me abaixo pra sentir o aroma de uma das flores. Ela é pequena e, embora grande parte delas sejam brancas, haviam algumas roxas, rosas e até algumas laranjas por perto. É doce, tipo mel."
    ""

# game/Kurusu route.rpy:82
translate italian kurusu_2cfa6801:

    # k "Álisso."
    k ""

# game/Kurusu route.rpy:84
translate italian kurusu_6d6be801:

    # "Kurusu sorri, sentando no chão e observando a cena."
    ""

# game/Kurusu route.rpy:86
translate italian kurusu_30f8830f:

    # "Ele tem essa mania de prestar atenção a detalhes. Sempre observando, sempre quieto. Sempre admirando."
    ""

# game/Kurusu route.rpy:88
translate italian kurusu_680c4f9c:

    # "Rubor cobre as minhas bochechas, vendo como ele me encara dessa forma. Tão bobo~"
    ""

# game/Kurusu route.rpy:90
translate italian kurusu_d253fd66:

    # mc "São lindas..."
    mc ""

# game/Kurusu route.rpy:92
translate italian kurusu_9f9feb56:

    # k "São sim, mas o cheiro..."
    k ""

# game/Kurusu route.rpy:94
translate italian kurusu_1ff18aa5:

    # mc "Eu não acho... É bem agradável na verdade~"
    mc ""

# game/Kurusu route.rpy:96
translate italian kurusu_8146ceff:

    # k "...Pena eu não poder tocar muito nelas."
    k ""

# game/Kurusu route.rpy:98
translate italian kurusu_ad8cd9c2:

    # "Eu me aproximo dele, olhando para suas mãos."
    ""

# game/Kurusu route.rpy:100
translate italian kurusu_e1c0fab0:

    # mc "Você se refere ao..."
    mc ""

# game/Kurusu route.rpy:102
translate italian kurusu_0780eff8:

    # k "Ao sangue da minha mãe, sim."
    k ""

# game/Kurusu route.rpy:104
translate italian kurusu_40d0ae05:

    # "Oh. Kurusu é filho da Senhora Morte. Ela mata tudo o que toca, e ele como herdeiro..."
    ""

# game/Kurusu route.rpy:106
translate italian kurusu_82f1c7eb:

    # mc "Você andava treinando controlar isso, não estava? Como tá indo? Alguma melhora?"
    mc ""

# game/Kurusu route.rpy:108
translate italian kurusu_c5865d3d:

    # k "Eu... Consigo tocar algumas coisas antes de apodrecer elas... Mas são breves segundos."
    k ""

# game/Kurusu route.rpy:110
translate italian kurusu_fac0d2e3:

    # "Kurusu suspira, levantando e pegando uma Álisso do chão."
    ""

# game/Kurusu route.rpy:114
translate italian kurusu_72e2fc73:

    # mc "Kurusu..."
    mc ""

# game/Kurusu route.rpy:116
translate italian kurusu_7c6266de:

    # "Kurusu suspira novamente e senta do meu lado, fazendo carinho em uma das minhas mechas de cabelo."
    ""

# game/Kurusu route.rpy:118
translate italian kurusu_a9b456ae:

    # k "Tá tudo bem [povname]. Um dia eu terei controle dessa situação, e você será a primeira a sentir o meu abraço apertado."
    k ""

# game/Kurusu route.rpy:120
translate italian kurusu_2e8798ad:

    # "Eu sorrio, abraçando ele por alguns instantes."
    ""

# game/Kurusu route.rpy:122
translate italian kurusu_3d8bc728:

    # mc "Hihi~ Eu vou estar mais que honrada em aceitar Kukki."
    mc ""

# game/Kurusu route.rpy:124
translate italian kurusu_371f7cc0:

    # k "Huhu~"
    k ""

# game/Kurusu route.rpy:126
translate italian kurusu_68d0fee5:

    # "Eu desfaço o abraço, me ajeitando na grama. Ele parece um pouco nervoso com o que eu fiz, mas isso é normal..."
    ""

# game/Kurusu route.rpy:128
translate italian kurusu_9813508b:

    # k "Então, como vai a busca pelo par ideal?"
    k ""

# game/Kurusu route.rpy:130
translate italian kurusu_d3b26143:

    # "Eu consigo ouvir ele apertando o tecido da roupa, evitando olhar nos meus olhos."
    ""

# game/Kurusu route.rpy:132
translate italian kurusu_3e57d3ab:

    # mc "Você não vai mesmo pensar em ir comigo, né?..."
    mc ""

# game/Kurusu route.rpy:134
translate italian kurusu_cd684b87:

    # k "Digamos que eu prefiro ser sua última opção, não a primeira."
    k ""

# game/Kurusu route.rpy:136
translate italian kurusu_49f0fc66:

    # "Kurusu sorri, ainda evitando de me olhar diretamente."
    ""

# game/Kurusu route.rpy:138
translate italian kurusu_c1f4b709:

    # "Ele deve estar nervoso com o assunto, por ter mencionado sem que eu tocasse no assunto..."
    ""

# game/Kurusu route.rpy:140
translate italian kurusu_79610dc6:

    # "Eu lembro de ter combinado com as meninas de começar o plano hoje, ele deve ter ouvido isso"
    ""

# game/Kurusu route.rpy:142
translate italian kurusu_ba2869b4:

    # "Mas acho que ele não espera que eu tenha escolhido ele"
    ""

# game/Kurusu route.rpy:144
translate italian kurusu_e8ce66dc:

    # "Ele é o meu melhor amigo, não teria graça ir sem ele."
    ""

# game/Kurusu route.rpy:146
translate italian kurusu_6bcbc197:

    # mc "Eu gosto de você, Kukki. Gostaria muito que fosse... Pensa um pouquinho nisso vai, por mim..."
    mc ""

# game/Kurusu route.rpy:148
translate italian kurusu_12843bfc:

    # "A minha pressão psicológica deve funcionar nele, já que eu consigo ver as orelhas dele ganhando cor."
    ""

# game/Kurusu route.rpy:150
translate italian kurusu_789735c0:

    # mc "Awn. Tá vermelhinho é?~"
    mc ""

# game/Kurusu route.rpy:152
translate italian kurusu_0a0142fd:

    # k "Cala a boca. Não foi nada..."
    k ""

# game/Kurusu route.rpy:154
translate italian kurusu_51d8f220:

    # "Ele vira o rosto tentando esconder a vermelhidez, mas a sua pele é tão pálida que a cor preenche o lugar inexistente da melanina."
    ""

# game/Kurusu route.rpy:156
translate italian kurusu_f1f0f684:

    # k "Eu... Eu vou pensar, okay? Mas não prometo nada..."
    k ""

# game/Kurusu route.rpy:158
translate italian kurusu_4a927248:

    # mc "Já é um começo. Obrigada."
    mc ""

# game/Kurusu route.rpy:160
translate italian kurusu_5be4f6f5:

    # k "De nada."
    k ""

# game/Kurusu route.rpy:162
translate italian kurusu_5487b7e6:

    # "Eu suspiro, me jogando de costas na grama e olhando pro céu."
    ""

# game/Kurusu route.rpy:164
translate italian kurusu_55ee30cc:

    # "A calmaria e o céu claro e livre de nuvens me fazem perceber o sono que eu estou, e um bocejo acaba saindo."
    ""

# game/Kurusu route.rpy:166
translate italian kurusu_5dc5deca:

    # mc "H-hnng~ Acho que vou tirar um cochilo. Me acompanha?"
    mc ""

# game/Kurusu route.rpy:168
translate italian kurusu_427430af:

    # "Kurusu deita com mais gentileza, passando os braços pela parte de trás da cabeça."
    ""

# game/Kurusu route.rpy:170
translate italian kurusu_40c0fabd:

    # k "Claro~"
    k ""

# game/Kurusu route.rpy:172
translate italian kurusu_5dfb2321:

    # "Aos poucos dá pra se perceber os pássaros assobiando uns pros outros, borboletas passando de uma árvore pra outra."
    ""

# game/Kurusu route.rpy:174
translate italian kurusu_dd5c646f:

    # "A serenidade que esse lugar transmite parece vinda de outra dimensão... Fico feliz do Kurusu ter um lugar só pra ele."
    ""

# game/Kurusu route.rpy:176
translate italian kurusu_ef645715:

    # mc "Hihi... Tão tranquilo~"
    mc ""

# game/Kurusu route.rpy:178
translate italian kurusu_dcfcbb26:

    # "Kurusu cobre os olhos com o braço, escurecendo a sua visão."
    ""

# game/Kurusu route.rpy:180
translate italian kurusu_c14e047a:

    # "Me virando na direção dele eu percebo o quão quieto Kurusu é. Ele sempre cuida de mim, tenta se abrir pros meus amigos e é gentil com as coisas."
    ""

# game/Kurusu route.rpy:182
translate italian kurusu_fe33e721:

    # "Eu queria passar mais tempo com ele... A correria da vida adulta logo vai começar."
    ""

# game/Kurusu route.rpy:184
translate italian kurusu_6ea124a6:

    # "E o meu medo de não ter mais ele do meu lado..."
    ""

# game/Kurusu route.rpy:186
translate italian kurusu_8146ffa8:

    # "Eu não quero nem pensar nisso."
    ""

# game/Kurusu route.rpy:188
translate italian kurusu_683eb731:

    # k "Eu consigo ouvir os seus pensamentos, [povname]. Descansa um pouquinho."
    k ""

# game/Kurusu route.rpy:190
translate italian kurusu_73b52865:

    # "Kurusu diz, imóvel. Obediente, eu coloco as mãos na minha barriga, aos poucos esvaziando os meus pensamentos..."
    ""

# game/Kurusu route.rpy:192
translate italian kurusu_ce55d1ed:

    # "...Até cair no sono."
    ""

# game/Kurusu route.rpy:194
translate italian kurusu_59c6a09d:

    # "Kurusu ao ouvir a minha respiração pesada, tira o braço do rosto."
    ""

# game/Kurusu route.rpy:196
translate italian kurusu_2e20edcd:

    # "Ele se apoia em um braço, me admirando."
    ""

# game/Kurusu route.rpy:200
translate italian kurusu_04a9d0ae:

    # k "Bom descanso, anjinho..."
    k ""

# game/Kurusu route.rpy:202
translate italian kurusu_d0f82e26:

    # "Ele dá um beijinho rápido na minha testa, virando para cair lentamente no sono junto comigo."
    ""

# game/Kurusu route.rpy:204
translate italian kurusu_a20cefa7:

    # "..."
    ""

# game/Kurusu route.rpy:206
translate italian kurusu_2b5d9d5f:

    # "Eu acordo, Kurusu me olhando com o seu sorriso de sempre."
    ""

# game/Kurusu route.rpy:208
translate italian kurusu_b6115764:

    # "Me levanto e estico o corpo, já soltando um bocejo longo."
    ""

# game/Kurusu route.rpy:210
translate italian kurusu_5e00c7d1:

    # mc "Y-yaawn~! Bom dia Kukki!"
    mc ""

# game/Kurusu route.rpy:212
translate italian kurusu_ae002e24:

    # "Kurusu ri, sentando e cruzando as pernas."
    ""

# game/Kurusu route.rpy:214
translate italian kurusu_b989b2e0:

    # k "Bom dia [povname]. Descansou bem?"
    k ""

# game/Kurusu route.rpy:216
translate italian kurusu_1e12c676:

    # mc "Parece que eu dormi por dois dias!"
    mc ""

# game/Kurusu route.rpy:218
translate italian kurusu_60ec0034:

    # "Kurusu levanta, botando a mochila nas costas."
    ""

# game/Kurusu route.rpy:220
translate italian kurusu_9fb19b67:

    # k "Então... Vamos?"
    k ""

# game/Kurusu route.rpy:222
translate italian kurusu_cf56c91a:

    # mc "Vamos! Ainda não comi nada, e acho que o sinal não bateu."
    mc ""

# game/Kurusu route.rpy:224
translate italian kurusu_d6913275:

    # k "Não, não bateu. E provavelmente a Skelly já deve ter enlouquecido por não encontrar a gente lá."
    k ""

# game/Kurusu route.rpy:226
translate italian kurusu_f609b2ec:

    # mc "É... Ela é um pouco fora da casinha."
    mc ""

# game/Kurusu route.rpy:228
translate italian kurusu_f8a1f2af:

    # "Nós dois rimos, imaginando como ela reagiria se nós não aparecessemos durante o dia"
    ""

# game/Kurusu route.rpy:230
translate italian kurusu_8ffc79e7:

    # "Ainda mais sabendo que eu decidi fazer com que o Kurusu me acompanhasse na festa."
    ""

# game/Kurusu route.rpy:232
translate italian kurusu_fac83535:

    # k "Ela parecia bem animada pra te ajudar nesse seu plano."
    k ""

# game/Kurusu route.rpy:234
translate italian kurusu_6e003a2b:

    # mc "Sim... Eu preciso por ele em prática logo~"
    mc ""

# game/Kurusu route.rpy:236
translate italian kurusu_bc9b7038:

    # k "...Boa sorte com isso."
    k ""

# game/Kurusu route.rpy:238
translate italian kurusu_c08530cd:

    # mc "Obrigada, vou precisar."
    mc ""

# game/Kurusu route.rpy:240
translate italian kurusu_90a379d5:

    # "Eu dou um risinho, Kurusu enfiando as mãos nos bolsos da calça."
    ""

# game/Kurusu route.rpy:242
translate italian kurusu_ada0d9ce:

    # "Nós fazemos o caminho de volta pro prédio do colégio e eventualmente nós voltamos para as nossas salas de aula separadas."
    ""

# game/Kurusu route.rpy:244
translate italian kurusu_6fb62416:

    # "Chegando em casa eu jogo a minha mochila em cima da cama e deito nela."
    ""

# game/Kurusu route.rpy:246
translate italian kurusu_d0bef178:

    # mc "Uff! Que cansaço~"
    mc ""

# game/Kurusu route.rpy:248
translate italian kurusu_75059c12:

    # "Eu pego o meu celular e mando um oi para o nosso grupinho secreto. Skelly criou porque queria poder reclamar do namorado sem culpa."
    ""

# game/Kurusu route.rpy:252
translate italian kurusu_23e472cc:

    # "Hora de dormir. Amanhã vai ser um longo dia!"
    ""

# game/Kurusu route.rpy:255
translate italian kurusu_bddda65b:

    # "Dia 2 - ultimo dia"
    ""

