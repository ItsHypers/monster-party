﻿# TODO: Translation updated at 2020-07-15 00:25

translate italian strings:

    # game/screens.rpy:256
    old "Back"
    new "Indietro"

    # game/screens.rpy:257
    old "History"
    new "Storia"

    # game/screens.rpy:258
    old "Skip"
    new "Salta"

    # game/screens.rpy:259
    old "Auto"
    new "Auto"

    # game/screens.rpy:260
    old "Save"
    new "Salva"

    # game/screens.rpy:261
    old "Q.Save"
    new "Salvataggio Rapido"

    # game/screens.rpy:262
    old "Q.Load"
    new "Caricamento Rapido"

    # game/screens.rpy:263
    old "Prefs"
    new "Preferenze"

    # game/screens.rpy:304
    old "Start"
    new "Avvio"

    # game/screens.rpy:312
    old "Load"
    new "Carica"

    # game/screens.rpy:314
    old "Preferences"
    new "Preferenze"

    # game/screens.rpy:318
    old "End Replay"
    new "Finisci Replay"

    # game/screens.rpy:322
    old "Main Menu"
    new "Menu Principale"

    # game/screens.rpy:324
    old "About"
    new "About"

    # game/screens.rpy:329
    old "Help"
    new "Aiuto"

    # game/screens.rpy:335
    old "Quit"
    new "Chiudi"

    # game/screens.rpy:476
    old "Return"
    new "Ritorna"

    # game/screens.rpy:560
    old "Version [config.version!t]\n"
    new "Versione [config.version!t]\n"

    # game/screens.rpy:566
    old "Made with {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"
    new "Fatto con {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"

    # game/screens.rpy:606
    old "Page {}"
    new "Pagina"

    # game/screens.rpy:606
    old "Automatic saves"
    new "Salvataggio automatico"

    # game/screens.rpy:606
    old "Quick saves"
    new "Salvataggio rapido"

    # game/screens.rpy:648
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A, %B %d %Y, %H:%M"

    # game/screens.rpy:648
    old "empty slot"
    new "Slot pulito"

    # game/screens.rpy:665
    old "<"
    new "<"

    # game/screens.rpy:668
    old "{#auto_page}A"
    new "{#auto_page}A"

    # game/screens.rpy:671
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # game/screens.rpy:677
    old ">"
    new ">"

    # game/screens.rpy:734
    old "Display"
    new "Display"

    # game/screens.rpy:735
    old "Window"
    new "Finestra"

    # game/screens.rpy:736
    old "Fullscreen"
    new "Fullscreen"

    # game/screens.rpy:740
    old "Language"
    new "Lingua"

    # game/screens.rpy:748
    old "Rollback Side"
    new "Rollback Side"

    # game/screens.rpy:749
    old "Disable"
    new "Disattiva"

    # game/screens.rpy:750
    old "Left"
    new "Sinistra"

    # game/screens.rpy:751
    old "Right"
    new "Destra"

    # game/screens.rpy:756
    old "Unseen Text"
    new "Testo non letto"

    # game/screens.rpy:757
    old "After Choices"
    new "Dopo Scelte"

    # game/screens.rpy:758
    old "Transitions"
    new "Transizioni"

    # game/screens.rpy:771
    old "Text Speed"
    new "Velocità testo"

    # game/screens.rpy:775
    old "Auto-Forward Time"
    new "Tempo Inoltro Automatico"

    # game/screens.rpy:782
    old "Music Volume"
    new "Volume Musica"

    # game/screens.rpy:789
    old "Sound Volume"
    new "Volume suono"

    # game/screens.rpy:795
    old "Test"
    new "Test"

    # game/screens.rpy:799
    old "Voice Volume"
    new "Volume Voce"

    # game/screens.rpy:810
    old "Mute All"
    new "Muta tutto"

    # game/screens.rpy:929
    old "The dialogue history is empty."
    new "La storia del dialogo è vuota."

    # game/screens.rpy:999
    old "Keyboard"
    new "Tastiera"

    # game/screens.rpy:1000
    old "Mouse"
    new "Mouse"

    # game/screens.rpy:1003
    old "Gamepad"
    new "Gamepad"

    # game/screens.rpy:1016
    old "Enter"
    new "Invio"

    # game/screens.rpy:1017
    old "Advances dialogue and activates the interface."
    new ""

    # game/screens.rpy:1020
    old "Space"
    new "Spazio"

    # game/screens.rpy:1021
    old "Advances dialogue without selecting choices."
    new "Avanza dialogo senza selezionare scelte."

    # game/screens.rpy:1024
    old "Arrow Keys"
    new "Frecce tastiera"

    # game/screens.rpy:1025
    old "Navigate the interface."
    new "Naviga l'interfaccia."

    # game/screens.rpy:1028
    old "Escape"
    new "Esc"

    # game/screens.rpy:1029
    old "Accesses the game menu."
    new "Accedi al menu del gioco."

    # game/screens.rpy:1032
    old "Ctrl"
    new "Ctrl"

    # game/screens.rpy:1033
    old "Skips dialogue while held down."
    new "Salta dialogo mentre premuto."

    # game/screens.rpy:1036
    old "Tab"
    new "Tab"

    # game/screens.rpy:1037
    old "Toggles dialogue skipping."
    new "Avvia salto del dialogo."

    # game/screens.rpy:1040
    old "Page Up"
    new "Pagina su"

    # game/screens.rpy:1041
    old "Rolls back to earlier dialogue."
    new "Ritorna al dialogo precedente."

    # game/screens.rpy:1044
    old "Page Down"
    new "Pagina giù"

    # game/screens.rpy:1045
    old "Rolls forward to later dialogue."
    new "Avanza in un dialogo successivo"

    # game/screens.rpy:1049
    old "Hides the user interface."
    new "Nasconde interfaccia."

    # game/screens.rpy:1053
    old "Takes a screenshot."
    new "Fa uno screenshot."

    # game/screens.rpy:1057
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Attiva/Disattiva assistenza {a=https://www.renpy.org/l/voicing}self-voicing{/a}."

    # game/screens.rpy:1063
    old "Left Click"
    new "Click sinistro"

    # game/screens.rpy:1067
    old "Middle Click"
    new "Click centrale"

    # game/screens.rpy:1071
    old "Right Click"
    new "Click destro"

    # game/screens.rpy:1075
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Rotellina del mouse Su\nClick Rollback Side "

    # game/screens.rpy:1079
    old "Mouse Wheel Down"
    new "Rotellina del mouse giù"

    # game/screens.rpy:1086
    old "Right Trigger\nA/Bottom Button"
    new "Grilletto destro\nA/Bottom Button"

    # game/screens.rpy:1090
    old "Left Trigger\nLeft Shoulder"
    new "Grilletto sinistro\nLeft Shoulder"

    # game/screens.rpy:1094
    old "Right Shoulder"
    new "Spalla destra."

    # game/screens.rpy:1099
    old "D-Pad, Sticks"
    new "D-Pad, Sticks"

    # game/screens.rpy:1103
    old "Start, Guide"
    new "Start, Guida"

    # game/screens.rpy:1107
    old "Y/Top Button"
    new "Y/Bottone superiore"

    # game/screens.rpy:1110
    old "Calibrate"
    new "Calibro"

    # game/screens.rpy:1175
    old "Yes"
    new "Sì"

    # game/screens.rpy:1176
    old "No"
    new "No"

    # game/screens.rpy:1222
    old "Skipping"
    new "Salta"

    # game/screens.rpy:1445
    old "Menu"
    new "Menu"

