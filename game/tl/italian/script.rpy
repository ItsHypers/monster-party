﻿# TODO: Translation updated at 2020-07-15 00:25

# game/script.rpy:24
translate italian start_c6301645:

    # "Olá, bem vinde ao Monster Party. Essa é uma DEMO e conta com 2 mini-rotas. O jogo ainda está em desenvolvimento, críticas e feedback são muito bem vindos."
    "Ciao, benvenuti a Monster Party. Questa é una  DEMO e ha 2 mini-routes. Questo gioco é ancora nello sviluppo, critiche e feedback sono molto graditi"

# game/script.rpy:25
translate italian start_fcf37af0:

    # "Por favor, comece dando um nome para a MC."
    "Quale é il tuo nome?"

# game/script.rpy:26
translate italian start_5b0d0225:

    # "Deixe em branco se quiser o nome Default."
    "Lascialo vuoto se vuoi che sia il nome Default"

# game/script.rpy:36
translate italian start_3a335b83:

    # "Seu nome é [povname], aproveite essa DEMO."
    "Ciao [povname], gradisci questa DEMO!"

# game/script.rpy:38
translate italian start_e4b8fda4:

    # "~PRÓLOGO~"
    "~Prologo~"

# game/script.rpy:40
translate italian start_5dfa76b6:

    # "Manhã de Segunda, primeiro período."
    "Lunedi' mattina, prima ora."

# game/script.rpy:42
translate italian start_a7abaf7d:

    # st "Certo, todos sentados? Então, como vocês sabem o ano letivo está para chegar ao fim, e a escola Lt. Sandalwood vai sediar o nosso baile anual."
    st "Okay, siete tutti seduti? Quindi, come sapete gia', L'anno scolastico sta per finire, e Lt. Sandalwood School ci ospitera' nel ballo annuale."

# game/script.rpy:44
translate italian start_769ae7d6:

    # "Stein começou a andar desengonçado e lentamente pela sala..."
    "Stein ha cominciato a camminare stranamete e piano per stanza..."

# game/script.rpy:46
translate italian start_05906fcf:

    # "Os alunos o acompanhavam com os olhos enquanto ele continuava a falar."
    "Gli alunni lo seguirono con gli occhi mentre lui continuo' a parlare."

# game/script.rpy:48
translate italian start_9a7de9b1:

    # "Alguns alunos conseguem lidar bem com a presença do professor na sala"
    "Alcuni alunni sono tranquilli anche se c'é il professore nella classe."

# game/script.rpy:50
translate italian start_ed2ae0df:

    # "Mas outros... Eu tenho certeza que nem tanto."
    "Ma gli altri ... Non ne sono sicuro"

# game/script.rpy:52
translate italian start_8d615112:

    # "Professor Stein não é exatamente uma pessoa ruim, ou assustadora..."
    "Professore Stein non é una persona malvagia, o spaventosa ..."

# game/script.rpy:54
translate italian start_12c81820:

    # "Mas o seu jeito desengonçado, a sua altura... Tudo é um pouquinho intimidante."
    "Ma il suo comportamento, sua altezza ... Tutto questo sembra minaccioso."

# game/script.rpy:56
translate italian start_cf3a21ea:

    # st "Vocês já tem ideia de par? É um evento muito importante, pois fecha o ciclo escolar de vocês."
    st "Hai gia' qualche idea? é un evento molto importante, concludendo tuo ciclo scolastico."

# game/script.rpy:58
translate italian start_2a704e29:

    # "Ah! Realmente, o final do ano tá proximo..."
    "Ah! Vero, la fine dell'anno scolastico é vicina ..."

# game/script.rpy:60
translate italian start_651a58fa:

    # a2 "Eh, nem sei se vou conseguir mesmo sair daqui, professor."
    a2 "Hey, non so neanche se potro' andarmene da qui, professore."

# game/script.rpy:62
translate italian start_1e438e09:

    # st "Calma... As provas finais não são tão ruins assim!"
    st "Calmati ... Gli esami non saranno cosi' difficili!"

# game/script.rpy:64
translate italian start_5d9e637e:

    # a1 "Pra você é fácil, você quem fez."
    a1 "Sara' facile per te, gli hai fatti tu!"

# game/script.rpy:66
translate italian start_aab528e4:

    # st "U-Uh, não é bem assim! Olha, o importante é sempre revisar e-"
    st "U-Uh, non é vero! Senti, la cosa importante é sempre controllare e-"

# game/script.rpy:68
translate italian start_a19d2ac7:

    # mc "Hm..."
    mc "Hm..."

# game/script.rpy:70
translate italian start_05948a62:

    # "Eu sempre acabo divagando nas aulas do professor Stein, e agora que ele mencionou o baile..."
    "Sono sempre distratto nella classe del Professore Stein , e ora che ha menzionato il ballo ..."

# game/script.rpy:72
translate italian start_2f3fc115:

    # "Eu não lembrei que precisava de um par."
    "Mi sono dimenticato che mi serve uscire con qualcuno..."

# game/script.rpy:74
translate italian start_8ad86641:

    # "A Skelly provavelmente vai com o namorado, a Merrii já deve ter alguém. E o Kurusu não vai querer ir á festa..."
    "Skelly probabilmente va con il suo ragazzo, Merrii forse ha gia' qualcuno. E Kurusu non andra' alla festa..."

# game/script.rpy:76
translate italian start_2bc8d25b:

    # "Não quero ir sozinha. Preciso encontrar alguém logo."
    "Non voglio andarci da solo. Devo trovare qualcuno."

# game/script.rpy:78
translate italian start_83e5368e:

    # mc "Mas é complicado, não tenho interesse em ninguém além dos meus amigos... Ugh."
    mc "Ma é complicato, Non ho interesse in nessuno tranne i miei amici ... Ugh."

# game/script.rpy:80
translate italian start_042851b4:

    # st "[povname]?"
    st "[povname]?"

# game/script.rpy:82
translate italian start_65e6283c:

    # mc "Eu podia pedir pra Skelly me arranjar um par, mas eu não quero depender dela pra isso."
    mc "Potrei chiedere a Skelly di fare un ballo, ma non voglio dipendere su di lei."

# game/script.rpy:84
translate italian start_aaff5573:

    # st "[povname], tudo bem?"
    st "[povname], come stai?"

# game/script.rpy:86
translate italian start_ee2636ff:

    # mc "E se eu conseguisse convencer o Kurusu a vir comigo? O problema é que ele não gosta desse tipo de socialização... O que eu vou fazer?"
    mc "E se convinco Kurusu di venire con me? Il problema é che a lui non piacciono questo tipo di socializzazione ... Cosa faro'?"

# game/script.rpy:88
translate italian start_b2a19f52:

    # st "[povname]."
    st "[povname]."

# game/script.rpy:90
translate italian start_01cfc3e0:

    # mc "Na verdade ele não gosta de nenhum tipo de socialização. ha... ha..."
    mc "Pensandoci, non gli piace socializzare affatto. ha ha .."

# game/script.rpy:92
translate italian start_767786ad:

    # "Stein se aproximou, me dando um peteleco na testa; o devaneio no qual estava perdida — e aparentemente falando sozinha — se dissipando, dando lugar à dor incômoda na minha cabeça por causa do peteleco que me assustou."
    "Stein era venuto, picchiettandomi sulla fronte; Il sogno dove ero perso - apparentemente parlavo con me stesso - sprecato, mostrando il mio incomfortabile dolore nella mia testa perché mi ero spaventato."

# game/script.rpy:94
translate italian start_56edf709:

    # mc "!!"
    mc "!!"

# game/script.rpy:96
translate italian start_469d0ae8:

    # st "[povname]!"
    st "[povname]!"

# game/script.rpy:98
translate italian start_7d93e45d:

    # "Stein elevou sua voz, colocando uma mão na cintura, me observando."
    "Stein ha alzato la voce, mettendo la mano sulla sua cintura, guardandomi."

# game/script.rpy:100
translate italian start_6541fc31:

    # mc "A-AI! O que foi, p-professor Stein!?"
    mc "A-AI! Cosa succede, p-professore Stein?"

# game/script.rpy:102
translate italian start_b87ddf3f:

    # st "Eu estava falando com você. Não prestou atenção?"
    st "Stavo parlando con te. Non mi ascoltavi?"

# game/script.rpy:104
translate italian start_5e8205f1:

    # mc "Desculpa, me perdi um pouco..."
    mc "Scusi, mi ero perso un po'"

# game/script.rpy:106
translate italian start_b4207cd0:

    # "Stein sorriu, fazendo meu coração dar uma batida mais forte."
    "Stein sorrise, faccendomi battere il cuore ancora di piu'."

# game/script.rpy:108
translate italian start_7325474d:

    # "Embora seja um sorriso um pouco estranho... É genuíno."
    "Anche se é un sorriso strano ... é puro."

# game/script.rpy:110
translate italian start_2d1e4451:

    # st "Não fique pra baixo. Hoje é o seu dia de arrumar a classe, não esquece ok? Traz a chave para mim quando terminar, vou estar na sala dos professores."
    st "Stai tranquillo. Oggi devi pulire la classe, non dimenticartelo, okay?? Portami la chiave quando finisci, Saro' nella stanza dei professori"

# game/script.rpy:112
translate italian start_a4afdf57:

    # mc "C-certo."
    mc "C-certo."

# game/script.rpy:114
translate italian start_c7fc0bd0:

    # "Eu preciso me organizar logo, duvido que tenha alguém sem par até esse ponto."
    "Devo organizzarmi un po'; Sono sicuro che mi stanno gia' aspettando"

# game/script.rpy:116
translate italian start_f1eb5d15:

    # "O sinal para o intervalo bateu, é melhor eu me preparar pra descer."
    "Il campanello, meglio se scendo..."

# game/script.rpy:118
translate italian start_69a3f9d2:

    # "O pessoal já deve estar indo pra cafeteria, os corredores ficavam mais espaçosos e vazios por conta disso..."
    "La gente deve gia' andare al bar, I corridoi sono molto piu' vuoti del solito..."

# game/script.rpy:120
translate italian start_2a46c8ca:

    # "Enquanto passava pelo corredor, passei pela porta da sala de aula do Kurusu."
    "Andando per il corridoio, ho attraversato la porta della classe di Kurusu."

# game/script.rpy:122
translate italian start_fc550b58:

    # "Dei uma espiadinha pela sala."
    "Ho guardato nella stanza."

# game/script.rpy:124
translate italian start_2e1f403a:

    # mc "Ué. Cadê ele? Já foi embora...?"
    mc "Huh. Dov'é?? é andato gia' via...?"

# game/script.rpy:128
translate italian start_00e8b5a1:

    # k "Me procurando?"
    k "Mi stai cercando?"

# game/script.rpy:130
translate italian start_34485c8a:

    # "Um arrepio se espalhou em minha espinha sentindo algo presente apoiando-se em meu ombro, me virando para trás."
    "Cominciai a tremare, sentendo qualcosa sulla mia spalla, facendomi girare."

# game/script.rpy:132
translate italian start_753ed437:

    # mc "Ah! Kukki!"
    mc "Ah! Kusumu!"

# game/script.rpy:134
translate italian start_b688c5e3:

    # "Eu solto um suspiro, dando um sorrisinho sem graça."
    "Ripresi il fiato, con un sorriso imbarazzato."

# game/script.rpy:136
translate italian start_6d05abee:

    # mc "N-não faz mais isso!"
    mc "N-non farlo piu'!"

# game/script.rpy:138
translate italian start_89de8caf:

    # "Kurusu sorri, tirando a mão do meu ombro e cruzando os braços enquanto ria do meu susto."
    "Kurusu sorrise, togliendo la sua mano dalla mia spalla e incrocciando le mani mentre rideva al mio spavento."

# game/script.rpy:140
translate italian start_4ba7a2a9:

    # k "Desculpa, [povname]... O que você queria?"
    k "Scusa, [povname] ... Cercavi qualcosa?"

# game/script.rpy:142
translate italian start_1d997f32:

    # mc "A-ahm.... Porque não vem almoçar comigo na lanchonete?"
    mc "A-ahm .... Perché non vieni a pranzare al bar con me?"

# game/script.rpy:144
translate italian start_464afea6:

    # k "Tudo bem. Esqueceu o dinheiro de novo?"
    k "Okay. Ti sei dimenticata I soldi di nuovo?"

# game/script.rpy:146
translate italian start_e7860134:

    # "Isso é vergonhoso. Eu esqueci o dinheiro pro almoço tantas vezes assim?"
    "Questo é vergognoso. Mi dimentico i soldi cosi' tante volte?"

# game/script.rpy:148
translate italian start_a4916d41:

    # "Tenho que prestar mais atenção no que faço durante o dia."
    "Devo concentrarmi di piu' la mattina."

# game/script.rpy:150
translate italian start_fb821e6d:

    # mc "N-não, seu idiota. Só queria passar um tempo com você."
    mc "N-no, idiota. Volevo solo spendere del tempo con te."

# game/script.rpy:152
translate italian start_5d2d48a3:

    # k "Certo, certo..."
    k "Certo, certo.."

# game/script.rpy:154
translate italian start_fb68cc2b:

    # mc "Mas se quiser me comprar um bolinho, eu aceito!"
    mc "Ma se vuoi che mi compri qualcosa, io accetto!"

# game/script.rpy:156
translate italian start_c7b2be85:

    # "Com um sorriso bobo no rosto, Kurusu deu um tapa leve nas minhas costas, brincando."
    "Con un sorrisino, Kurusu mi tocco' la spalla, in modo giocoso."

# game/script.rpy:158
translate italian start_57dd886a:

    # k "Porque você ~só quer passar um tempo comigo~, certo?"
    k "Perché tu ~ vuoi solo spendere del tempo con me ~, vero?"

# game/script.rpy:160
translate italian start_b291e46b:

    # "Eu gosto da amizade que temos. Desde pequenos, Kurusu é gentil comigo. Eu espero que isso nunca mude..."
    "Amo la nostra amicizia. Da bambini, Kurusu era sempre stato gentile con me. Spero che non cambi mai ..."

# game/script.rpy:162
translate italian start_2bacafe6:

    # "Ele sempre gostou de me mostrar bichinhos que encontrava. Lagartinhas, besourinhos, borboletas de todas as cores... Mas nunca pôde tocar, por causa dos poderes da sua mãe."
    "Adorava mostrarmi gli animaletti che trovava. bruchi, insetti, farfalle di tutti i colori ... Ma non li toccava mai, a causa dei poteri di sua madre."

# game/script.rpy:164
translate italian start_2e7985eb:

    # "Quando percebi que estava absorvida em pensamentos novamente, já tínhamos chegado na lanchonete."
    "Durante il tempo che io ho speso pensando, eravamo gia' arrivati al bar."

# game/script.rpy:166
translate italian start_6191296b:

    # "- Na lanchonete -"
    "- Il bar -"

# game/script.rpy:168
translate italian start_628d8534:

    # "Kurusu me dá um toque no braço, direcionando meu corpo para dar um passo para frente. Olho para ele e o mesmo aponta para uma mesa vazia da lanchonete."
    "Kurusu mi tocco' sulla mano, dicendo al mio corpo di andare avanti. Lo guardo e lui mi mostra un tavolo vuoto."

# game/script.rpy:170
translate italian start_9ac67b12:

    # k "Vá se sentar. Eu pego o almoço e o seu bolinho, me espere lá."
    k "Siediti. Vado a prendere il cibo, aspettami li'."

# game/script.rpy:172
translate italian start_af0c5e96:

    # mc "Oh. Obrigada Kukki!"
    mc "Oh, grazie Kukki!"

# game/script.rpy:174
translate italian start_c4c1e2cb:

    # "Ele dá um ~tchauzinho~ com a mão, virando-se em direção á lanchonete."
    "Mi dice ~ ciao-ciao ~ con la mano, andando verso il bar."

# game/script.rpy:176
translate italian start_217e9ae2:

    # "Eu sorrio, observando ele fazer o trajeto."
    "Io sorrido, guardandolo."

# game/script.rpy:178
translate italian start_e67edb61:

    # "Kurusu sempre foi mais aberto comigo, isso me deixa feliz."
    "Kurusu é sempre stato se stesso con me, e questo mi rallegra."

# game/script.rpy:180
translate italian start_42662dea:

    # "Ele não brincava com as outras crianças porque tinha medo de tocar nelas, mas nunca foi assim comigo. Eu nunca soube o por quê. Talvez por me conhecer melhor? Por se sentir mais confortável?"
    "Lui non giocava mai con gli altri, per paura di toccarli, ma non con me. non sapevo il perché. Fose perché mi conosceva meglio? Perché sentirsi piu' aperto?"

# game/script.rpy:182
translate italian start_cea545be:

    # "Saber que eu entendia o problema dele? Eu imagino que sim."
    "Sara' perché io capisco il suo problema? Forse ..."

# game/script.rpy:184
translate italian start_6da936da:

    # "Nossos pais sempre foram muito próximos, e quando o humano da Morte se foi, minha mãe fez de tudo para ajudá-la..."
    "I nostri genitori erano amici molto stretti, e quando l'Umano della Morte scompari', mia madre fece di tutto per aiutarla ..."

# game/script.rpy:186
translate italian start_336f042f:

    # "O que resultou com Kurusu morando conosco por um tempo, por conta do trabalho da mãe de Kurusu."
    ""

# game/script.rpy:188
translate italian start_48436560:

    # "Foi bem divertido, ficávamos até tarde brincando e lendo quadrinhos."
    ""

# game/script.rpy:190
translate italian start_572a0270:

    # "Claro que ele evita me tocar por muito tempo, mas ele gosta de ser meu amigo. E isso é o mais importante agora."
    ""

# game/script.rpy:192
translate italian start_f5d466cc:

    # "Só queria que ele perdesse um pouco dessa tensão quando o assunto é socializar. Ir no baile vai ser uma ótima oportunidade pra gente passar um tempo se divertindo."
    ""

# game/script.rpy:195
translate italian start_4b084085:

    # "Eu entendo que ele é naturalmente tímido, mas..."
    ""

# game/script.rpy:197
translate italian start_e0d59b8c:

    # "Kurusu voltou com as duas bandejas, uma delas com o bolinho que ele havia prometido mais cedo."
    ""

# game/script.rpy:199
translate italian start_48441590:

    # k "Perdida nos pensamentos de novo, [povname]?"
    k ""

# game/script.rpy:201
translate italian start_76406145:

    # "A timidez dele é um dos charminhos que o deixam único."
    ""

# game/script.rpy:203
translate italian start_1ae8a70f:

    # mc "Mais ou menos. As provas chegando sempre me deixam nervosa..."
    mc ""

# game/script.rpy:205
translate italian start_b9873805:

    # k "Qualquer coisa você pode fazer as recuperações, não se preocupa."
    k ""

# game/script.rpy:207
translate italian start_1e65d00f:

    # mc "Prefiro não precisar delas..."
    mc ""

# game/script.rpy:209
translate italian start_872716b6:

    # "Ele olha para mim com um sorriso de canto nos lábios."
    ""

# game/script.rpy:211
translate italian start_4954e144:

    # k "Você vai conseguir, [povname]."
    k ""

# game/script.rpy:213
translate italian start_60b45f9f:

    # "Logo muda o foco para o seu sanduíche, mordendo com grande vontade."
    ""

# game/script.rpy:215
translate italian start_7a1caf97:

    # "Eu me ajeito na cadeira, limpando a garganta levemente."
    ""

# game/script.rpy:217
translate italian start_417c472b:

    # mc "Então, sobre o baile..."
    mc ""

# game/script.rpy:219
translate italian start_67a08757:

    # "Kurusu arqueou uma sobrancelha e respirou fundo, revirando os olhos e mantendo-se a mastigar o alimento."
    ""

# game/script.rpy:221
translate italian start_22a3e58b:

    # k "Não vou."
    k ""

# game/script.rpy:223
translate italian start_102fd0a9:

    # mc "Ah, qual é! Por que não?"
    mc ""

# game/script.rpy:225
translate italian start_637e7053:

    # k "Sabe que não gosto de multidões..."
    k ""

# game/script.rpy:227
translate italian start_cca1d595:

    # mc "Mas gosta de mim, não gosta?"
    mc ""

# game/script.rpy:229
translate italian start_d10e1db0:

    # "Kurusu ri, apoiando o rosto em uma das mãos. O pão segurado firmemente na outra mão com uma marca de mordida no meio."
    ""

# game/script.rpy:231
translate italian start_fd463d6b:

    # k "Será que gosto?"
    k ""

# game/script.rpy:233
translate italian start_95186d4f:

    # "Eu apoio o cotovelo na mesa imitando a pose do Kurusu, brincando com ele."
    ""

# game/script.rpy:235
translate italian start_61478f40:

    # mc "Claro que eu gosto da [povname]! Eu prometi que ia casar com ela quando nós éramos crianças, e eu gosto de manter as minhas promessas."
    mc ""

# game/script.rpy:237
translate italian start_c8cfe468:

    # "Eu digo, e ele assiste a atuação com uma expressão tediosa mas ainda com um sorriso."
    ""

# game/script.rpy:239
translate italian start_687cb384:

    # k "Você falando assim talvez eu acredite. Repete, vai que dá certo dessa vez."
    k ""

# game/script.rpy:241
translate italian start_ea58c129:

    # mc "E ainda pedi para a lua ser nossa testemunha. Nunca vou esquecer aquela noite..."
    mc ""

# game/script.rpy:243
translate italian start_3bdd2ae9:

    # "Kurusu ri baixinho com um leve rubor em suas bochechas."
    ""

# game/script.rpy:245
translate italian start_9270301c:

    # k "Uhum. E o que mais? Também pedi pro Sol abençoar nosso futuro? É por isso que você não larga do meu pé assim?"
    k ""

# game/script.rpy:247
translate italian start_57310957:

    # "Eu dou um sorrisinho, voltando a me sentar ereta no banquinho da cantina."
    ""

# game/script.rpy:249
translate italian start_441b5fb9:

    # mc "Para, eu tô falando sério... Eu não quero ir sozinha Kukki, por favor!"
    mc ""

# game/script.rpy:251
translate italian start_fb9f910d:

    # "Kurusu encolhe e desvia o olhar, corando nas extremidades das bochechas. Ele segurou o sanduíche com as duas mãos e o encarou por um momento."
    ""

# game/script.rpy:253
translate italian start_987688f0:

    # k "Hum... Eu vou pensar a respeito."
    k ""

# game/script.rpy:255
translate italian start_258c6bca:

    # mc "Obrigada, você é o melhor!"
    mc ""

# game/script.rpy:257
translate italian start_1ef3490a:

    # "Kurusu deu mais uma mordida no sanduíche, o rubor desaparecendo aos pouco do seu rosto, voltando ao tom normal da sua pele cinzenta."
    ""

# game/script.rpy:259
translate italian start_579a0c5f:

    # "Skelly e o namorado sentam na mesa, Kyte acenou e Skelly deu um beijo rápido na bochecha da [povname]."
    ""

# game/script.rpy:261
translate italian start_545a5be8:

    # sk "Oi! Tudo bem, Kukki?"
    sk ""

# game/script.rpy:263
translate italian start_c3290b02:

    # k "Olá."
    k ""

# game/script.rpy:265
translate italian start_d1bf1a29:

    # mc "Oi Skelly, oi Kyte!"
    mc ""

# game/script.rpy:267
translate italian start_482dbd85:

    # ky "Falaê. De boa?"
    ky ""

# game/script.rpy:269
translate italian start_d55ae269:

    # mc "Yep!"
    mc ""

# game/script.rpy:271
translate italian start_9fe709ea:

    # sk "Então, baile! Você já foi convidada? Já sabe com que vestido ir?"
    sk ""

# game/script.rpy:273
translate italian start_f2d09a2d:

    # mc "Não..."
    mc ""

# game/script.rpy:275
translate italian start_36a4c141:

    # sk "Ah, mas isso é ótimo. Chama a Merrii, nós vamos pra sua casa e te ajudamos com isso. Aposto que não irá se arrepender."
    sk ""

# game/script.rpy:277
translate italian start_0652cbcf:

    # mc "Ah? Okay, mas sabe que a minha mãe não gosta de visitas surpresa, então se comporta."
    mc ""

# game/script.rpy:279
translate italian start_e468f2a8:

    # sk "Ela já nos conhece. Não tem problema, né?"
    sk ""

# game/script.rpy:281
translate italian start_57ce590e:

    # "Kurusu suspira e coloca o capuz, terminando de comer o sanduíche."
    ""

# game/script.rpy:283
translate italian start_52f0290d:

    # mc "Não tem não... Então, você vai com o Kyte?"
    mc ""

# game/script.rpy:285
translate italian start_4c022535:

    # sk "Claro! Meu amorzinho e eu adoramos dançar!"
    sk ""

# game/script.rpy:287
translate italian start_edfd26d7:

    # "Skelly dá um beijinho na bochecha do Kyte, passando um braço pelo braço dele. Ele reage coçando a nuca com vergonha."
    ""

# game/script.rpy:289
translate italian start_6ebb7d79:

    # ky "Eu já curto mais pela comida mesmo."
    ky ""

# game/script.rpy:291
translate italian start_f13f4b19:

    # "Skelly deu um tapinha no braço dele, rindo enquanto aperta o namorado."
    ""

# game/script.rpy:293
translate italian start_2c5db5fb:

    # sk "Ai, para. Eu sei que você gosta de sair comigo."
    sk ""

# game/script.rpy:295
translate italian start_6c085138:

    # ky "Você sabe que sim, é divertido!"
    ky ""

# game/script.rpy:297
translate italian start_633dcbe7:

    # "Kurusu solta uma risada baixa, me olhando e desviando o olhar para a mesa em seguida."
    ""

# game/script.rpy:299
translate italian start_6a10c08f:

    # mc "Ah sim, eu convidei o Kurusu pro baile..."
    mc ""

# game/script.rpy:301
translate italian start_61880f6d:

    # sk "O Kukki? E você vai?"
    sk ""

# game/script.rpy:303
translate italian start_7efd5ec0:

    # "Skelly muda de posição, sentando de frente pro Kurusu. Os dois se encararam por alguns segundos."
    ""

# game/script.rpy:305
translate italian start_81f15860:

    # "Ele se encolhe, mantendo uma certa distância, respondendo baixo."
    ""

# game/script.rpy:307
translate italian start_ff425589:

    # k "Eu não sei ainda. Sabe como é..."
    k ""

# game/script.rpy:309
translate italian start_9f43eb45:

    # mc "Ai, eu não quero acabar tendo que ir sozinha~"
    mc ""

# game/script.rpy:311
translate italian start_e7d8bfe9:

    # sk "Nem precisa! Merrii e eu arrumamos um par pra você."
    sk ""

# game/script.rpy:313
translate italian start_cfa9f478:

    # k "H-hm."
    k ""

# game/script.rpy:315
translate italian start_db3fd8c2:

    # "Kurusu se mexe na cadeira, desconfortável com o rumo que a conversa está seguindo."
    ""

# game/script.rpy:317
translate italian start_0174b114:

    # mc "Mas eu não quero depender de vocês. Consigo me virar."
    mc ""

# game/script.rpy:319
translate italian start_cb43b6ba:

    # ky "Se eu fosse você, confiava na Skelly... Digo, ela tem bom gosto né. Acabou me escolhendo."
    ky ""

# game/script.rpy:321
translate italian start_a2724d52:

    # "Kyte ri, fazendo Skelly revirar os olhos com o comentário. Ela não consegue segurar uma risadinha, dando um tapinha de leve no braço do namorado."
    ""

# game/script.rpy:323
translate italian start_4447ca22:

    # sk "Cala a boca, bobão. Vamos [povname], vai ser divertido."
    sk ""

# game/script.rpy:325
translate italian start_2417551f:

    # mc "Tá, tá."
    mc ""

# game/script.rpy:327
translate italian start_36f8987e:

    # "O sinal bate a tempo de eu terminar o meu almoço. O tempo passou rápido desta vez, devo ter me distraído enquanto conversava com eles."
    ""

# game/script.rpy:329
translate italian start_0b035321:

    # sk "Então [povname], me espera na entrada do colégio. Vamos pegar a Merrii e ir pra sua casa."
    sk ""

# game/script.rpy:331
translate italian start_47567f50:

    # ky "E eu não posso ir não?"
    ky ""

# game/script.rpy:333
translate italian start_87134ea0:

    # sk "Não, você vai pra casa. Precisa estudar pra sua prova, lembra?"
    sk ""

# game/script.rpy:335
translate italian start_961ba9a4:

    # ky "Ah, é..."
    ky ""

# game/script.rpy:337
translate italian start_aedac7c5:

    # ky "Kurusu, quer jogar hoje?"
    ky ""

# game/script.rpy:339
translate italian start_ac5819b1:

    # k "Claro."
    k ""

# game/script.rpy:341
translate italian start_b5835a1b:

    # "Skelly revirou os olhos, vendo que seu namorado parecia ~extremamente~ atento às provas que viriam."
    ""

# game/script.rpy:343
translate italian start_04909c5c:

    # sk "Então, eu preciso ir. Tenho aula de Rituais agora e não posso entrar atrasada. Até!"
    sk ""

# game/script.rpy:345
translate italian start_8ab17447:

    # mc "Tchauzinho!"
    mc ""

# game/script.rpy:347
translate italian start_311b6c22:

    # k "Bye."
    k ""

# game/script.rpy:349
translate italian start_28a3554d:

    # ky "Até mais, chuchu!"
    ky ""

# game/script.rpy:351
translate italian start_56e3a498:

    # "Nos levantamos das cadeiras e cada um segue em direção á sua sala."
    ""

# game/script.rpy:353
translate italian start_4f39e534:

    # "Chegando no corredor, alguém acaba esbarrando em mim. Meu corpo cai no chão e eu acabo ficando desnorteada."
    ""

# game/script.rpy:355
translate italian start_d0a1b7f5:

    # mc "A-ai..! Cuidado!"
    mc ""

# game/script.rpy:357
translate italian start_007099b4:

    # who "O-oh... Desculpa, você está bem?"
    who ""

# game/script.rpy:359
translate italian start_f90474e1:

    # "Ele estende uma mão, me ajudando a levantar"
    ""

# game/script.rpy:361
translate italian start_21a308ac:

    # "Eu percebo que ele está todo enfaixado... Não deve ser por acidente, então ele deve ser uma múmia."
    ""

# game/script.rpy:363
translate italian start_15bcede4:

    # mc "Obrigada... Você se machucou?"
    mc ""

# game/script.rpy:365
translate italian start_65a4aa16:

    # who "Não, eu... A aula!"
    who ""

# game/script.rpy:367
translate italian start_59e3f2fa:

    # "Ele solta a mão que segurava a minha e corre em direção ás salas. Deve ser aula de Rituais também..."
    ""

# game/script.rpy:369
translate italian start_798515b3:

    # mc "Heheh. Espero que ele chegue a tempo."
    mc ""

# game/script.rpy:371
translate italian start_29867f2e:

    # "- Sala de aula -"
    ""

# game/script.rpy:373
translate italian start_0479f07f:

    # st "Não se esqueçam de fazer um resumo da matéria de hoje. Amanhã temos um teste e esse conteúdo vai cair. Podem ir, bom descanso pra vocês."
    st ""

# game/script.rpy:375
translate italian start_ffc5f795:

    # "Os alunos começam a sair aos poucos, alguns em grupos e outros sozinhos. E os murmúrios de conversas se afastavam com o tempo."
    ""

# game/script.rpy:377
translate italian start_2019f839:

    # "Quando terminei de arrumar minhas coisas, O professor Stein me chamou a atenção."
    ""

# game/script.rpy:379
translate italian start_a7d9d489:

    # st "[povname]? Você se esqueceu do que eu pedi hoje mais cedo?"
    st ""

# game/script.rpy:381
translate italian start_afdb4a9d:

    # mc "Eh? Ah, é verdade! Desculpa professor, eu ando com a cabeça muito ocupada."
    mc ""

# game/script.rpy:383
translate italian start_c6531220:

    # st "Pensando em meninos, hmm? Na sua idade eu também era assim."
    st ""

# game/script.rpy:385
translate italian start_f236081a:

    # "Ele se apoia em uma das mesas, dando um sorrisinho, me observando terminar de organizar o material."
    ""

# game/script.rpy:387
translate italian start_bf70295e:

    # mc "Ah? Não! Não é isso não! Eu só.... Espera, você também era assim?"
    mc ""

# game/script.rpy:389
translate italian start_bb8d706e:

    # st "É... Eu sempre fui meio careta. Embora eu tenha dado tudo de mim, meus relacionamentos não duraram muito."
    st ""

# game/script.rpy:391
translate italian start_8c0fef3d:

    # "Ele ri baixinho, cruzando os braços e fazendo um pequeno ~não~ com a cabeça"
    ""

# game/script.rpy:393
translate italian start_03634cc3:

    # mc "Ah, sinto muito por isso. Mas não é por esse motivo que eu estou distraída."
    mc ""

# game/script.rpy:395
translate italian start_474b5ebe:

    # st "Certo... Então se precisar conversar já sabe, pode chamar o Stein sem problemas~"
    st ""

# game/script.rpy:397
translate italian start_cf38b91e:

    # mc "Hihih~ Pode deixar professor Stein~"
    mc ""

# game/script.rpy:399
translate italian start_6430f996:

    # "Stein levanta da mesa, passando as mãos levemente no jaleco, ajeitando."
    ""

# game/script.rpy:401
translate italian start_f67e35f6:

    # st "Bom, eu vou pra sala checar se está tudo preparado para a prova de amanhã. Você vai ficar bem?"
    st ""

# game/script.rpy:403
translate italian start_bca8c66e:

    # "Eu dou um sorrisinho, arrastando minha cadeira para perto da mesa."
    ""

# game/script.rpy:405
translate italian start_b7b78489:

    # mc "Vou ficar sim, não se preocupa! Logo mais eu te entrego a chave."
    mc ""

# game/script.rpy:407
translate italian start_885e3bd9:

    # st "Certo... Até, [povname]."
    st ""

# game/script.rpy:409
translate italian start_a376f4e6:

    # "Stein sai da sala e eu fico sozinha enquanto arrumava as coisas"
    ""

# game/script.rpy:411
translate italian start_cbfdf261:

    # "Era estranho quando os alunos não estavam presentes, mas também parecia que eu possuía uma sala inteira para mim."
    ""

# game/script.rpy:413
translate italian start_e393b2bc:

    # mc "Droga... Eu marquei de encontrar as meninas agora."
    mc ""

# game/script.rpy:415
translate italian start_b67ec3f9:

    # mc "Vou mandar uma mensagem pra Skelly."
    mc ""

# game/script.rpy:417
translate italian start_75bc79ad:

    # "Eu envio uma mensagem no nosso grupo pessoal, avisando que eu havia ficado responsável pela limpeza de hoje."
    ""

# game/script.rpy:419
translate italian start_94830479:

    # "Elas respondem dizendo que vão vir me ajudar com a sala, assim voltamos mais cedo para casa."
    ""

# game/script.rpy:421
translate italian start_00805f01:

    # mc "Certo, elas devem chegar logo..."
    mc ""

# game/script.rpy:423
translate italian start_0445db6d:

    # "Após alguns minutos elas chegam, e nós nos dividimos entre as tarefas restantes."
    ""

# game/script.rpy:425
translate italian start_c8881d91:

    # "Terminamos em questão de minutos, e logo estávamos prontas para ir para casa."
    ""

# game/script.rpy:427
translate italian start_5226b824:

    # me "Uuf! Tudo pronto então?"
    me ""

# game/script.rpy:429
translate italian start_b8d0bc2e:

    # sk "Até que não demorou tanto!"
    sk ""

# game/script.rpy:431
translate italian start_d27ef349:

    # mc "Ainda bem né. Vamos? Preciso entregar a chave pro professor Stein."
    mc ""

# game/script.rpy:433
translate italian start_a792fffc:

    # me "Ahh... Nós te esperamos do lado de fora da sala."
    me ""

# game/script.rpy:435
translate italian start_e6d06181:

    # sk "E deixar a [povname] sozinha com o Stein? Você não bate bem né, Merrii?"
    sk ""

# game/script.rpy:437
translate italian start_22f056a3:

    # mc "Sozinha? Como assim?"
    mc ""

# game/script.rpy:439
translate italian start_e89986d7:

    # sk "É... Ele é estranho."
    sk ""

# game/script.rpy:441
translate italian start_9b90a189:

    # mc "Estranho?"
    mc ""

# game/script.rpy:443
translate italian start_2c71084f:

    # sk "Sim, você não acha? Ele bebe líquido de bateria como se fosse um suco de caixinha!"
    sk ""

# game/script.rpy:449
translate italian start_3e46b9e0:

    # sk "Você não acha?"
    sk ""

# game/script.rpy:450
translate italian start_63899158:

    # mc "É o jeito dele, Skelly. Não fala assim..."
    mc ""

# game/script.rpy:451
translate italian start_d1641316:

    # sk "Ok, mas você vai lá. A gente te espera aqui!"
    sk ""

# game/script.rpy:452
translate italian start_6b91e1d4:

    # mc "Tá. Não vou demorar."
    mc ""

# game/script.rpy:456
translate italian start_eb0fa930:

    # sk "Né? Mas você vai mesmo assim?"
    sk ""

# game/script.rpy:457
translate italian start_375a5ed9:

    # mc "Sim. Eu preciso entregar a chave pra ele."
    mc ""

# game/script.rpy:458
translate italian start_29191569:

    # sk "Ok, sem problema."
    sk ""

# game/script.rpy:459
translate italian start_dac780a8:

    # mc "Não vou demorar, só um segundo."
    mc ""

# game/script.rpy:462
translate italian start_60436f4d:

    # "Eu bati na porta, de pé em frente à sala que esperava ser aberta."
    ""

# game/script.rpy:464
translate italian start_69876dd5:

    # "Enquanto espero ser atendida, ouço o barulho de coisas caindo, alguém batendo em algo..."
    ""

# game/script.rpy:466
translate italian start_200096b6:

    # "Fico confusa, mas tento ignorar e esperar."
    ""

# game/script.rpy:468
translate italian start_73ded2c0:

    # "Stein abre a porta sem o seu jaleco de professor, segurando uma das suas mãos, caída, na outra."
    ""

# game/script.rpy:470
translate italian start_5938e9ac:

    # "Tentando se apoiar na porta, seu óculos basicamente caindo do seu rosto..."
    ""

# game/script.rpy:474
translate italian start_950bca37:

    # "É uma cena cômica e ao mesmo tempo preocupante."
    ""

# game/script.rpy:476
translate italian start_5ad0214e:

    # st "Ah, [povname]! Você já terminou de arrumar a sala?"
    st ""

# game/script.rpy:478
translate italian start_38a495bc:

    # mc "Sim, aqui está a chave."
    mc ""

# game/script.rpy:480
translate italian start_fc5f6c39:

    # st "Oh, obrigado."
    st ""

# game/script.rpy:482
translate italian start_4746b5fb:

    # "Stein pega a chave, guardando no bolso da calça."
    ""

# game/script.rpy:486
translate italian start_52a0ae6e:

    # st "Você precisa de mais alguma coisa?"
    st ""

# game/script.rpy:488
translate italian start_98c3e564:

    # mc "Ah...? N-não, é só que-"
    mc ""

# game/script.rpy:490
translate italian start_af32b381:

    # "Eu não consigo tirar o olhar da mão decepada que o professor Stein está segurando. Ela se mexe como normalmente, mas é tão... Surreal."
    ""

# game/script.rpy:492
translate italian start_43f64aec:

    # st "Hmm? Ah! A minha mão te assustou?"
    st ""

# game/script.rpy:494
translate italian start_4464021a:

    # "Stein sorri, balançando a mão decepada, brincando com a própria situação."
    ""

# game/script.rpy:496
translate italian start_668d69b5:

    # mc "N-não, é que... Você está sempre usando o jaleco, então é raro ver você sem ele. É uma camisa muito fofa, sabia?"
    mc ""

# game/script.rpy:498
translate italian start_bb4f2a46:

    # "Eu sorrio, desviando a atenção para a camisa dele."
    ""

# game/script.rpy:500
translate italian start_530198dd:

    # st "Muito gentil da sua parte, mas elogios não vão te dar pontos na prova~"
    st ""

# game/script.rpy:502
translate italian start_1522939a:

    # "Eu cruzo os braços, estufando o peito."
    ""

# game/script.rpy:504
translate italian start_cc4d8ed0:

    # mc "Sabe que não preciso."
    mc ""

# game/script.rpy:506
translate italian start_854a6ea1:

    # "Ele dá uma risadinha, guardando a mão multilada."
    ""

# game/script.rpy:508
translate italian start_0ca7afc3:

    # mc "Então, já vou indo... Minhas amigas estão me esperando."
    mc ""

# game/script.rpy:510
translate italian start_8eb428eb:

    # st "Sim... Espera! Um presentinho pra você."
    st ""

# game/script.rpy:512
translate italian start_0c12781b:

    # "Stein bota a mão restante no bolso e tira um docinho, oferecendo."
    ""

# game/script.rpy:514
translate italian start_ff01f460:

    # st "Aqui. Por ter sido uma boa menina e me ajudado hoje."
    st ""

# game/script.rpy:516
translate italian start_627899b2:

    # "Eu pego o doce, examinando ele com cuidado. A embalagem é fofa."
    ""

# game/script.rpy:518
translate italian start_290a245c:

    # mc "Você... Sempre carrega isso no bolso?"
    mc ""

# game/script.rpy:520
translate italian start_62d7e521:

    # st "Eu gosto. Me ajuda a concentrar no trabalho, já que é feito de açúcar."
    st ""

# game/script.rpy:522
translate italian start_1d7b8fb5:

    # mc "Obrigada. Então, até amanhã professor, não fique até muito tarde."
    mc ""

# game/script.rpy:524
translate italian start_2fcc26e1:

    # st "O mesmo para você. Sei que não precisa revisar tanto, mas não exagere. Durma cedo."
    st ""

# game/script.rpy:526
translate italian start_f2141119:

    # mc "Okay, até!"
    mc ""

# game/script.rpy:528
translate italian start_22f96efe:

    # "Eu me despeço dele e guardo o docinho no bolso da minha roupa. Talvez me ajude durante as revisões."
    ""

# game/script.rpy:530
translate italian start_1e55d1db:

    # "Eu me dirijo para a minha sala, onde Skelly e Merrii me esperavam."
    ""

# game/script.rpy:534
translate italian start_89309cab:

    # mc "Então eu já vou indo, preciso revisar."
    mc ""

# game/script.rpy:536
translate italian start_26909154:

    # st "Não exagere hoje, sim? Coma bem na janta e durma bastante, você não precisa revisar tanto como alguns da sua turma, então descanse."
    st ""

# game/script.rpy:538
translate italian start_da15e0b3:

    # mc "Certo, até amanhã."
    mc ""

# game/script.rpy:540
translate italian start_0b91f0be:

    # st "Até!"
    st ""

# game/script.rpy:542
translate italian start_cc1cd3d3:

    # "Ele se vira e fecha a porta, voltando aos seus afazeres. Eu me dirijo para a minha sala, onde Skelly e Merrii me esperavam."
    ""

# game/script.rpy:548
translate italian start_893ff5f5:

    # mc "Certo. Estou pronta, vamos?"
    mc ""

# game/script.rpy:554
translate italian sala_538e9e59:

    # "-Casa da [povname], no quarto-"
    ""

# game/script.rpy:557
translate italian sala_3b00c92c:

    # sk "Ah~! Não aguento mais essa matéria."
    sk ""

# game/script.rpy:559
translate italian sala_980de9de:

    # me "Você não devia estudar a mesma coisa por tanto tempo, só vai se aborrecer com a matéria."
    me ""

# game/script.rpy:561
translate italian sala_42ff748c:

    # mc "Bom, eu já vi o que precisava. Se quiserem, podemos fazer outra coisa."
    mc ""

# game/script.rpy:563
translate italian sala_6d5c3d0e:

    # sk "Ok!"
    sk ""

# game/script.rpy:565
translate italian sala_1120a4a7:

    # "Skelly se levanta do chão em que havia deitado, sentando perto de mim."
    ""

# game/script.rpy:567
translate italian sala_9da7117b:

    # sk "Seu par pro baile, [povname]. Já decidiu?"
    sk ""

# game/script.rpy:569
translate italian sala_c07f76ad:

    # me "[povname] ainda não tem um par?"
    me ""

# game/script.rpy:571
translate italian sala_e1efa61a:

    # mc "Ainda não... Você já tem, Merrii?"
    mc ""

# game/script.rpy:573
translate italian sala_d5efb881:

    # me "Claro. Fui pedida assim que o diretor anunciou a festa. Por algum motivo..."
    me ""

# game/script.rpy:575
translate italian sala_7e1db39e:

    # sk "Tsc. Bom. Quais são as opções?"
    sk ""

# game/script.rpy:577
translate italian sala_a13d1f20:

    # mc "Hmm... Deixa eu ver..."
    mc ""

# game/script.rpy:581
translate italian sala_45455055:

    # mc "Ah! Tem o Kurusu, mas ele falou que ia decidir mais tarde..."
    mc ""

# game/script.rpy:583
translate italian sala_47f60bae:

    # sk "Kukki? Nem pensar. Ele vai passar o baile inteiro reclamando de estar ali."
    sk ""

# game/script.rpy:585
translate italian sala_47580bda:

    # me "Mais alguém em mente?"
    me ""

# game/script.rpy:587
translate italian sala_ec004ed3:

    # mc "Não... Sabe que não converso com muita gente."
    mc ""

# game/script.rpy:589
translate italian sala_7c24716d:

    # sk "Pois devia. Vai que você acha alguém legal, né?"
    sk ""

# game/script.rpy:591
translate italian sala_1f39b514:

    # me "Bom... Tem aquele menino que fica te olhando na biblioteca."
    me ""

# game/script.rpy:595
translate italian sala_122a3c65:

    # mc "Que menino??"
    mc ""

# game/script.rpy:597
translate italian sala_758a55d3:

    # sk "Vai me dizer que você nunca viu ele, [povname]?"
    sk ""

# game/script.rpy:599
translate italian sala_7d1216be:

    # mc "Não? Como ele é?"
    mc ""

# game/script.rpy:601
translate italian sala_c19e0f88:

    # me "É um rapaz com faixas, de olho puxado..."
    me ""

# game/script.rpy:605
translate italian sala_966d844f:

    # mc "Ah, eu esbarrei com ele no corredor hoje!"
    mc ""

# game/script.rpy:607
translate italian sala_57a6a848:

    # sk "Você só dá bola fora hein [povname]..."
    sk ""

# game/script.rpy:609
translate italian sala_ce47963a:

    # mc "Fica quieta~ Mas porquê ele fica me encarando?"
    mc ""

# game/script.rpy:611
translate italian sala_9ef79d8e:

    # sk "Olha, eu sinceramente não sei. Parece que ele quer falar contigo ou alguma coisa assim."
    sk ""

# game/script.rpy:613
translate italian sala_d295edfd:

    # me "É um pouquinho estranho, mas ele deve ser tímido..."
    me ""

# game/script.rpy:615
translate italian sala_6e741c98:

    # sk "Cê devia começar por ele, já que ele tem interesse~"
    sk ""

# game/script.rpy:617
translate italian sala_d04cfbea:

    # mc "Hmm... Então acho que amanhã eu tento. Ah, mas e se ele já tiver um par?"
    mc ""

# game/script.rpy:619
translate italian sala_8185ea7b:

    # sk "Bom... Nesse caso, acho que é melhor ter mais opções."
    sk ""

# game/script.rpy:621
translate italian sala_4de5e52e:

    # me "[povname], tem algum rapaz na sua sala que te interessa?"
    me ""

# game/script.rpy:623
translate italian sala_16e9c093:

    # mc "Eu não tenho muita certeza... Não conheço eles o suficiente."
    mc ""

# game/script.rpy:625
translate italian sala_87dbe417:

    # sk "Você não precisa conhecer eles. Só precisa ser alguém que você se sinta confortável indo."
    sk ""

# game/script.rpy:627
translate italian sala_983640ae:

    # mc "Humm... Bom, tem o Gomi. Ele é um Oni, senta no fundo da sala."
    mc ""

# game/script.rpy:631
translate italian sala_64ef88c6:

    # mc "Ele é legal, sempre me pede material emprestado antes da aula começar."
    mc ""

# game/script.rpy:633
translate italian sala_5519d115:

    # me "Ele é esquecido? Que fofinho."
    me ""

# game/script.rpy:635
translate italian sala_8a86a8aa:

    # mc "Uhum. Os professores sempre chamam a atenção por ele ficar bagunçando e pregando peças, mas fora isso, ele parece ser legal."
    mc ""

# game/script.rpy:637
translate italian sala_f2ddcabe:

    # sk "Ah, então você gosta dele por ser engraçadinho?"
    sk ""

# game/script.rpy:639
translate italian sala_48ec517f:

    # mc "Não diria gostar... Ele só parece ser amigável."
    mc ""

# game/script.rpy:641
translate italian sala_68d17031:

    # me "Ok... Mais algum que te chame a atenção?"
    me ""

# game/script.rpy:643
translate italian sala_7c1cfb6b:

    # sk "Ah! Ah! Que tal o Hanael!? Ele é super gente fina, e é amigo do meu nenê. Quem sabe ele não arranja alguma coisa pra você?"
    sk ""

# game/script.rpy:647
translate italian sala_12511d63:

    # me "Eu acho ele meio bobinho, mas se a [povname] gostar..."
    me ""

# game/script.rpy:649
translate italian sala_de6e5d89:

    # mc "É, quem sabe. Se ele for divertido..."
    mc ""

# game/script.rpy:651
translate italian sala_a0312251:

    # sk "Bom... Mais alguém?"
    sk ""

# game/script.rpy:653
translate italian sala_ec17c75b:

    # me "Eu conheço um rapaz, mas... Ele é meio tímido."
    me ""

# game/script.rpy:655
translate italian sala_f0bf0da1:

    # mc "E quem seria ele?"
    mc ""

# game/script.rpy:657
translate italian sala_aca51ab2:

    # me "É um fantasma, Damien. Mas fora da classe é meio difícil encontrar ele..."
    me ""

# game/script.rpy:661
translate italian sala_27d5a350:

    # me "Porque ele gosta de ficar invisível pra fugir das pessoas."
    me ""

# game/script.rpy:663
translate italian sala_b78c90dd:

    # mc "Nossa... Mas então porque sugeriu ele? Provavelmente ele nem vai querer ir."
    mc ""

# game/script.rpy:665
translate italian sala_ed8e6a42:

    # me "Porque achei que vocês dois ficariam fofinhos juntos~ "
    me ""

# game/script.rpy:667
translate italian sala_1c41e935:

    # sk "Merrii, para. Foco."
    sk ""

# game/script.rpy:669
translate italian sala_8a43a26d:

    # me "Mas eu falei sério! Acho que a [povname] devia tentar se ela achar ele interessante."
    me ""

# game/script.rpy:671
translate italian sala_1dd3faac:

    # mc "E-eu vou ver... De repente ele é um cara bacana né?"
    mc ""

# game/script.rpy:673
translate italian sala_546d8cb2:

    # "Essa conversa toda está me deixando com vergonha. E se eles não gostarem de mim?"
    ""

# game/script.rpy:675
translate italian sala_2081dab2:

    # "E se eu acabar tendo que ir sozinha para o baile??"
    ""

# game/script.rpy:677
translate italian sala_1e970fb2:

    # me "Sim, sim! Também pode ajudar ele a se abrir mais com as pessoas."
    me ""

# game/script.rpy:679
translate italian sala_f7703ad9:

    # mc "Entendi. Bem, acho que esse é um número razoável de opções."
    mc ""

# game/script.rpy:681
translate italian sala_02596f4c:

    # sk "Ah bom, então você vai começar amanhã?"
    sk ""

# game/script.rpy:683
translate italian sala_b2ace761:

    # mc "Vou sim. A festa tá chegando preciso de alguém logo."
    mc ""

# game/script.rpy:685
translate italian sala_b56dedf1:

    # me "Então é melhor nós irmos dormir! Temos provas amanhã cedinho."
    me ""

# game/script.rpy:687
translate italian sala_948a56ac:

    # sk "Certo!"
    sk ""

# game/script.rpy:689
translate italian sala_46c48878:

    # "Skelly se levantou e imitou a pose de um super-herói, com as mãos na cintura e o olhar fixo no teto."
    ""

# game/script.rpy:691
translate italian sala_426ac29c:

    # sk "Pra cama então, senhoritas!"
    sk ""

# game/script.rpy:693
translate italian sala_56389841:

    # mc "Boa noite!"
    mc ""

# game/script.rpy:697
translate italian sala_65d4c874:

    # "- Segunda de manhã, casa da [povname] -"
    ""

# game/script.rpy:699
translate italian sala_c8efb472:

    # "Eu e a Skelly estamos sentadas na mesa, esperando o café ficar pronto."
    ""

# game/script.rpy:701
translate italian sala_45856559:

    # "Merrii gosta de cozinhar pra gente. Além de ser uma boa comida, é até melhor para nós que somos preguiçosas~"
    ""

# game/script.rpy:703
translate italian sala_f86950d6:

    # sk "Bom, você precisa começar hoje, [povname]. Ainda tem que comprar a roupa que combine com o seu par."
    sk ""

# game/script.rpy:705
translate italian sala_c2751626:

    # mc "Roupa que combine...?"
    mc ""

# game/script.rpy:707
translate italian sala_417f0074:

    # sk "Sim, você quer agradá-lo, não quer?"
    sk ""

# game/script.rpy:709
translate italian sala_91bfaee6:

    # mc "Errr... Acho que sim...?"
    mc ""

# game/script.rpy:711
translate italian sala_4dad4e8a:

    # "Skelly se entusiasma demais quando o assunto se trata de festas."
    ""

# game/script.rpy:713
translate italian sala_fdc4232b:

    # sk "Você tem que estar impecável! Merrii e eu vamos te ajudar com isso, ok? Você só precisa se preocupar em arranjar um par."
    sk ""

# game/script.rpy:715
translate italian sala_dde1eb56:

    # mc "Ceerto!"
    mc ""

# game/script.rpy:717
translate italian sala_52d0b01c:

    # "Merrii se aproxima trazendo os pratos com cuidado, um em cada mão para cada uma de nós."
    ""

# game/script.rpy:719
translate italian sala_aff62c7a:

    # me "Aqui está. Comam logo meninas, não queremos chegar atrasadas!"
    me ""

# game/script.rpy:721
translate italian sala_616fb86d:

    # sk "Não precisa nem falar."
    sk ""

# game/script.rpy:723
translate italian sala_7e7c68ce:

    # "Nós comemos animadas, preocupadas com o horário."
    ""

# game/script.rpy:725
translate italian sala_e8287451:

    # "- Após as provas -"
    ""

# game/script.rpy:727
translate italian sala_8481d0d4:

    # st "Certo! Acabou o tempo. Por favor, guardem seus materiais. A representante vai coletar as provas, o resto está dispensado pelo resto do dia."
    st ""

# game/script.rpy:729
translate italian sala_eaf811d5:

    # mc "Ok, é a minha chance. Hora de escolher com quem passar um tempo!"
    mc ""

# game/script.rpy:740
translate italian sala_150a6999:

    # "Kurusu é meu amigo desde que eu me lembro, vai ser mais divertido ir com alguém que eu conheça."
    ""

translate italian strings:

    # game/script.rpy:445
    old "Na verdade não."
    new ""

    # game/script.rpy:445
    old "Ahm... Ele é um pouquinho estranho."
    new ""

    # game/script.rpy:731
    old "Quem eu devo escolher para ser meu parceiro?"
    new ""

    # game/script.rpy:731
    old "O menino enfaixado"
    new ""

    # game/script.rpy:731
    old "Kurusu"
    new ""

