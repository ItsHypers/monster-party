﻿# TODO: Translation updated at 2020-07-14 20:51

# game/script.rpy:24
translate english start_c6301645:

    # "Olá, bem vinde ao Monster Party. Essa é uma DEMO e conta com 2 mini-rotas. O jogo ainda está em desenvolvimento, críticas e feedback são muito bem vindos."
    "Hello, Welcome to the Monster Party. This is a DEMO and has 2 mini-routes. The game is still in development, criticism and feedback are very welcome."

# game/script.rpy:25
translate english start_fcf37af0:

    # "Por favor, comece dando um nome para a MC."
    "What is your name?"

# game/script.rpy:26
translate english start_5b0d0225:

    # "Deixe em branco se quiser o nome Default."
    "Leave it blank if you want the name to be Default."

# game/script.rpy:36
translate english start_3a335b83:

    # "Seu nome é [povname], aproveite essa DEMO."
    "Hello [povname], enjoy this DEMO."

# game/script.rpy:38
translate english start_e4b8fda4:

    # "~PRÓLOGO~"
    "PROLOGUE"

# game/script.rpy:40
translate english start_5dfa76b6:

    # "Manhã de Segunda, primeiro período."
    "Monday morning, first period."

# game/script.rpy:42
translate english start_a7abaf7d:

    # st "Certo, todos sentados? Então, como vocês sabem o ano letivo está para chegar ao fim, e a escola Lt. Sandalwood vai sediar o nosso baile anual."
    st "Okay, everyone seated? So, as you know, the school year is about to end, and Lt. Sandalwood School will host our annual ball."

# game/script.rpy:44
translate english start_769ae7d6:

    # "Stein começou a andar desengonçado e lentamente pela sala..."
    "Stein started to walk awkwardly and slowly around the room ..."

# game/script.rpy:46
translate english start_05906fcf:

    # "Os alunos o acompanhavam com os olhos enquanto ele continuava a falar."
    "Students followed him with their eyes as he continued to speak"

# game/script.rpy:48
translate english start_9a7de9b1:

    # "Alguns alunos conseguem lidar bem com a presença do professor na sala"
    "Some students are able to cope well with the teacher's presence in the classroom"

# game/script.rpy:50
translate english start_ed2ae0df:

    # "Mas outros... Eu tenho certeza que nem tanto."
    "But others ... I'm not so sure"

# game/script.rpy:52
translate english start_8d615112:

    # "Professor Stein não é exatamente uma pessoa ruim, ou assustadora..."
    "Professor Stein is not exactly a bad person, or scary ..."

# game/script.rpy:54
translate english start_12c81820:

    # "Mas o seu jeito desengonçado, a sua altura... Tudo é um pouquinho intimidante."
    "But his clumsy way, his height ... Everything is a little intimidating"

# game/script.rpy:56
translate english start_cf3a21ea:

    # st "Vocês já tem ideia de par? É um evento muito importante, pois fecha o ciclo escolar de vocês."
    st "do you already have a couple ideas? It is a very important event, as it closes your school cycle."

# game/script.rpy:58
translate english start_2a704e29:

    # "Ah! Realmente, o final do ano tá proximo..."
    "Ah! Really, the end of the year is near ..."

# game/script.rpy:60
translate english start_651a58fa:

    # a2 "Eh, nem sei se vou conseguir mesmo sair daqui, professor."
    a2 "Hey, I don't even know if I'm going to be able to get out of here, professor."

# game/script.rpy:62
translate english start_1e438e09:

    # st "Calma... As provas finais não são tão ruins assim!"
    st "Take it easy ... The final exams are not that bad!"

# game/script.rpy:64
translate english start_5d9e637e:

    # a1 "Pra você é fácil, você quem fez."
    a1 "It's easy for you, you made it"

# game/script.rpy:66
translate english start_aab528e4:

    # st "U-Uh, não é bem assim! Olha, o importante é sempre revisar e-"
    st "U-Uh, not quite! Look, the important thing is always to review e-"

# game/script.rpy:68
translate english start_a19d2ac7:

    # mc "Hm..."
    mc "Hm..."

# game/script.rpy:70
translate english start_05948a62:

    # "Eu sempre acabo divagando nas aulas do professor Stein, e agora que ele mencionou o baile..."
    "I always end up rambling in Professor Stein's classes, and now that he mentioned the dance ..."

# game/script.rpy:72
translate english start_2f3fc115:

    # "Eu não lembrei que precisava de um par."
    "I forgot I needed a date..."

# game/script.rpy:74
translate english start_8ad86641:

    # "A Skelly provavelmente vai com o namorado, a Merrii já deve ter alguém. E o Kurusu não vai querer ir á festa..."
    "Skelly probably goes with her boyfriend, Merrii must have someone already. And Kurusu won't want to go to the party ..."

# game/script.rpy:76
translate english start_2bc8d25b:

    # "Não quero ir sozinha. Preciso encontrar alguém logo."
    "I don't want to go alone. I need to find someone soon."

# game/script.rpy:78
translate english start_83e5368e:

    # mc "Mas é complicado, não tenho interesse em ninguém além dos meus amigos... Ugh."
    mc "But it's complicated, I have no interest in anyone other than my friends ... Ugh."

# game/script.rpy:80
translate english start_042851b4:

    # st "[povname]?"
    st "[povname]?"

# game/script.rpy:82
translate english start_65e6283c:

    # mc "Eu podia pedir pra Skelly me arranjar um par, mas eu não quero depender dela pra isso."
    mc "I could ask Skelly to get me a partner, but I don't want to depend on her for that."

# game/script.rpy:84
translate english start_aaff5573:

    # st "[povname], tudo bem?"
    st "[povname], how are you?"

# game/script.rpy:86
translate english start_ee2636ff:

    # mc "E se eu conseguisse convencer o Kurusu a vir comigo? O problema é que ele não gosta desse tipo de socialização... O que eu vou fazer?"
    mc "What if I managed to convince Kurusu to come with me? The problem is that he doesn't like this kind of socialization ... What am I going to do?"

# game/script.rpy:88
translate english start_b2a19f52:

    # st "[povname]."
    st "[povname]."

# game/script.rpy:90
translate english start_01cfc3e0:

    # mc "Na verdade ele não gosta de nenhum tipo de socialização. ha... ha..."
    mc "In fact, he doesn't like any kind of socializing. ha ha .."

# game/script.rpy:92
translate english start_767786ad:

    # "Stein se aproximou, me dando um peteleco na testa; o devaneio no qual estava perdida — e aparentemente falando sozinha — se dissipando, dando lugar à dor incômoda na minha cabeça por causa do peteleco que me assustou."
    "Stein came over, flicking me on the forehead; the reverie in which I was lost - and apparently speaking to myself - dissipated, giving way to the uncomfortable pain in my head because of the flickering that scared me."

# game/script.rpy:94
translate english start_56edf709:

    # mc "!!"
    mc "!!"

# game/script.rpy:96
translate english start_469d0ae8:

    # st "[povname]!"
    st "[povname]!"

# game/script.rpy:98
translate english start_7d93e45d:

    # "Stein elevou sua voz, colocando uma mão na cintura, me observando."
    "Stein raised his voice, placing a hand on his waist, watching me."

# game/script.rpy:100
translate english start_6541fc31:

    # mc "A-AI! O que foi, p-professor Stein!?"
    mc "A-AI! What is it, p-Professor Stein !?"

# game/script.rpy:102
translate english start_b87ddf3f:

    # st "Eu estava falando com você. Não prestou atenção?"
    st "I was talking to you. Didn't you pay attention?"

# game/script.rpy:104
translate english start_5e8205f1:

    # mc "Desculpa, me perdi um pouco..."
    mc "Sorry, I got a little ... lost"

# game/script.rpy:106
translate english start_b4207cd0:

    # "Stein sorriu, fazendo meu coração dar uma batida mais forte."
    "Stein smiled, making my heart beat faster."

# game/script.rpy:108
translate english start_7325474d:

    # "Embora seja um sorriso um pouco estranho... É genuíno."
    "Although it is a little strange smile ... It is genuine."

# game/script.rpy:110
translate english start_2d1e4451:

    # st "Não fique pra baixo. Hoje é o seu dia de arrumar a classe, não esquece ok? Traz a chave para mim quando terminar, vou estar na sala dos professores."
    st "Don't get down. Today is your day to clean the class, don't forget ok? Bring me the key when you're done, I'll be in the teachers' room"

# game/script.rpy:112
translate english start_a4afdf57:

    # mc "C-certo."
    mc "S-Sure."

# game/script.rpy:114
translate english start_c7fc0bd0:

    # "Eu preciso me organizar logo, duvido que tenha alguém sem par até esse ponto."
    "I need to get organized soon; I bet they are all waiting for me."

# game/script.rpy:116
translate english start_f1eb5d15:

    # "O sinal para o intervalo bateu, é melhor eu me preparar pra descer."
    "The signal for the break, I better get ready to go down..."

# game/script.rpy:118
translate english start_69a3f9d2:

    # "O pessoal já deve estar indo pra cafeteria, os corredores ficavam mais espaçosos e vazios por conta disso..."
    "People must already be going to the cafeteria, the corridors were more spacious and empty than I've ever seen them..."

# game/script.rpy:120
translate english start_2a46c8ca:

    # "Enquanto passava pelo corredor, passei pela porta da sala de aula do Kurusu."
    "As I passed down the hall, I passed the door to Kurusu's classroom."

# game/script.rpy:122
translate english start_fc550b58:

    # "Dei uma espiadinha pela sala."
    "I peeked around the room."

# game/script.rpy:124
translate english start_2e1f403a:

    # mc "Ué. Cadê ele? Já foi embora...?"
    mc "Huh. Where is he? Already gone...?"

# game/script.rpy:128
translate english start_00e8b5a1:

    # k "Me procurando?"
    k "Looking for me?"

# game/script.rpy:130
translate english start_34485c8a:

    # "Um arrepio se espalhou em minha espinha sentindo algo presente apoiando-se em meu ombro, me virando para trás."
    "A shiver spread down my spine, feeling something present leaning on my shoulder, turning me back."

# game/script.rpy:132
translate english start_753ed437:

    # mc "Ah! Kukki!"
    mc "Ah Kukki!!"

# game/script.rpy:134
translate english start_b688c5e3:

    # "Eu solto um suspiro, dando um sorrisinho sem graça."
    "I let out a breath, giving an embarrassed little smile."

# game/script.rpy:136
translate english start_6d05abee:

    # mc "N-não faz mais isso!"
    mc "N-don't do that!"

# game/script.rpy:138
translate english start_89de8caf:

    # "Kurusu sorri, tirando a mão do meu ombro e cruzando os braços enquanto ria do meu susto."
    "Kurusu smiles, taking his hand off my shoulder and crossing his arms while laughing at my fright."

# game/script.rpy:140
translate english start_4ba7a2a9:

    # k "Desculpa, [povname]... O que você queria?"
    k "Sorry, [povname] ... What did you want?"

# game/script.rpy:142
translate english start_1d997f32:

    # mc "A-ahm.... Porque não vem almoçar comigo na lanchonete?"
    mc "A-ahm .... Why don't you come to lunch with me at the cafeteria?"

# game/script.rpy:144
translate english start_464afea6:

    # k "Tudo bem. Esqueceu o dinheiro de novo?"
    k "Okay. Forgot the money again?"

# game/script.rpy:146
translate english start_e7860134:

    # "Isso é vergonhoso. Eu esqueci o dinheiro pro almoço tantas vezes assim?"
    "This is shameful. Do I forget to bring money for lunch often?"

# game/script.rpy:148
translate english start_a4916d41:

    # "Tenho que prestar mais atenção no que faço durante o dia."
    "I have to pay more attention to what I do during the day."

# game/script.rpy:150
translate english start_fb821e6d:

    # mc "N-não, seu idiota. Só queria passar um tempo com você."
    mc "N-no, you idiot. I just wanted to spend time with you."

# game/script.rpy:152
translate english start_5d2d48a3:

    # k "Certo, certo..."
    k "Okay, Sure..."

# game/script.rpy:154
translate english start_fb68cc2b:

    # mc "Mas se quiser me comprar um bolinho, eu aceito!"
    mc "But if you want to buy me a cupcake, I accept!"

# game/script.rpy:156
translate english start_c7b2be85:

    # "Com um sorriso bobo no rosto, Kurusu deu um tapa leve nas minhas costas, brincando."
    "With a silly smile on his face, Kurusu slapped my back lightly, playfully."

# game/script.rpy:158
translate english start_57dd886a:

    # k "Porque você ~só quer passar um tempo comigo~, certo?"
    k "Because you ~ just want to spend time with me ~, right?"

# game/script.rpy:160
translate english start_b291e46b:

    # "Eu gosto da amizade que temos. Desde pequenos, Kurusu é gentil comigo. Eu espero que isso nunca mude..."
    "I like the friendship we have. Since childhood, Kurusu has been kind to me. I hope it never changes ..."

# game/script.rpy:162
translate english start_2bacafe6:

    # "Ele sempre gostou de me mostrar bichinhos que encontrava. Lagartinhas, besourinhos, borboletas de todas as cores... Mas nunca pôde tocar, por causa dos poderes da sua mãe."
    "He always liked to show me pets he found. Caterpillars, beetles, butterflies of all colors ... But he was never able to touch them, because of his mother's powers."

# game/script.rpy:164
translate english start_2e7985eb:

    # "Quando percebi que estava absorvida em pensamentos novamente, já tínhamos chegado na lanchonete."
    "When I realized I was absorbed in thoughts again, we had already arriving at the cafeteria."

# game/script.rpy:166
translate english start_6191296b:

    # "- Na lanchonete -"
    "The Cafeteria"

# game/script.rpy:168
translate english start_628d8534:

    # "Kurusu me dá um toque no braço, direcionando meu corpo para dar um passo para frente. Olho para ele e o mesmo aponta para uma mesa vazia da lanchonete."
    "Kurusu gives me a touch on the arm, directing my body to take a step forward. I look at him and he points to an empty cafeteria table."

# game/script.rpy:170
translate english start_9ac67b12:

    # k "Vá se sentar. Eu pego o almoço e o seu bolinho, me espere lá."
    k "Go sit. I'll get lunch and your cupcake, wait for me there."

# game/script.rpy:172
translate english start_af0c5e96:

    # mc "Oh. Obrigada Kukki!"
    mc "Oh. Thank you Kukki!"

# game/script.rpy:174
translate english start_c4c1e2cb:

    # "Ele dá um ~tchauzinho~ com a mão, virando-se em direção á lanchonete."
    "He gives a ~ bye-bye ~ with his hand, turning towards the cafeteria."

# game/script.rpy:176
translate english start_217e9ae2:

    # "Eu sorrio, observando ele fazer o trajeto."
    "I smile, watching him make his way."

# game/script.rpy:178
translate english start_e67edb61:

    # "Kurusu sempre foi mais aberto comigo, isso me deixa feliz."
    "Kurusu has always been more open with me, it makes me happy"

# game/script.rpy:180
translate english start_42662dea:

    # "Ele não brincava com as outras crianças porque tinha medo de tocar nelas, mas nunca foi assim comigo. Eu nunca soube o por quê. Talvez por me conhecer melhor? Por se sentir mais confortável?"
    "He did not play with the other children because he was afraid to touch them, but it was never like that for me. I never knew why. Maybe because he knew me better? Why feel more comfortable?"

# game/script.rpy:182
translate english start_cea545be:

    # "Saber que eu entendia o problema dele? Eu imagino que sim."
    "Knowing that I understood his problem? Maybe."

# game/script.rpy:184
translate english start_6da936da:

    # "Nossos pais sempre foram muito próximos, e quando o humano da Morte se foi, minha mãe fez de tudo para ajudá-la..."
    "Our parents were always very close, and when the Human of Death was gone, my mother did everything to help her ..."

# game/script.rpy:186
translate english start_336f042f:

    # "O que resultou com Kurusu morando conosco por um tempo, por conta do trabalho da mãe de Kurusu."
    "Which resulted in Kurusu living with us for a while, due to the work of Kurusu's mother."

# game/script.rpy:188
translate english start_48436560:

    # "Foi bem divertido, ficávamos até tarde brincando e lendo quadrinhos."
    "It was really fun, we stayed up late playing and reading comics. "

# game/script.rpy:190
translate english start_572a0270:

    # "Claro que ele evita me tocar por muito tempo, mas ele gosta de ser meu amigo. E isso é o mais importante agora."
    "Of course he avoids touching me for a long time, but he likes to be my friend. And that is the most important thing now."

# game/script.rpy:192
translate english start_f5d466cc:

    # "Só queria que ele perdesse um pouco dessa tensão quando o assunto é socializar. Ir no baile vai ser uma ótima oportunidade pra gente passar um tempo se divertindo."
    "I just wanted him to lose some of that tension when it comes to socializing. Going to the ball will be a great opportunity for us to spend time having fun."

# game/script.rpy:195
translate english start_4b084085:

    # "Eu entendo que ele é naturalmente tímido, mas..."
    "I understand that he is naturally shy, but ..."

# game/script.rpy:197
translate english start_e0d59b8c:

    # "Kurusu voltou com as duas bandejas, uma delas com o bolinho que ele havia prometido mais cedo."
    "Kurusu came back with the two trays, one with the cupcake he had *promised* earlier."

# game/script.rpy:199
translate english start_48441590:

    # k "Perdida nos pensamentos de novo, [povname]?"
    k "Lost in thought again, [povname]?"

# game/script.rpy:201
translate english start_76406145:

    # "A timidez dele é um dos charminhos que o deixam único."
    "His shyness is one of the charms that make him unique."

# game/script.rpy:203
translate english start_1ae8a70f:

    # mc "Mais ou menos. As provas chegando sempre me deixam nervosa..."
    mc "More or less. The results coming always makes me nervous ..."

# game/script.rpy:205
translate english start_b9873805:

    # k "Qualquer coisa você pode fazer as recuperações, não se preocupa."
    k "Anything you can do to feel better, don't worry"

# game/script.rpy:207
translate english start_1e65d00f:

    # mc "Prefiro não precisar delas..."
    mc "Thank you."

# game/script.rpy:209
translate english start_872716b6:

    # "Ele olha para mim com um sorriso de canto nos lábios."
    "He looks at me with a singing smile on his lips."

# game/script.rpy:211
translate english start_4954e144:

    # k "Você vai conseguir, [povname]."
    k "You will make it, [povname]."

# game/script.rpy:213
translate english start_60b45f9f:

    # "Logo muda o foco para o seu sanduíche, mordendo com grande vontade."
    "He then shifts the focus to his sandwich, biting with great will."

# game/script.rpy:215
translate english start_7a1caf97:

    # "Eu me ajeito na cadeira, limpando a garganta levemente."
    "I adjust myself in the chair, clearing my throat slightly."

# game/script.rpy:217
translate english start_417c472b:

    # mc "Então, sobre o baile..."
    mc "So, about the prom ..."

# game/script.rpy:219
translate english start_67a08757:

    # "Kurusu arqueou uma sobrancelha e respirou fundo, revirando os olhos e mantendo-se a mastigar o alimento."
    "Kurusu raised an eyebrow and took a deep breath, then rolls his eyes and keeping chewing on the food"

# game/script.rpy:221
translate english start_22a3e58b:

    # k "Não vou."
    k "I  will not go."

# game/script.rpy:223
translate english start_102fd0a9:

    # mc "Ah, qual é! Por que não?"
    mc "Ah, come on! Why not?"

# game/script.rpy:225
translate english start_637e7053:

    # k "Sabe que não gosto de multidões..."
    k "You know I don't like crowds..."

# game/script.rpy:227
translate english start_cca1d595:

    # mc "Mas gosta de mim, não gosta?"
    mc "But you like me, don't you?"

# game/script.rpy:229
translate english start_d10e1db0:

    # "Kurusu ri, apoiando o rosto em uma das mãos. O pão segurado firmemente na outra mão com uma marca de mordida no meio."
    "Kurusu laughs, resting his face on one hand. The bread held tightly in the other hand with a bite mark in the middle."

# game/script.rpy:231
translate english start_fd463d6b:

    # k "Será que gosto?"
    k "Do I?"

# game/script.rpy:233
translate english start_95186d4f:

    # "Eu apoio o cotovelo na mesa imitando a pose do Kurusu, brincando com ele."
    "I support my elbow on the table imitating Kurusu's pose, playing with him."

# game/script.rpy:235
translate english start_61478f40:

    # mc "Claro que eu gosto da [povname]! Eu prometi que ia casar com ela quando nós éramos crianças, e eu gosto de manter as minhas promessas."
    mc "\"Of course I like [povname]! I promised that I would marry them when we were kids, and I like to keep my promises.\""

# game/script.rpy:237
translate english start_c8cfe468:

    # "Eu digo, e ele assiste a atuação com uma expressão tediosa mas ainda com um sorriso."
    "I say, and he watches the performance with a tedious expression but still with a smile."

# game/script.rpy:239
translate english start_687cb384:

    # k "Você falando assim talvez eu acredite. Repete, vai que dá certo dessa vez."
    k "You talking like that might make me believe. Repeat it, it will work this time."

# game/script.rpy:241
translate english start_ea58c129:

    # mc "E ainda pedi para a lua ser nossa testemunha. Nunca vou esquecer aquela noite..."
    mc "\"And I even asked the moon to be our witness. I will never forget that night...\""

# game/script.rpy:243
translate english start_3bdd2ae9:

    # "Kurusu ri baixinho com um leve rubor em suas bochechas."
    "Kurusu laughs softly with a slight flush on his cheeks"

# game/script.rpy:245
translate english start_9270301c:

    # k "Uhum. E o que mais? Também pedi pro Sol abençoar nosso futuro? É por isso que você não larga do meu pé assim?"
    k "Mmm yes. And what else? Did I also ask the Sun to bless our future? Is that why you don't let go of my foot like that?"

# game/script.rpy:247
translate english start_57310957:

    # "Eu dou um sorrisinho, voltando a me sentar ereta no banquinho da cantina."
    "I smile, returning to sit upright on the canteen stool."

# game/script.rpy:249
translate english start_441b5fb9:

    # mc "Para, eu tô falando sério... Eu não quero ir sozinha Kukki, por favor!"
    mc "Stop, I'm serious ... I don't want to go alone Kukki, please!"

# game/script.rpy:251
translate english start_fb9f910d:

    # "Kurusu encolhe e desvia o olhar, corando nas extremidades das bochechas. Ele segurou o sanduíche com as duas mãos e o encarou por um momento."
    "Kurusu shrinks and looks away, blushing at the ends of his cheeks. He held the sandwich in both hands and stared at it for a moment."

# game/script.rpy:253
translate english start_987688f0:

    # k "Hum... Eu vou pensar a respeito."
    k "I'll think about it."

# game/script.rpy:255
translate english start_258c6bca:

    # mc "Obrigada, você é o melhor!"
    mc "Thank you, you are the best!!"

# game/script.rpy:257
translate english start_1ef3490a:

    # "Kurusu deu mais uma mordida no sanduíche, o rubor desaparecendo aos pouco do seu rosto, voltando ao tom normal da sua pele cinzenta."
    "Kurusu took another bite of the sandwich, the blush disappearing little by little from his face, returning to the normal tone of his gray skin."

# game/script.rpy:259
translate english start_579a0c5f:

    # "Skelly e o namorado sentam na mesa, Kyte acenou e Skelly deu um beijo rápido na bochecha da [povname]."
    "Skelly and her boyfriend sit at the table, Kyte waved and Skelly gave [povname] a quick kiss on the cheek"

# game/script.rpy:261
translate english start_545a5be8:

    # sk "Oi! Tudo bem, Kukki?"
    sk "Hi! How are you, Kusumu?"

# game/script.rpy:263
translate english start_c3290b02:

    # k "Olá."
    k "Hello!"

# game/script.rpy:265
translate english start_d1bf1a29:

    # mc "Oi Skelly, oi Kyte!"
    mc "Hey Skelly! Hey Kyte!"

# game/script.rpy:267
translate english start_482dbd85:

    # ky "Falaê. De boa?"
    ky "How are you doing?"

# game/script.rpy:269
translate english start_d55ae269:

    # mc "Yep!"
    mc "Good!"

# game/script.rpy:271
translate english start_9fe709ea:

    # sk "Então, baile! Você já foi convidada? Já sabe com que vestido ir?"
    sk "So, prom! Have you ever been invited? Do you know what you will be wearing?"

# game/script.rpy:273
translate english start_f2d09a2d:

    # mc "Não..."
    mc "No..."

# game/script.rpy:275
translate english start_36a4c141:

    # sk "Ah, mas isso é ótimo. Chama a Merrii, nós vamos pra sua casa e te ajudamos com isso. Aposto que não irá se arrepender."
    sk "Oh. Well this is great. Call Merrii, we will go to your house and help you with that. I bet you won't regret it!"

# game/script.rpy:277
translate english start_0652cbcf:

    # mc "Ah? Okay, mas sabe que a minha mãe não gosta de visitas surpresa, então se comporta."
    mc "Oh? Okay, but you know my mom doesn't like surprise visits, so just behave"

# game/script.rpy:279
translate english start_e468f2a8:

    # sk "Ela já nos conhece. Não tem problema, né?"
    sk "She already knows us, so... No problem, right?"

# game/script.rpy:281
translate english start_57ce590e:

    # "Kurusu suspira e coloca o capuz, terminando de comer o sanduíche."
    "Kurusu sighs and puts on the hood, finishing eating the sandwich"

# game/script.rpy:283
translate english start_52f0290d:

    # mc "Não tem não... Então, você vai com o Kyte?"
    mc "No... So, are you going with Kyte?"

# game/script.rpy:285
translate english start_4c022535:

    # sk "Claro! Meu amorzinho e eu adoramos dançar!"
    sk "Of course! My sweetheart, and I love to dance!"

# game/script.rpy:287
translate english start_edfd26d7:

    # "Skelly dá um beijinho na bochecha do Kyte, passando um braço pelo braço dele. Ele reage coçando a nuca com vergonha."
    "Skelly kisses Kyte on the cheek, putting an arm around his arm. He reacts by scratching the back of his head in shame"

# game/script.rpy:289
translate english start_6ebb7d79:

    # ky "Eu já curto mais pela comida mesmo."
    ky "I enjoy it more for the food."

# game/script.rpy:291
translate english start_f13f4b19:

    # "Skelly deu um tapinha no braço dele, rindo enquanto aperta o namorado."
    "Skelly pats his arm, laughing as she squeezed her boyfriend"

# game/script.rpy:293
translate english start_2c5db5fb:

    # sk "Ai, para. Eu sei que você gosta de sair comigo."
    sk "Oh, stop. I know you like to go out with me."

# game/script.rpy:295
translate english start_6c085138:

    # ky "Você sabe que sim, é divertido!"
    ky "You know it! it's fun!"

# game/script.rpy:297
translate english start_633dcbe7:

    # "Kurusu solta uma risada baixa, me olhando e desviando o olhar para a mesa em seguida."
    "Kurusu lets out a low laugh, looking at me and looking away at the table next."

# game/script.rpy:299
translate english start_6a10c08f:

    # mc "Ah sim, eu convidei o Kurusu pro baile..."
    mc "Oh, I invited Kurusu to the dance ..."

# game/script.rpy:301
translate english start_61880f6d:

    # sk "O Kukki? E você vai?"
    sk "Kukki? And you? go to the prom?"

# game/script.rpy:303
translate english start_7efd5ec0:

    # "Skelly muda de posição, sentando de frente pro Kurusu. Os dois se encararam por alguns segundos."
    "Skelly changes position, sitting facing Kurusu. The two stared at each other for a few seconds."

# game/script.rpy:305
translate english start_81f15860:

    # "Ele se encolhe, mantendo uma certa distância, respondendo baixo."
    "He cringes, keeping his distance, answering low."

# game/script.rpy:307
translate english start_ff425589:

    # k "Eu não sei ainda. Sabe como é..."
    k "I do not know yet. You know how it is.."

# game/script.rpy:309
translate english start_9f43eb45:

    # mc "Ai, eu não quero acabar tendo que ir sozinha~"
    mc "Oh, I don't want to end up having to go alone ~"

# game/script.rpy:311
translate english start_e7d8bfe9:

    # sk "Nem precisa! Merrii e eu arrumamos um par pra você."
    sk "You Don't need to! Merrii and I got you a partner."

# game/script.rpy:313
translate english start_cfa9f478:

    # k "H-hm."
    k "H-hm..."

# game/script.rpy:315
translate english start_db3fd8c2:

    # "Kurusu se mexe na cadeira, desconfortável com o rumo que a conversa está seguindo."
    "Kurusu moves in his chair, uncomfortable with the direction the conversation is taking."

# game/script.rpy:317
translate english start_0174b114:

    # mc "Mas eu não quero depender de vocês. Consigo me virar."
    mc "But I don't want to depend on you. I can turn around"

# game/script.rpy:319
translate english start_cb43b6ba:

    # ky "Se eu fosse você, confiava na Skelly... Digo, ela tem bom gosto né. Acabou me escolhendo."
    ky "If I were you, I'd trust Skelly... I mean, she has good taste. She ended up choosing me."

# game/script.rpy:321
translate english start_a2724d52:

    # "Kyte ri, fazendo Skelly revirar os olhos com o comentário. Ela não consegue segurar uma risadinha, dando um tapinha de leve no braço do namorado."
    "Kyte laughs, making Skelly roll her eyes at the comment. She can't help but giggle, patting her boyfriend on the arm."

# game/script.rpy:323
translate english start_4447ca22:

    # sk "Cala a boca, bobão. Vamos [povname], vai ser divertido."
    sk "Shut up, silly. Come on [povname], it will be fun."

# game/script.rpy:325
translate english start_2417551f:

    # mc "Tá, tá."
    mc "Ok, ok."

# game/script.rpy:327
translate english start_36f8987e:

    # "O sinal bate a tempo de eu terminar o meu almoço. O tempo passou rápido desta vez, devo ter me distraído enquanto conversava com eles."
    "The bell rings in time for me to finish my lunch. Time passed quickly this time, I must have been distracted while talking to them."

# game/script.rpy:329
translate english start_0b035321:

    # sk "Então [povname], me espera na entrada do colégio. Vamos pegar a Merrii e ir pra sua casa."
    sk "So [povname], wait for me at the entrance to the school. Let's get Merrii and go to your place."

# game/script.rpy:331
translate english start_47567f50:

    # ky "E eu não posso ir não?"
    ky "Can I not come?"

# game/script.rpy:333
translate english start_87134ea0:

    # sk "Não, você vai pra casa. Precisa estudar pra sua prova, lembra?"
    sk "No, you go home. You need to study for your exam, remember?"

# game/script.rpy:335
translate english start_961ba9a4:

    # ky "Ah, é..."
    ky "Oh yes, I do."

# game/script.rpy:337
translate english start_aedac7c5:

    # ky "Kurusu, quer jogar hoje?"
    ky "Kurusu, do you want to play today?"

# game/script.rpy:339
translate english start_ac5819b1:

    # k "Claro."
    k "Sure"

# game/script.rpy:341
translate english start_b5835a1b:

    # "Skelly revirou os olhos, vendo que seu namorado parecia ~extremamente~ atento às provas que viriam."
    "Skelly rolled her eyes, seeing that her boyfriend seemed ~ extremely ~ attentive to the sport to come."

# game/script.rpy:343
translate english start_04909c5c:

    # sk "Então, eu preciso ir. Tenho aula de Rituais agora e não posso entrar atrasada. Até!"
    sk "So, I need to go. I have ritual class now and I can't be late. Until later!"

# game/script.rpy:345
translate english start_8ab17447:

    # mc "Tchauzinho!"
    mc "Bye-bye!"

# game/script.rpy:347
translate english start_311b6c22:

    # k "Bye."
    k "Bye!"

# game/script.rpy:349
translate english start_28a3554d:

    # ky "Até mais, chuchu!"
    ky "See ya!"

# game/script.rpy:351
translate english start_56e3a498:

    # "Nos levantamos das cadeiras e cada um segue em direção á sua sala."
    "We get up from the chairs and each one heads towards his room."

# game/script.rpy:353
translate english start_4f39e534:

    # "Chegando no corredor, alguém acaba esbarrando em mim. Meu corpo cai no chão e eu acabo ficando desnorteada."
    "Arriving in the corridor, someone ends up bumping into me. My body falls to the floor and I end up bewildered."

# game/script.rpy:355
translate english start_d0a1b7f5:

    # mc "A-ai..! Cuidado!"
    mc "Ah! Watch out!"

# game/script.rpy:357
translate english start_007099b4:

    # who "O-oh... Desculpa, você está bem?"
    who "O-oh ... Sorry, are you okay?"

# game/script.rpy:359
translate english start_f90474e1:

    # "Ele estende uma mão, me ajudando a levantar"
    "He extends a hand, helping me get up"

# game/script.rpy:361
translate english start_21a308ac:

    # "Eu percebo que ele está todo enfaixado... Não deve ser por acidente, então ele deve ser uma múmia."
    "I realize he's all wrapped up... he must be a mummy."

# game/script.rpy:363
translate english start_15bcede4:

    # mc "Obrigada... Você se machucou?"
    mc "Thank you ... Did you get hurt?"

# game/script.rpy:365
translate english start_65a4aa16:

    # who "Não, eu... A aula!"
    who "No; Im late to class!"

# game/script.rpy:367
translate english start_59e3f2fa:

    # "Ele solta a mão que segurava a minha e corre em direção ás salas. Deve ser aula de Rituais também..."
    "He lets go of my hand and runs towards the rooms. It must be a ritual class too ..."

# game/script.rpy:369
translate english start_798515b3:

    # mc "Heheh. Espero que ele chegue a tempo."
    mc "Heheh. I hope he gets there in time."

# game/script.rpy:371
translate english start_29867f2e:

    # "- Sala de aula -"
    "Classroom"

# game/script.rpy:373
translate english start_0479f07f:

    # st "Não se esqueçam de fazer um resumo da matéria de hoje. Amanhã temos um teste e esse conteúdo vai cair. Podem ir, bom descanso pra vocês."
    st "Don't forget to analyse today's story. Tomorrow we have a test and it will be included. You can go, have a good rest!"

# game/script.rpy:375
translate english start_ffc5f795:

    # "Os alunos começam a sair aos poucos, alguns em grupos e outros sozinhos. E os murmúrios de conversas se afastavam com o tempo."
    "Students start to leave little by little, some in groups and others alone. And the murmurs of conversation drifted away over time."

# game/script.rpy:377
translate english start_2019f839:

    # "Quando terminei de arrumar minhas coisas, O professor Stein me chamou a atenção."
    "When I finished packing, Professor Stein caught my eye."

# game/script.rpy:379
translate english start_a7d9d489:

    # st "[povname]? Você se esqueceu do que eu pedi hoje mais cedo?"
    st "[povname]? Did you forget what I asked earlier today?"

# game/script.rpy:381
translate english start_afdb4a9d:

    # mc "Eh? Ah, é verdade! Desculpa professor, eu ando com a cabeça muito ocupada."
    mc "Eh? Ah, you're right! Sorry, professor, I'm very busy"

# game/script.rpy:383
translate english start_c6531220:

    # st "Pensando em meninos, hmm? Na sua idade eu também era assim."
    st "Thinking about boys, hmm? At your age, I was like that too."

# game/script.rpy:385
translate english start_f236081a:

    # "Ele se apoia em uma das mesas, dando um sorrisinho, me observando terminar de organizar o material."
    "He leans on one of the tables, smiling, watching me finish organizing the material."

# game/script.rpy:387
translate english start_bf70295e:

    # mc "Ah? Não! Não é isso não! Eu só.... Espera, você também era assim?"
    mc "Oh? No! No it's not that! I just.... Wait, were you like that too?"

# game/script.rpy:389
translate english start_bb8d706e:

    # st "É... Eu sempre fui meio careta. Embora eu tenha dado tudo de mim, meus relacionamentos não duraram muito."
    st "Yeah ... I was always kind of grimace. Although I gave my all, my relationships did not last long."

# game/script.rpy:391
translate english start_8c0fef3d:

    # "Ele ri baixinho, cruzando os braços e fazendo um pequeno ~não~ com a cabeça"
    "He laughs softly, crossing his arms and making a little ~ no ~ with his head"

# game/script.rpy:393
translate english start_03634cc3:

    # mc "Ah, sinto muito por isso. Mas não é por esse motivo que eu estou distraída."
    mc "Ah, I'm sorry about that. But that's not why I'm distracted"

# game/script.rpy:395
translate english start_474b5ebe:

    # st "Certo... Então se precisar conversar já sabe, pode chamar o Stein sem problemas~"
    st "to ... So if you need to talk you know, you can call Stein without any problems ~"

# game/script.rpy:397
translate english start_cf38b91e:

    # mc "Hihih~ Pode deixar professor Stein~"
    mc "Hahaha ~ You can leave Professor Stein ~"

# game/script.rpy:399
translate english start_6430f996:

    # "Stein levanta da mesa, passando as mãos levemente no jaleco, ajeitando."
    "Stein gets up from the table, running his hands lightly over his coat, adjusting."

# game/script.rpy:401
translate english start_f67e35f6:

    # st "Bom, eu vou pra sala checar se está tudo preparado para a prova de amanhã. Você vai ficar bem?"
    st "Well, I'm going to the room to check if everything is ready for tomorrow's test. You'll be fine?"

# game/script.rpy:403
translate english start_bca8c66e:

    # "Eu dou um sorrisinho, arrastando minha cadeira para perto da mesa."
    "I smirk, dragging my chair over to the table"

# game/script.rpy:405
translate english start_b7b78489:

    # mc "Vou ficar sim, não se preocupa! Logo mais eu te entrego a chave."
    mc "I'll stay, don't worry! Soon I'll give you the key."

# game/script.rpy:407
translate english start_885e3bd9:

    # st "Certo... Até, [povname]."
    st "Sure, See you, [povname]"

# game/script.rpy:409
translate english start_a376f4e6:

    # "Stein sai da sala e eu fico sozinha enquanto arrumava as coisas"
    "Stein leaves the room and I am alone while I pack up"

# game/script.rpy:411
translate english start_cbfdf261:

    # "Era estranho quando os alunos não estavam presentes, mas também parecia que eu possuía uma sala inteira para mim."
    "It was strange when the students were not present, but it also felt like I had a whole room to myself"

# game/script.rpy:413
translate english start_e393b2bc:

    # mc "Droga... Eu marquei de encontrar as meninas agora."
    mc "Damn ... I made an appointment to meet the girls now"

# game/script.rpy:415
translate english start_b67ec3f9:

    # mc "Vou mandar uma mensagem pra Skelly."
    mc "I'll send a message to Skelly"

# game/script.rpy:417
translate english start_75bc79ad:

    # "Eu envio uma mensagem no nosso grupo pessoal, avisando que eu havia ficado responsável pela limpeza de hoje."
    "I send a message to our personal group, saying that I was responsible for cleaning today"

# game/script.rpy:419
translate english start_94830479:

    # "Elas respondem dizendo que vão vir me ajudar com a sala, assim voltamos mais cedo para casa."
    "They respond by saying that they will come and help me with the room, so we return home early."

# game/script.rpy:421
translate english start_00805f01:

    # mc "Certo, elas devem chegar logo..."
    mc "Okay, they should be here soon ..."

# game/script.rpy:423
translate english start_0445db6d:

    # "Após alguns minutos elas chegam, e nós nos dividimos entre as tarefas restantes."
    "After a few minutes they arrive, and we are divided between the remaining tasks."

# game/script.rpy:425
translate english start_c8881d91:

    # "Terminamos em questão de minutos, e logo estávamos prontas para ir para casa."
    "We were done in a matter of minutes, and soon we were ready to go home."

# game/script.rpy:427
translate english start_5226b824:

    # me "Uuf! Tudo pronto então?"
    me "Wow! Everything ready then?"

# game/script.rpy:429
translate english start_b8d0bc2e:

    # sk "Até que não demorou tanto!"
    sk "It didn't take that long!"

# game/script.rpy:431
translate english start_d27ef349:

    # mc "Ainda bem né. Vamos? Preciso entregar a chave pro professor Stein."
    mc "Thankfully, right. We going? I need to hand over the key to Professor Stein"

# game/script.rpy:433
translate english start_a792fffc:

    # me "Ahh... Nós te esperamos do lado de fora da sala."
    me "Ahh ... We wait for you outside the room"

# game/script.rpy:435
translate english start_e6d06181:

    # sk "E deixar a [povname] sozinha com o Stein? Você não bate bem né, Merrii?"
    sk "And leave [povname] alone with Stein? You don't feel well, right, Merrii?"

# game/script.rpy:437
translate english start_22f056a3:

    # mc "Sozinha? Como assim?"
    mc "Alone? Feel well??"

# game/script.rpy:439
translate english start_e89986d7:

    # sk "É... Ele é estranho."
    sk "Yeah ... He's weird."

# game/script.rpy:441
translate english start_9b90a189:

    # mc "Estranho?"
    mc "Weird?"

# game/script.rpy:443
translate english start_2c71084f:

    # sk "Sim, você não acha? Ele bebe líquido de bateria como se fosse um suco de caixinha!"
    sk "Yes, don't you think? He drinks liquid from the battery as if it were a juice box!"

# game/script.rpy:449
translate english start_3e46b9e0:

    # sk "Você não acha?"
    sk "You don't agree?"

# game/script.rpy:450
translate english start_63899158:

    # mc "É o jeito dele, Skelly. Não fala assim..."
    mc "It's his way, Skelly. Don't talk like that ..."

# game/script.rpy:451
translate english start_d1641316:

    # sk "Ok, mas você vai lá. A gente te espera aqui!"
    sk "Okay, well you go there. We are waiting for you here!"

# game/script.rpy:452
translate english start_6b91e1d4:

    # mc "Tá. Não vou demorar."
    mc "OK. I will not be long."

# game/script.rpy:456
translate english start_eb0fa930:

    # sk "Né? Mas você vai mesmo assim?"
    sk "Huh? But are you going anyway?"

# game/script.rpy:457
translate english start_375a5ed9:

    # mc "Sim. Eu preciso entregar a chave pra ele."
    mc "Yes I need to hand the key to him"

# game/script.rpy:458
translate english start_29191569:

    # sk "Ok, sem problema."
    sk "Alright, no problem."

# game/script.rpy:459
translate english start_dac780a8:

    # mc "Não vou demorar, só um segundo."
    mc "I won't be long, just a second"

# game/script.rpy:462
translate english start_60436f4d:

    # "Eu bati na porta, de pé em frente à sala que esperava ser aberta."
    "I knocked on the door, standing in front of the room."

# game/script.rpy:464
translate english start_69876dd5:

    # "Enquanto espero ser atendida, ouço o barulho de coisas caindo, alguém batendo em algo..."
    "As I wait to be answered, I hear the sound of things falling, someone hitting something."

# game/script.rpy:466
translate english start_200096b6:

    # "Fico confusa, mas tento ignorar e esperar."
    "I get confused, but ignore it and wait."

# game/script.rpy:468
translate english start_73ded2c0:

    # "Stein abre a porta sem o seu jaleco de professor, segurando uma das suas mãos, caída, na outra."
    "Stein opens the door without his professor's coat, holding one hand, drooping, in the other."

# game/script.rpy:470
translate english start_5938e9ac:

    # "Tentando se apoiar na porta, seu óculos basicamente caindo do seu rosto..."
    "Trying to lean on the door, his glasses basically falling off his face .."

# game/script.rpy:474
translate english start_950bca37:

    # "É uma cena cômica e ao mesmo tempo preocupante."
    "It is like a comic scene and at the same time, somewhat worrisome."

# game/script.rpy:476
translate english start_5ad0214e:

    # st "Ah, [povname]! Você já terminou de arrumar a sala?"
    st "Ah, [povname]! Have you finished cleaning up the room?"

# game/script.rpy:478
translate english start_38a495bc:

    # mc "Sim, aqui está a chave."
    mc "Yes, here's the key"

# game/script.rpy:480
translate english start_fc5f6c39:

    # st "Oh, obrigado."
    st "oh thank you"

# game/script.rpy:482
translate english start_4746b5fb:

    # "Stein pega a chave, guardando no bolso da calça."
    "Stein takes the key, keeping it in his pants pocket"

# game/script.rpy:486
translate english start_52a0ae6e:

    # st "Você precisa de mais alguma coisa?"
    st "Do you need something else?"

# game/script.rpy:488
translate english start_98c3e564:

    # mc "Ah...? N-não, é só que-"
    mc "Ah...? N-no, its just-"

# game/script.rpy:490
translate english start_af32b381:

    # "Eu não consigo tirar o olhar da mão decepada que o professor Stein está segurando. Ela se mexe como normalmente, mas é tão... Surreal."
    "I couldnt take my eyes off his severed hand that professor Stein's holding. She's moving as always but its so... Surreal."

# game/script.rpy:492
translate english start_43f64aec:

    # st "Hmm? Ah! A minha mão te assustou?"
    st "Hmm? Ah! Did my hand frighten you?"

# game/script.rpy:494
translate english start_4464021a:

    # "Stein sorri, balançando a mão decepada, brincando com a própria situação."
    "Stein smiles, swinging his severed hand. He's joking with his own situation."

# game/script.rpy:496
translate english start_668d69b5:

    # mc "N-não, é que... Você está sempre usando o jaleco, então é raro ver você sem ele. É uma camisa muito fofa, sabia?"
    mc "N-No, it's... You're always using your labs coat. So its kinda unusual to see you without it. Do you know it acually a very cute shirt you've got in there?"

# game/script.rpy:498
translate english start_bb4f2a46:

    # "Eu sorrio, desviando a atenção para a camisa dele."
    "I smile, taking my eyes off his hand. It's kinda rude so I just stare at his shirt."

# game/script.rpy:500
translate english start_530198dd:

    # st "Muito gentil da sua parte, mas elogios não vão te dar pontos na prova~"
    st "That's very kind of you, but compliments arent getting you points on your exams~"

# game/script.rpy:502
translate english start_1522939a:

    # "Eu cruzo os braços, estufando o peito."
    "I cross my arms, puffing my chest outwards"

# game/script.rpy:504
translate english start_cc4d8ed0:

    # mc "Sabe que não preciso."
    mc "You know I dont need them."

# game/script.rpy:506
translate english start_854a6ea1:

    # "Ele dá uma risadinha, guardando a mão multilada."
    "He chuckles, pocketing his severed hand."

# game/script.rpy:508
translate english start_0ca7afc3:

    # mc "Então, já vou indo... Minhas amigas estão me esperando."
    mc "So, I'll be going... My frends are waiting for me."

# game/script.rpy:510
translate english start_8eb428eb:

    # st "Sim... Espera! Um presentinho pra você."
    st "Yea... Wait! A little present for you."

# game/script.rpy:512
translate english start_0c12781b:

    # "Stein bota a mão restante no bolso e tira um docinho, oferecendo."
    "Stin puts his hand onto his pocket and takes a hard candy, offering. It has the shape of a brain."

# game/script.rpy:514
translate english start_ff01f460:

    # st "Aqui. Por ter sido uma boa menina e me ajudado hoje."
    st "Here. For being a good student and helped me out today."

# game/script.rpy:516
translate english start_627899b2:

    # "Eu pego o doce, examinando ele com cuidado. A embalagem é fofa."
    "I take the candy from his hands, examining it. It's actually cute~"

# game/script.rpy:518
translate english start_290a245c:

    # mc "Você... Sempre carrega isso no bolso?"
    mc "Do you... Always have one on your pockets?"

# game/script.rpy:520
translate english start_62d7e521:

    # st "Eu gosto. Me ajuda a concentrar no trabalho, já que é feito de açúcar."
    st "I like them. Helps me get on with my job, since its made from sugar. Makes the brain run better."

# game/script.rpy:522
translate english start_1d7b8fb5:

    # mc "Obrigada. Então, até amanhã professor, não fique até muito tarde."
    mc "Thanks! Then, until tomorrow teacher. Don't be up till late~"

# game/script.rpy:524
translate english start_2fcc26e1:

    # st "O mesmo para você. Sei que não precisa revisar tanto, mas não exagere. Durma cedo."
    st "Same for you. I know you don't need to study that much, but dont overdo it. Sleep early."

# game/script.rpy:526
translate english start_f2141119:

    # mc "Okay, até!"
    mc "Okay! See ya!"

# game/script.rpy:528
translate english start_22f96efe:

    # "Eu me despeço dele e guardo o docinho no bolso da minha roupa. Talvez me ajude durante as revisões."
    "I say goodbye to him and pocket my candy onto my pocket. Maybe it'll help me later."

# game/script.rpy:530
translate english start_1e55d1db:

    # "Eu me dirijo para a minha sala, onde Skelly e Merrii me esperavam."
    "I walk until I meet the corridor where Skelly and Merrii was waiting for me, and we depart together."

# game/script.rpy:534
translate english start_89309cab:

    # mc "Então eu já vou indo, preciso revisar."
    mc "So I'll be going. I need to study for tomorrow."

# game/script.rpy:536
translate english start_26909154:

    # st "Não exagere hoje, sim? Coma bem na janta e durma bastante, você não precisa revisar tanto como alguns da sua turma, então descanse."
    st "Don't overdo, okay? Eat your dinner and sleep lots. You don't need to study as much as your colleagues, so rest well."

# game/script.rpy:538
translate english start_da15e0b3:

    # mc "Certo, até amanhã."
    mc "Yea. See you tomorrow."

# game/script.rpy:540
translate english start_0b91f0be:

    # st "Até!"
    st "See ya!"

# game/script.rpy:542
translate english start_cc1cd3d3:

    # "Ele se vira e fecha a porta, voltando aos seus afazeres. Eu me dirijo para a minha sala, onde Skelly e Merrii me esperavam."
    "He turns around and closes the door, getting back into whatever he was on. I walk until I meet the corridor which Skelly and Merrii was waiting for me, and we depart together."

# game/script.rpy:548
translate english start_893ff5f5:

    # mc "Certo. Estou pronta, vamos?"
    mc "Right, I'm ready. Shall we go?"

# game/script.rpy:554
translate english sala_538e9e59:

    # "-Casa da [povname], no quarto-"
    "[povname]'s house. Her room-"

# game/script.rpy:557
translate english sala_3b00c92c:

    # sk "Ah~! Não aguento mais essa matéria."
    sk "Ahh~! I don't want to study anymore."

# game/script.rpy:559
translate english sala_980de9de:

    # me "Você não devia estudar a mesma coisa por tanto tempo, só vai se aborrecer com a matéria."
    me "You shouldn't study the same subject for so long. You'll just get angry and not study at all."

# game/script.rpy:561
translate english sala_42ff748c:

    # mc "Bom, eu já vi o que precisava. Se quiserem, podemos fazer outra coisa."
    mc "Well, I already done what I needed. If you want to we can do something else for the time being."

# game/script.rpy:563
translate english sala_6d5c3d0e:

    # sk "Ok!"
    sk "Okay!"

# game/script.rpy:565
translate english sala_1120a4a7:

    # "Skelly se levanta do chão em que havia deitado, sentando perto de mim."
    "Skelly gets up from the floor she was laying, sitting besides me."

# game/script.rpy:567
translate english sala_9da7117b:

    # sk "Seu par pro baile, [povname]. Já decidiu?"
    sk "Your partner for our graduation prom, [povname]. Have you decided it yet?"

# game/script.rpy:569
translate english sala_c07f76ad:

    # me "[povname] ainda não tem um par?"
    me "[povname] doesn't have a partner?"

# game/script.rpy:571
translate english sala_e1efa61a:

    # mc "Ainda não... Você já tem, Merrii?"
    mc "Not yet... Do you have one, Merrii?"

# game/script.rpy:573
translate english sala_d5efb881:

    # me "Claro. Fui pedida assim que o diretor anunciou a festa. Por algum motivo..."
    me "Of course! I was asked out as soon as the director made a announcement. For some reason..."

# game/script.rpy:575
translate english sala_7e1db39e:

    # sk "Tsc. Bom. Quais são as opções?"
    sk "Tsk. Well, what are your options?"

# game/script.rpy:577
translate english sala_a13d1f20:

    # mc "Hmm... Deixa eu ver..."
    mc "Hmm... Let me see..."

# game/script.rpy:581
translate english sala_45455055:

    # mc "Ah! Tem o Kurusu, mas ele falou que ia decidir mais tarde..."
    mc "Ah! I've asked Kurusu out, but he said he would look into it."

# game/script.rpy:583
translate english sala_47f60bae:

    # sk "Kukki? Nem pensar. Ele vai passar o baile inteiro reclamando de estar ali."
    sk "Kukki? Not a chance. He will complain the whole party, wanting to go home sooner."

# game/script.rpy:585
translate english sala_47580bda:

    # me "Mais alguém em mente?"
    me "Do you have someone else in mind?"

# game/script.rpy:587
translate english sala_ec004ed3:

    # mc "Não... Sabe que não converso com muita gente."
    mc "No... You know I don't talk to other people beside my friends."

# game/script.rpy:589
translate english sala_7c24716d:

    # sk "Pois devia. Vai que você acha alguém legal, né?"
    sk "Well you should, you could meet someone cool don't you think?"

# game/script.rpy:591
translate english sala_1f39b514:

    # me "Bom... Tem aquele menino que fica te olhando na biblioteca."
    me "Well... There's that boy that keeps looking at you when were at the library."

# game/script.rpy:595
translate english sala_122a3c65:

    # mc "Que menino??"
    mc "Which boy???"

# game/script.rpy:597
translate english sala_758a55d3:

    # sk "Vai me dizer que você nunca viu ele, [povname]?"
    sk "You didn't notice him, [povname]?"

# game/script.rpy:599
translate english sala_7d1216be:

    # mc "Não? Como ele é?"
    mc "Nope?? How is he like?"

# game/script.rpy:601
translate english sala_c19e0f88:

    # me "É um rapaz com faixas, de olho puxado..."
    me "Its a boy with some cloth wrapped around his body, with cute eyes..."

# game/script.rpy:605
translate english sala_966d844f:

    # mc "Ah, eu esbarrei com ele no corredor hoje!"
    mc "Ah! We bumped onto eachother at the lockers today!"

# game/script.rpy:607
translate english sala_57a6a848:

    # sk "Você só dá bola fora hein [povname]..."
    sk "he just messed up [povname]..."

# game/script.rpy:609
translate english sala_ce47963a:

    # mc "Fica quieta~ Mas porquê ele fica me encarando?"
    mc "Stop it~ But why does he do that?"

# game/script.rpy:611
translate english sala_9ef79d8e:

    # sk "Olha, eu sinceramente não sei. Parece que ele quer falar contigo ou alguma coisa assim."
    sk "I don't really know. Seems like he wants to speak with you or something like that."

# game/script.rpy:613
translate english sala_d295edfd:

    # me "É um pouquinho estranho, mas ele deve ser tímido..."
    me "Its actually cute, but he might be a little shy..."

# game/script.rpy:615
translate english sala_6e741c98:

    # sk "Cê devia começar por ele, já que ele tem interesse~"
    sk "You should start with him, since he already has an interest in you~"

# game/script.rpy:617
translate english sala_d04cfbea:

    # mc "Hmm... Então acho que amanhã eu tento. Ah, mas e se ele já tiver um par?"
    mc "Hmm... Then I'll try to know him tomorrow. But what if he already has someone to go with?"

# game/script.rpy:619
translate english sala_8185ea7b:

    # sk "Bom... Nesse caso, acho que é melhor ter mais opções."
    sk "Well... In this case you might want to have more options."

# game/script.rpy:621
translate english sala_4de5e52e:

    # me "[povname], tem algum rapaz na sua sala que te interessa?"
    me "[povname] does you classroom has someone that sparkles some interest on you?"

# game/script.rpy:623
translate english sala_16e9c093:

    # mc "Eu não tenho muita certeza... Não conheço eles o suficiente."
    mc "I'm not sure... I dont know them enough."

# game/script.rpy:625
translate english sala_87dbe417:

    # sk "Você não precisa conhecer eles. Só precisa ser alguém que você se sinta confortável indo."
    sk "You dont need to know them. Just needs so be someone you'd feel comfortable going with."

# game/script.rpy:627
translate english sala_983640ae:

    # mc "Humm... Bom, tem o Gomi. Ele é um Oni, senta no fundo da sala."
    mc "Hmmm... Well there's Gomi. He's a Oni from my class, hes usually messing around tho."

# game/script.rpy:631
translate english sala_64ef88c6:

    # mc "Ele é legal, sempre me pede material emprestado antes da aula começar."
    mc "He's cool, seems like a nice guy but always ends up asking me to lend him some lead."

# game/script.rpy:633
translate english sala_5519d115:

    # me "Ele é esquecido? Que fofinho."
    me "He's clumsy? What a cutie~"

# game/script.rpy:635
translate english sala_8a86a8aa:

    # mc "Uhum. Os professores sempre chamam a atenção por ele ficar bagunçando e pregando peças, mas fora isso, ele parece ser legal."
    mc "Yea. Our teaches always asks for his attention as he's always messing around and pranking his friends. But besides that, he's cool."

# game/script.rpy:637
translate english sala_f2ddcabe:

    # sk "Ah, então você gosta dele por ser engraçadinho?"
    sk "So you like him because he's a clown?"

# game/script.rpy:639
translate english sala_48ec517f:

    # mc "Não diria gostar... Ele só parece ser amigável."
    mc "I don't think so... He's just friendly y'know?"

# game/script.rpy:641
translate english sala_68d17031:

    # me "Ok... Mais algum que te chame a atenção?"
    me "Okay... Someone else in mind?"

# game/script.rpy:643
translate english sala_7c1cfb6b:

    # sk "Ah! Ah! Que tal o Hanael!? Ele é super gente fina, e é amigo do meu nenê. Quem sabe ele não arranja alguma coisa pra você?"
    sk "Oh! Oh! What about Hanael!? He's super laid back and he's friend with my babey. Who knows he could be into you?"

# game/script.rpy:647
translate english sala_12511d63:

    # me "Eu acho ele meio bobinho, mas se a [povname] gostar..."
    me "I think he's kinda silly, but if [povname] likes him..."

# game/script.rpy:649
translate english sala_de6e5d89:

    # mc "É, quem sabe. Se ele for divertido..."
    mc "Yea, who knows. If he's funny enough..."

# game/script.rpy:651
translate english sala_a0312251:

    # sk "Bom... Mais alguém?"
    sk "Okay... Who else?"

# game/script.rpy:653
translate english sala_ec17c75b:

    # me "Eu conheço um rapaz, mas... Ele é meio tímido."
    me "I know a guy but he's... Too too shy."

# game/script.rpy:655
translate english sala_f0bf0da1:

    # mc "E quem seria ele?"
    mc "And who is he?"

# game/script.rpy:657
translate english sala_aca51ab2:

    # me "É um fantasma, Damien. Mas fora da classe é meio difícil encontrar ele..."
    me "He's a ghoul, Damien. But besides from our classroom, its kinda difficult to find him. He doesnt want to talk to nobody."

# game/script.rpy:661
translate english sala_27d5a350:

    # me "Porque ele gosta de ficar invisível pra fugir das pessoas."
    me "Because he prefers to be invisible, quiet."

# game/script.rpy:663
translate english sala_b78c90dd:

    # mc "Nossa... Mas então porque sugeriu ele? Provavelmente ele nem vai querer ir."
    mc "Wow... But why did you mention him? He probably doesn't want to go either."

# game/script.rpy:665
translate english sala_ed8e6a42:

    # me "Porque achei que vocês dois ficariam fofinhos juntos~ "
    me "Because I thought you could look cute together~"

# game/script.rpy:667
translate english sala_1c41e935:

    # sk "Merrii, para. Foco."
    sk "Merrii stop. Focus."

# game/script.rpy:669
translate english sala_8a43a26d:

    # me "Mas eu falei sério! Acho que a [povname] devia tentar se ela achar ele interessante."
    me "But I was serious! I think [povname] should try if she finds him interesting enough."

# game/script.rpy:671
translate english sala_1dd3faac:

    # mc "E-eu vou ver... De repente ele é um cara bacana né?"
    mc "W-We'll see... He could be a nice guy, yea."

# game/script.rpy:673
translate english sala_546d8cb2:

    # "Essa conversa toda está me deixando com vergonha. E se eles não gostarem de mim?"
    "All this talking is making me kinda nervous... What if no one wants to go with me?"

# game/script.rpy:675
translate english sala_2081dab2:

    # "E se eu acabar tendo que ir sozinha para o baile??"
    "What if I might end up going to prom alone??"

# game/script.rpy:677
translate english sala_1e970fb2:

    # me "Sim, sim! Também pode ajudar ele a se abrir mais com as pessoas."
    me "Yea yea! That could help him open more to people."

# game/script.rpy:679
translate english sala_f7703ad9:

    # mc "Entendi. Bem, acho que esse é um número razoável de opções."
    mc "Got it. Well, its a reasonable number of options. I'll think about it later."

# game/script.rpy:681
translate english sala_02596f4c:

    # sk "Ah bom, então você vai começar amanhã?"
    sk "Oh okay, then you'll start tomorrow?"

# game/script.rpy:683
translate english sala_b2ace761:

    # mc "Vou sim. A festa tá chegando preciso de alguém logo."
    mc "Yea i will. Party's coming right up and I need someone soon enough."

# game/script.rpy:685
translate english sala_b56dedf1:

    # me "Então é melhor nós irmos dormir! Temos provas amanhã cedinho."
    me "Then we better get to sleep! Our finals are tomorrow early."

# game/script.rpy:687
translate english sala_948a56ac:

    # sk "Certo!"
    sk "Yeea!"

# game/script.rpy:689
translate english sala_46c48878:

    # "Skelly se levantou e imitou a pose de um super-herói, com as mãos na cintura e o olhar fixo no teto."
    "Skelly gets up, puts her hands on her waist and looks at the ceilling, striking a super-hero pose."

# game/script.rpy:691
translate english sala_426ac29c:

    # sk "Pra cama então, senhoritas!"
    sk "Off to the bed we go, ladies!"

# game/script.rpy:693
translate english sala_56389841:

    # mc "Boa noite!"
    mc "Good night!"

# game/script.rpy:697
translate english sala_65d4c874:

    # "- Segunda de manhã, casa da [povname] -"
    "Tuesday morning, [povname]'s house-"

# game/script.rpy:699
translate english sala_c8efb472:

    # "Eu e a Skelly estamos sentadas na mesa, esperando o café ficar pronto."
    "Skelly and I are waiting for our breakfast on the table."

# game/script.rpy:701
translate english sala_45856559:

    # "Merrii gosta de cozinhar pra gente. Além de ser uma boa comida, é até melhor para nós que somos preguiçosas~"
    "Merrii likes to cook for us. Besides getting a real nice meal, its better for us lazy gals~"

# game/script.rpy:703
translate english sala_f86950d6:

    # sk "Bom, você precisa começar hoje, [povname]. Ainda tem que comprar a roupa que combine com o seu par."
    sk "Well, you need to start today, [povname]. You still have to buy clothes that matches your partner's taste."

# game/script.rpy:705
translate english sala_c2751626:

    # mc "Roupa que combine...?"
    mc "I need to buy clothes...?"

# game/script.rpy:707
translate english sala_417f0074:

    # sk "Sim, você quer agradá-lo, não quer?"
    sk "Yea. You want to make them gag, don't you?"

# game/script.rpy:709
translate english sala_91bfaee6:

    # mc "Errr... Acho que sim...?"
    mc "Err... I think so...?"

# game/script.rpy:711
translate english sala_4dad4e8a:

    # "Skelly se entusiasma demais quando o assunto se trata de festas."
    "Skelly often gets too excited when it's about partys. That's one of her charms~"

# game/script.rpy:713
translate english sala_fdc4232b:

    # sk "Você tem que estar impecável! Merrii e eu vamos te ajudar com isso, ok? Você só precisa se preocupar em arranjar um par."
    sk "You need to be impecable! Merrii and I will help you, okay? You just need to get a date!"

# game/script.rpy:715
translate english sala_dde1eb56:

    # mc "Ceerto!"
    mc "Allriight~"

# game/script.rpy:717
translate english sala_52d0b01c:

    # "Merrii se aproxima trazendo os pratos com cuidado, um em cada mão para cada uma de nós."
    "Merrii gets close with our plates, carefully, giving it for us."

# game/script.rpy:719
translate english sala_aff62c7a:

    # me "Aqui está. Comam logo meninas, não queremos chegar atrasadas!"
    me "There is it. Eat well and we'll be off! Don't want to be late today!"

# game/script.rpy:721
translate english sala_616fb86d:

    # sk "Não precisa nem falar."
    sk "You don't even need to say it."

# game/script.rpy:723
translate english sala_7e7c68ce:

    # "Nós comemos animadas, preocupadas com o horário."
    "We eat in a hurry, Starting to worry about being late."

# game/script.rpy:725
translate english sala_e8287451:

    # "- Após as provas -"
    "~After exams~"

# game/script.rpy:727
translate english sala_8481d0d4:

    # st "Certo! Acabou o tempo. Por favor, guardem seus materiais. A representante vai coletar as provas, o resto está dispensado pelo resto do dia."
    st "Right, time's up! Please get your stuff done. Class' representant will collect your exams and you're free to go."

# game/script.rpy:729
translate english sala_eaf811d5:

    # mc "Ok, é a minha chance. Hora de escolher com quem passar um tempo!"
    mc "Okay, now's my chance. Time to choose who I want to spend some time!"

# game/script.rpy:740
translate english sala_150a6999:

    # "Kurusu é meu amigo desde que eu me lembro, vai ser mais divertido ir com alguém que eu conheça."
    "Kurusu is my friend since I know myself. It should be fun to be with someone that I know well."

translate english strings:

    # game/script.rpy:445
    old "Na verdade não."
    new "Not really."

    # game/script.rpy:445
    old "Ahm... Ele é um pouquinho estranho."
    new "Ahm ... He's a little weird."

    # game/script.rpy:731
    old "Quem eu devo escolher para ser meu parceiro?"
    new "Who should I choose to be my partner?"

    # game/script.rpy:731
    old "O menino enfaixado"
    new "The bandaged boy"

    # game/script.rpy:731
    old "Kurusu"
    new "Kurusu"

