﻿# TODO: Translation updated at 2020-07-14 20:51

# game/Kurusu route.rpy:6
translate english kurusu_7f65ea29:

    # mc "Kurusu deve estar na sala!"
    mc "Kurusu must be in the room!"

# game/Kurusu route.rpy:8
translate english kurusu_68a4541e:

    # "Segurando a minha mochila pelos ombros, corri até a sala do Kurusu. Quando cheguei, ele estava terminando de arrumar seu material."
    "Holding my backpack by the shoulders, I ran to Kurusu's office. When I arrived, he was finishing packing his stuff."

# game/Kurusu route.rpy:10
translate english kurusu_33b56670:

    # mc "Kukki~!"
    mc "Kukki!"

# game/Kurusu route.rpy:12
translate english kurusu_518d1d5f:

    # "Eu o chamei, acenando."
    "I called out to him, waving."

# game/Kurusu route.rpy:14
translate english kurusu_f9e813a7:

    # k "[povname]."
    k "[povname]."

# game/Kurusu route.rpy:16
translate english kurusu_d77a72b9:

    # "Ele se virou, com um sorrisinho."
    "He turned, with a little smile."

# game/Kurusu route.rpy:18
translate english kurusu_483eeb1c:

    # k "Que bom te ver. Ainda não foi pra casa?"
    k "Good to see you. Haven't you gone home yet?"

# game/Kurusu route.rpy:20
translate english kurusu_6f61d9d6:

    # mc "Não... Na verdade estava esperando por você."
    mc "No ... I was actually waiting for you."

# game/Kurusu route.rpy:22
translate english kurusu_e055fc22:

    # "Eu dou um sorrisinho me apoiando na porta, esperando ele terminar de se aprontar."
    "I smile a little while leaning against the door, waiting for him to finish getting ready."

# game/Kurusu route.rpy:24
translate english kurusu_a0112c45:

    # k "Por mim? Mas que honra. *ri*"
    k "For me? What an honor."

# game/Kurusu route.rpy:26
translate english kurusu_f7ff1f3a:

    # mc "Para. *ri* Eu queria passar um tempo com você~"
    mc "I wanted to spend time with you ~"

# game/Kurusu route.rpy:28
translate english kurusu_3099ed46:

    # mc "Isso é, se não for incômodo..."
    mc "That's If you want to?"

# game/Kurusu route.rpy:30
translate english kurusu_292e8cca:

    # k "Não, não é incômodo."
    k "Yes. I want to."

# game/Kurusu route.rpy:32
translate english kurusu_96416e89:

    # "Kurusu passou o braço pelo meu pescoço, ainda sorrindo."
    "Kurusu put his arm around my neck, still smiling."

# game/Kurusu route.rpy:34
translate english kurusu_f27cb8c1:

    # k "Então que tal irmos para o pátio? Com certeza não vão nos incomodar lá."
    k "So how about we go to the patio? Noone will bother us there."

# game/Kurusu route.rpy:36
translate english kurusu_f0324398:

    # mc "Ooh~ O seu cantinho secreto? Vamos~!"
    mc "Ooh ~ Your secret corner? Come on ~!"

# game/Kurusu route.rpy:38
translate english kurusu_00a71ec0:

    # "Kurusu tira o braço do meu ombro, deixando a área com uma sensação de vazio."
    "Kurusu takes his arm from my shoulder, leaving the area with a feeling of emptiness."

# game/Kurusu route.rpy:40
translate english kurusu_f2fc0084:

    # "Nós nos dirigimos pelo pátio. O intervalo sempre junta os alunos em todas as áreas do colégio."
    "We headed across the courtyard. The break always brings students together in all areas of the college."

# game/Kurusu route.rpy:42
translate english kurusu_4e3ad19b:

    # "Passando pelo ginásio, conseguimos ouvir o grito de vários alunos"
    "Passing through the gym, we could hear the cry of several students"

# game/Kurusu route.rpy:44
translate english kurusu_5b039826:

    # "O time de basquete deve estar jogando."
    "The basketball team must be playing."

# game/Kurusu route.rpy:46
translate english kurusu_11372a93:

    # k "Heh. Deve ser divertido..."
    k "Heh. It must be fun ..."

# game/Kurusu route.rpy:48
translate english kurusu_2dea122d:

    # mc "O que, Kukki?"
    mc "What, Kukki?"

# game/Kurusu route.rpy:50
translate english kurusu_3022fb80:

    # k "Jogar... Praticar esportes com o pessoal no ginásio."
    k "Playing sports, with the staff in the gym."

# game/Kurusu route.rpy:52
translate english kurusu_125c2134:

    # "Kurusu suspira e, ao ver a minha expressão de preocupação, solta um risinho"
    "Kurusu sighs and, when he sees my expression of concern, lets out a little laugh"

# game/Kurusu route.rpy:54
translate english kurusu_4a2354c1:

    # k "Não é nada, [povname]. Não precisa se preocupar tanto~"
    k "It's nothing, [povname]. No need to worry so much ~"

# game/Kurusu route.rpy:56
translate english kurusu_0a541e00:

    # mc "Eu não disse nada~"
    mc "I didn't say anything ~"

# game/Kurusu route.rpy:58
translate english kurusu_652db694:

    # "Ele tem o costume de se preocupar demais comigo e não deixar que eu faça o mesmo."
    "He has a habit of worrying too much about me and not letting me do the same."

# game/Kurusu route.rpy:60
translate english kurusu_5ac93450:

    # "É um pouco frustrante, mas não quero abusar"
    "It's a little frustrating, but I don't want to say anything."

# game/Kurusu route.rpy:62
translate english kurusu_e2b8efcf:

    # "Ele já me deixa ter contato com ele, e se ele estiver bem com isso, já é o suficiente pra mim."
    "He already lets me have contact with him, and if he's okay with it, that's enough for me."

# game/Kurusu route.rpy:64
translate english kurusu_a5e6eb94:

    # "Chegando numa clareira, Kurusu sinaliza para um caminho semi-morto no meio dos arbustos."
    "Arriving in a clearing, Kurusu signals to a half-dead path through the bushes."

# game/Kurusu route.rpy:66
translate english kurusu_0a10b940:

    # "Ele me disse que as vezes que vem pra cá, acaba sem querer machucando um ou dois ramos..."
    "He told me that the times he comes here, he ends up accidentally hurting one or two branches ..."

# game/Kurusu route.rpy:68
translate english kurusu_52d4a297:

    # "Enquanto ele entra no arbusto, eu faço um carinho gentil na parte seca. Ele parece notar, mas não dá muita bola."
    "As he enters the bush, I gently caress the dry part. He seems to notice, but he doesn't care much."

# game/Kurusu route.rpy:70
translate english kurusu_e073ee22:

    # "Passando pelo arbusto, ele dá lugar para um caminho fechado com algumas flores mistas desabrochando."
    "Passing through the bush, it gives way to a closed path with some mixed flowers blooming."

# game/Kurusu route.rpy:72
translate english kurusu_0bccb5e0:

    # mc "Eu sempre fico impressionada com esse lugar... É muito lindo."
    mc "I am always impressed by this place ... It is very beautiful"

# game/Kurusu route.rpy:74
translate english kurusu_8ce377e8:

    # "Eu sorrio, apreciando a beleza do lugar. É quieto demais para um colégio."
    "I smile, enjoying the beauty of the place. It's too quiet for a college."

# game/Kurusu route.rpy:76
translate english kurusu_7335a99b:

    # k "Minhas melhores sonecas foram aqui, é um lugar muito bom pra dormir..."
    k "My best naps were here, it's a very good place to sleep ..."

# game/Kurusu route.rpy:78
translate english kurusu_0d75d13d:

    # k "Embora as flores tenham um cheiro um pouco forte pro meu gosto."
    k "Although the flowers smell a little strong for my taste."

# game/Kurusu route.rpy:80
translate english kurusu_49d3045d:

    # "Eu me abaixo pra sentir o aroma de uma das flores. Ela é pequena e, embora grande parte delas sejam brancas, haviam algumas roxas, rosas e até algumas laranjas por perto. É doce, tipo mel."
    "I bend down to smell one of the flowers. They is small and, although most of them are white, there were some purples, roses and even some oranges nearby. It's sweet, like honey."

# game/Kurusu route.rpy:82
translate english kurusu_2cfa6801:

    # k "Álisso."
    k "Alisso's, They look good?"

# game/Kurusu route.rpy:84
translate english kurusu_6d6be801:

    # "Kurusu sorri, sentando no chão e observando a cena."
    "Kurusu smiles, sitting on the floor and watching the scene"

# game/Kurusu route.rpy:86
translate english kurusu_30f8830f:

    # "Ele tem essa mania de prestar atenção a detalhes. Sempre observando, sempre quieto. Sempre admirando."
    "He has this habit of paying attention to details. Always watching, always quiet. Always admiring."

# game/Kurusu route.rpy:88
translate english kurusu_680c4f9c:

    # "Rubor cobre as minhas bochechas, vendo como ele me encara dessa forma. Tão bobo~"
    "Red covers my cheeks, seeing how he looks at me that way. So silly ~"

# game/Kurusu route.rpy:90
translate english kurusu_d253fd66:

    # mc "São lindas..."
    mc "They're beautiful"

# game/Kurusu route.rpy:92
translate english kurusu_9f9feb56:

    # k "São sim, mas o cheiro..."
    k "Yes they are, but the smell ..."

# game/Kurusu route.rpy:94
translate english kurusu_1ff18aa5:

    # mc "Eu não acho... É bem agradável na verdade~"
    mc "I don't think so... It's quite pleasant actually ~"

# game/Kurusu route.rpy:96
translate english kurusu_8146ceff:

    # k "...Pena eu não poder tocar muito nelas."
    k "... Too bad I can't touch them"

# game/Kurusu route.rpy:98
translate english kurusu_ad8cd9c2:

    # "Eu me aproximo dele, olhando para suas mãos."
    "I approach him, looking at his hands."

# game/Kurusu route.rpy:100
translate english kurusu_e1c0fab0:

    # mc "Você se refere ao..."
    mc "You mean the ..."

# game/Kurusu route.rpy:102
translate english kurusu_0780eff8:

    # k "Ao sangue da minha mãe, sim."
    k "My mother's blood, yes."

# game/Kurusu route.rpy:104
translate english kurusu_40d0ae05:

    # "Oh. Kurusu é filho da Senhora Morte. Ela mata tudo o que toca, e ele como herdeiro..."
    "Oh. Kurusu is Lady Death's son. She kills everything she touches, and he as heir ..."

# game/Kurusu route.rpy:106
translate english kurusu_82f1c7eb:

    # mc "Você andava treinando controlar isso, não estava? Como tá indo? Alguma melhora?"
    mc "You were training to control it, weren't you? How's it going? Any improvement?"

# game/Kurusu route.rpy:108
translate english kurusu_c5865d3d:

    # k "Eu... Consigo tocar algumas coisas antes de apodrecer elas... Mas são breves segundos."
    k "I ... I can touch some things before they rot ... But only for a few seconds."

# game/Kurusu route.rpy:110
translate english kurusu_fac0d2e3:

    # "Kurusu suspira, levantando e pegando uma Álisso do chão."
    "Kurusu sighs, lifting and picking up an Álisso from the floor."

# game/Kurusu route.rpy:114
translate english kurusu_72e2fc73:

    # mc "Kurusu..."
    mc "Kurusu..."

# game/Kurusu route.rpy:116
translate english kurusu_7c6266de:

    # "Kurusu suspira novamente e senta do meu lado, fazendo carinho em uma das minhas mechas de cabelo."
    "Kurusu sighs again and sits next to me, stroking one of my locks of hair."

# game/Kurusu route.rpy:118
translate english kurusu_a9b456ae:

    # k "Tá tudo bem [povname]. Um dia eu terei controle dessa situação, e você será a primeira a sentir o meu abraço apertado."
    k "It's okay [povname]. One day I will be in control of this situation, and you will be the first to feel my tight embrace."

# game/Kurusu route.rpy:120
translate english kurusu_2e8798ad:

    # "Eu sorrio, abraçando ele por alguns instantes."
    "I smile, hugging him for a few moments."

# game/Kurusu route.rpy:122
translate english kurusu_3d8bc728:

    # mc "Hihi~ Eu vou estar mais que honrada em aceitar Kukki."
    mc "I will be more than honored to accept Kukki."

# game/Kurusu route.rpy:124
translate english kurusu_371f7cc0:

    # k "Huhu~"
    k "Thank you."

# game/Kurusu route.rpy:126
translate english kurusu_68d0fee5:

    # "Eu desfaço o abraço, me ajeitando na grama. Ele parece um pouco nervoso com o que eu fiz, mas isso é normal..."
    "I undo the hug, settling on the grass. He seems a little nervous about what I did, but that's normal ..."

# game/Kurusu route.rpy:128
translate english kurusu_9813508b:

    # k "Então, como vai a busca pelo par ideal?"
    k "So, how is the search for the ideal match going?"

# game/Kurusu route.rpy:130
translate english kurusu_d3b26143:

    # "Eu consigo ouvir ele apertando o tecido da roupa, evitando olhar nos meus olhos."
    "I can hear him squeezing the fabric of his clothes, avoiding looking me in the eye."

# game/Kurusu route.rpy:132
translate english kurusu_3e57d3ab:

    # mc "Você não vai mesmo pensar em ir comigo, né?..."
    mc "You won't even think about going with me, will you? ..."

# game/Kurusu route.rpy:134
translate english kurusu_cd684b87:

    # k "Digamos que eu prefiro ser sua última opção, não a primeira."
    k "I prefer to be your last option, not the first."

# game/Kurusu route.rpy:136
translate english kurusu_49f0fc66:

    # "Kurusu sorri, ainda evitando de me olhar diretamente."
    "Kurusu smiles, still avoiding looking at me directly."

# game/Kurusu route.rpy:138
translate english kurusu_c1f4b709:

    # "Ele deve estar nervoso com o assunto, por ter mencionado sem que eu tocasse no assunto..."
    "He must be nervous about it, for having mentioned it without me mentioning it first..."

# game/Kurusu route.rpy:140
translate english kurusu_79610dc6:

    # "Eu lembro de ter combinado com as meninas de começar o plano hoje, ele deve ter ouvido isso"
    "I remember agreeing with the girls to start the plan today, he must have heard that"

# game/Kurusu route.rpy:142
translate english kurusu_ba2869b4:

    # "Mas acho que ele não espera que eu tenha escolhido ele"
    "But I think he doesn't expect me to choose him"

# game/Kurusu route.rpy:144
translate english kurusu_e8ce66dc:

    # "Ele é o meu melhor amigo, não teria graça ir sem ele."
    "He's my best friend, it wouldn't be fun to go without him."

# game/Kurusu route.rpy:146
translate english kurusu_6bcbc197:

    # mc "Eu gosto de você, Kukki. Gostaria muito que fosse... Pensa um pouquinho nisso vai, por mim..."
    mc "I like you, Kukki. I would really like it to be you... Think about it for a while, for me ..."

# game/Kurusu route.rpy:148
translate english kurusu_12843bfc:

    # "A minha pressão psicológica deve funcionar nele, já que eu consigo ver as orelhas dele ganhando cor."
    "My psychological pressure should work on him, as I can see his ears turning color."

# game/Kurusu route.rpy:150
translate english kurusu_789735c0:

    # mc "Awn. Tá vermelhinho é?~"
    mc "Awn. Are you red? ~"

# game/Kurusu route.rpy:152
translate english kurusu_0a0142fd:

    # k "Cala a boca. Não foi nada..."
    k "Shut up. It was nothing..."

# game/Kurusu route.rpy:154
translate english kurusu_51d8f220:

    # "Ele vira o rosto tentando esconder a vermelhidez, mas a sua pele é tão pálida que a cor preenche o lugar inexistente da melanina."
    "He turns his face trying to hide the redness, but his skin is so pale that the color fills in the non-existent place of melanin."

# game/Kurusu route.rpy:156
translate english kurusu_f1f0f684:

    # k "Eu... Eu vou pensar, okay? Mas não prometo nada..."
    k "I ... I'm going to think, okay? But I promise nothing ..."

# game/Kurusu route.rpy:158
translate english kurusu_4a927248:

    # mc "Já é um começo. Obrigada."
    mc "It's a start! Thanks."

# game/Kurusu route.rpy:160
translate english kurusu_5be4f6f5:

    # k "De nada."
    k "No problem."

# game/Kurusu route.rpy:162
translate english kurusu_5487b7e6:

    # "Eu suspiro, me jogando de costas na grama e olhando pro céu."
    "I throw myself on the grass and look up at the sky."

# game/Kurusu route.rpy:164
translate english kurusu_55ee30cc:

    # "A calmaria e o céu claro e livre de nuvens me fazem perceber o sono que eu estou, e um bocejo acaba saindo."
    "The calm and the clear, cloud-free sky make me realize how tired I am, and a yawn confirms it."

# game/Kurusu route.rpy:166
translate english kurusu_5dc5deca:

    # mc "H-hnng~ Acho que vou tirar um cochilo. Me acompanha?"
    mc "H-hnng ~ I think I'm going to take a nap. Joining me?"

# game/Kurusu route.rpy:168
translate english kurusu_427430af:

    # "Kurusu deita com mais gentileza, passando os braços pela parte de trás da cabeça."
    "Kurusu lies down more gently, wrapping his arms around the back of his head."

# game/Kurusu route.rpy:170
translate english kurusu_40c0fabd:

    # k "Claro~"
    k "Sure~"

# game/Kurusu route.rpy:172
translate english kurusu_5dfb2321:

    # "Aos poucos dá pra se perceber os pássaros assobiando uns pros outros, borboletas passando de uma árvore pra outra."
    "Gradually you can see the birds whistling at each other, butterflies passing from one tree to another."

# game/Kurusu route.rpy:174
translate english kurusu_dd5c646f:

    # "A serenidade que esse lugar transmite parece vinda de outra dimensão... Fico feliz do Kurusu ter um lugar só pra ele."
    "The serenity that this place conveys seems to come from another dimension ... I am happy that Kurusu has a place just for him."

# game/Kurusu route.rpy:176
translate english kurusu_ef645715:

    # mc "Hihi... Tão tranquilo~"
    mc "So peaceful out here ~"

# game/Kurusu route.rpy:178
translate english kurusu_dcfcbb26:

    # "Kurusu cobre os olhos com o braço, escurecendo a sua visão."
    "Kurusu covers his eyes with his arm, darkening his vision."

# game/Kurusu route.rpy:180
translate english kurusu_c14e047a:

    # "Me virando na direção dele eu percebo o quão quieto Kurusu é. Ele sempre cuida de mim, tenta se abrir pros meus amigos e é gentil com as coisas."
    "Turning towards him, I realize how quiet Kurusu is. He always takes care of me, tries to open up to my friends and is kind to everyone."

# game/Kurusu route.rpy:182
translate english kurusu_fe33e721:

    # "Eu queria passar mais tempo com ele... A correria da vida adulta logo vai começar."
    "I wanted to spend more time with him ... The rush of adult life will soon begin"

# game/Kurusu route.rpy:184
translate english kurusu_6ea124a6:

    # "E o meu medo de não ter mais ele do meu lado..."
    "And my fear of not having him by my side .."

# game/Kurusu route.rpy:186
translate english kurusu_8146ffa8:

    # "Eu não quero nem pensar nisso."
    "I don't even want to think about it"

# game/Kurusu route.rpy:188
translate english kurusu_683eb731:

    # k "Eu consigo ouvir os seus pensamentos, [povname]. Descansa um pouquinho."
    k "I can hear your thoughts, [povname]. Get some rest."

# game/Kurusu route.rpy:190
translate english kurusu_73b52865:

    # "Kurusu diz, imóvel. Obediente, eu coloco as mãos na minha barriga, aos poucos esvaziando os meus pensamentos..."
    "Kurusu says, motionless. Obediently, I place my hands on my belly, gradually emptying my thoughts ..."

# game/Kurusu route.rpy:192
translate english kurusu_ce55d1ed:

    # "...Até cair no sono."
    "..Until I fall asleep."

# game/Kurusu route.rpy:194
translate english kurusu_59c6a09d:

    # "Kurusu ao ouvir a minha respiração pesada, tira o braço do rosto."
    "Kurusu, hearing my heavy breathing, removes his arm from his face."

# game/Kurusu route.rpy:196
translate english kurusu_2e20edcd:

    # "Ele se apoia em um braço, me admirando."
    "He leans on one arm, admiring me."

# game/Kurusu route.rpy:200
translate english kurusu_04a9d0ae:

    # k "Bom descanso, anjinho..."
    k "Have a Good rest, little angel ..."

# game/Kurusu route.rpy:202
translate english kurusu_d0f82e26:

    # "Ele dá um beijinho rápido na minha testa, virando para cair lentamente no sono junto comigo."
    "He gives a quick kiss on my forehead, turning to fall to sleep with me"

# game/Kurusu route.rpy:204
translate english kurusu_a20cefa7:

    # "..."
    "..."

# game/Kurusu route.rpy:206
translate english kurusu_2b5d9d5f:

    # "Eu acordo, Kurusu me olhando com o seu sorriso de sempre."
    "I wake up, Kurusu looking at me with his usual smile."

# game/Kurusu route.rpy:208
translate english kurusu_b6115764:

    # "Me levanto e estico o corpo, já soltando um bocejo longo."
    "I get up and stretch my body, already letting out a long yawn"

# game/Kurusu route.rpy:210
translate english kurusu_5e00c7d1:

    # mc "Y-yaawn~! Bom dia Kukki!"
    mc "Good Morning Kukki!"

# game/Kurusu route.rpy:212
translate english kurusu_ae002e24:

    # "Kurusu ri, sentando e cruzando as pernas."
    "Kurusu laughs, sitting and crossing his legs"

# game/Kurusu route.rpy:214
translate english kurusu_b989b2e0:

    # k "Bom dia [povname]. Descansou bem?"
    k "Good morning [povname]. Did you have a good sleep?"

# game/Kurusu route.rpy:216
translate english kurusu_1e12c676:

    # mc "Parece que eu dormi por dois dias!"
    mc "it feels like I slept for two days!"

# game/Kurusu route.rpy:218
translate english kurusu_60ec0034:

    # "Kurusu levanta, botando a mochila nas costas."
    "Kurusu laughs, then gets up, putting his backpack on his back."

# game/Kurusu route.rpy:220
translate english kurusu_9fb19b67:

    # k "Então... Vamos?"
    k "Ready to go?"

# game/Kurusu route.rpy:222
translate english kurusu_cf56c91a:

    # mc "Vamos! Ainda não comi nada, e acho que o sinal não bateu."
    mc "We will! I haven't eaten yet, and I don't think the signal has hit."

# game/Kurusu route.rpy:224
translate english kurusu_d6913275:

    # k "Não, não bateu. E provavelmente a Skelly já deve ter enlouquecido por não encontrar a gente lá."
    k "No, it didn't. And Skelly must have already gone crazy for not meeting us there."

# game/Kurusu route.rpy:226
translate english kurusu_f609b2ec:

    # mc "É... Ela é um pouco fora da casinha."
    mc "Yeah ... She's a little out of the house."

# game/Kurusu route.rpy:228
translate english kurusu_f8a1f2af:

    # "Nós dois rimos, imaginando como ela reagiria se nós não aparecessemos durante o dia"
    "We both laughed, wondering how she would react if we didn't show up during the day"

# game/Kurusu route.rpy:230
translate english kurusu_8ffc79e7:

    # "Ainda mais sabendo que eu decidi fazer com que o Kurusu me acompanhasse na festa."
    "Even more knowing that I decided to have Kurusu accompany me to the party."

# game/Kurusu route.rpy:232
translate english kurusu_fac83535:

    # k "Ela parecia bem animada pra te ajudar nesse seu plano."
    k "She seemed very excited to help you with your plan."

# game/Kurusu route.rpy:234
translate english kurusu_6e003a2b:

    # mc "Sim... Eu preciso por ele em prática logo~"
    mc "Yes ... I need to put it into practice soon~"

# game/Kurusu route.rpy:236
translate english kurusu_bc9b7038:

    # k "...Boa sorte com isso."
    k "...Good luck with that."

# game/Kurusu route.rpy:238
translate english kurusu_c08530cd:

    # mc "Obrigada, vou precisar."
    mc "Thanks, I need it."

# game/Kurusu route.rpy:240
translate english kurusu_90a379d5:

    # "Eu dou um risinho, Kurusu enfiando as mãos nos bolsos da calça."
    "I chuckle, Kurusu stuffing his hands in his pants pockets."

# game/Kurusu route.rpy:242
translate english kurusu_ada0d9ce:

    # "Nós fazemos o caminho de volta pro prédio do colégio e eventualmente nós voltamos para as nossas salas de aula separadas."
    "We make our way back to the school building and eventually we go back to our separate classrooms."

# game/Kurusu route.rpy:244
translate english kurusu_6fb62416:

    # "Chegando em casa eu jogo a minha mochila em cima da cama e deito nela."
    "When I get home I throw my backpack on the bed and lie down on it."

# game/Kurusu route.rpy:246
translate english kurusu_d0bef178:

    # mc "Uff! Que cansaço~"
    mc "Uff! What a day ~"

# game/Kurusu route.rpy:248
translate english kurusu_75059c12:

    # "Eu pego o meu celular e mando um oi para o nosso grupinho secreto. Skelly criou porque queria poder reclamar do namorado sem culpa."
    "I take out my cell phone and say hi to our secret little group. Skelly created it because she wanted to be able to complain about her boyfriend without guilt."

# game/Kurusu route.rpy:252
translate english kurusu_23e472cc:

    # "Hora de dormir. Amanhã vai ser um longo dia!"
    "Time to sleep. Tomorrow is going to be a long day"

# game/Kurusu route.rpy:255
translate english kurusu_bddda65b:

    # "Dia 2 - ultimo dia"
    "Day 2 - last day"

