﻿# TODO: Translation updated at 2020-07-15 04:31 (GMT+3)

# game/Anorak Route.rpy:6
translate english anorak_290fbf8c:

    # mc "Aquele rapaz parece ser gentil... Talvez ele aceite."
    mc "That boy seems to be nice... Maybe he'll accept."

# game/Anorak Route.rpy:8
translate english anorak_bde8af61:

    # mc "Merrii disse que ele frequenta a biblioteca, vai ser um bom lugar para conversar."
    mc "Merrii said that he likes to frequent the library, it will be a good place to talk"

# game/Anorak Route.rpy:10
translate english anorak_663e60d4:

    # mc "E eu preciso estudar pra prova, posso pegar o conteúdo por lá."
    mc "And I need to study for the exam, I can get some books over there."

# game/Anorak Route.rpy:12
translate english anorak_ac920b0d:

    # "Eu pego a minha mochila e vou em direção á biblioteca, passando pelos corredores de sala de aula."
    "I grabbed my backpack and headed towards the library, passing by the classroom hallways."

# game/Anorak Route.rpy:14
translate english anorak_913be8dc:

    # "~Biblioteca~"
    "~ Library ~"

# game/Anorak Route.rpy:16
translate english anorak_985eeddb:

    # "Procurando o assunto na estante, percebo que a coleção é vasta demais. Vai demorar um tempo até achar o que preciso..."
    "While searching for the subject through the shelves, I realized that the school's collection of books was extraordinarily vast. It would take a while to find what I need ..."

# game/Anorak Route.rpy:18
translate english anorak_ab8a76b3:

    # "Não é rígido demais uma escola ter estantes organizadas em ordem alfabética?..."
    "Isn't it too rigid for a school to have bookshelves organized in alphabetical order? ..." # Odd statement since schools are supposed to be precise and well organized. Consider removing or rewriting.

# game/Anorak Route.rpy:20
translate english anorak_4a8eec86:

    # "Eu não teria tanta disposição pra organizar desse jeito. Talvez conseguisse com a ajuda das minhas amigas..."
    "I couldn't be arsed to organize things that way. Well, maybe I could with the help of my friends..."

# game/Anorak Route.rpy:22
translate english anorak_26085bbc:

    # "Ou iríamos conversar o dia todo e deixar a bagunça completamente de lado."
    "Or perhaps we would end up talking all day and leave things unorganized."

# game/Anorak Route.rpy:24
translate english anorak_bbbdffeb:

    # mc "Não é hora de sair pensando em organizações de estante, [povname]. Eu preciso procurar aquele maldito livro para estudar ou estou encrencada no teste."
    mc "This is not the time to be thinking about organizing bookshelfs, [povname]. I need to look for that damn book or I'll fail the exam."

# game/Anorak Route.rpy:26
translate english anorak_98fdafc7:

    # who "Precisa de alguma ajuda?"
    who "Do you need any help?"

# game/Anorak Route.rpy:28
translate english anorak_140416ce:

    # "Uma voz sussurrada atrás de mim me paralisou por alguns segundos. Pensei que estaria sozinha nesse corredor!"
    "A faint voice reached out to me from behind. I froze. I thought I was by myself in this aisle!"

# game/Anorak Route.rpy:30
translate english anorak_906fddc6:

    # "Eu falei comigo mesma em voz alta? Urgh! Eu preciso parar com esse costume ou as pessoas vão achar que eu sou maluca."
    "Did they hear me talking to myself? Urgh! I need to stop doing this or people will start thinking I'm crazy."

# game/Anorak Route.rpy:32
translate english anorak_9d7363dd:

    # "Me virei para encontrar a pessoa que me fez a pergunta. Um garoto segurando um livro de capa marrom e título escrito em detalhes dourados me encarava sério, esperando uma suposta resposta."
    "I turned around to find a boy holding a brown book, its title written in golden letters. He was looking at me with a stern look on his face, apparently waiting for me to respond."

# game/Anorak Route.rpy:34
translate english anorak_ef665cb2:

    # "Eu devo estar demorando demais para falar com ele, julgando pelo jeito como ele me olha."
    "Judging by his facial expression, he was getting impatient." # This line isn't really necessary because the previous one is already effective at describing the other person's feelings. Don't know whether it also applies to the Portuguese version.

# game/Anorak Route.rpy:36
translate english anorak_9ce77d5e:

    # mc "Uh- n-na verdade não. Mas obrigada pela ajuda..."
    mc "Uh- n-not really. But thanks for the offer ..."

# game/Anorak Route.rpy:38
translate english anorak_fe178959:

    # "Ele assentiu e se virou em direção à mesa perto de outras estantes, sentando em uma das cadeiras."
    "He nodded, turned away from me and went to take a seat by one of the other bookshelves."

# game/Anorak Route.rpy:40
translate english anorak_d2ce7668:

    # "E com a mesma expressão que me respondeu calado, abriu o livro direto na metade, como se estivesse marcando mentalmente a página que havia parado."
    "And with the same serious look on his face, he swiftly opened the book and continued reading, as if he had never gotten up to approach me."

# game/Anorak Route.rpy:42
translate english anorak_c884cc98:

    # "Coincidentemente, o assunto do livro do rapaz parecia ser o que eu precisava."
    "Coincidentally, after further analyzing the book he was reading, it seemed to match the one I was looking for."

# game/Anorak Route.rpy:44
translate english anorak_f55d8639:

    # "Procurando por um livro parecido nas estantes, achei um semelhante. E abrindo aproximadamente na parte em que ele estava, me deparei com o assunto."
    "I returned to the shelves and found a copy of the same book. I sat down not too far from him, opened the book roughly as far as he had, flipped through a couple more pages and came across the subject I needed."

# game/Anorak Route.rpy:46
translate english anorak_8fea2db2:

    # mc "Hmm..."
    mc "Hmm..."

# game/Anorak Route.rpy:48
translate english anorak_246f6057:

    # "Ele parecia concentrado no que estava lendo, como se fosse uma obrigação ler aquilo. Talvez o assunto do teste dele não seja tão interessante assim."
    "I looked over to him once more. Whatever it was he was reading, it seemed to have grasped his full attention. His serious approach piqued my interest."

# game/Anorak Route.rpy:50
translate english anorak_f1a5afb7:

    # mc "Sobre o que é esse seu livro??"
    mc "What subject are you studying for?"

# game/Anorak Route.rpy:52
translate english anorak_7b7cfff5:

    # "O menino deu um saltinho da cadeira e suas faixas se esticaram como se também tivessem se assustado"
    "The boy jumped up from his chair, startled."

# game/Anorak Route.rpy:54
translate english anorak_b369f6c4:

    # "Ele fez um sinal de silêncio em frente a sua boca, um -shh- mudo com certa irritação."
    "He put his index finger on his mouth and let out a slight noise in irritation, singaling for me to shush."

# game/Anorak Route.rpy:56
translate english anorak_fc94dcc3:

    # who "Estamos em uma biblioteca."
    who "We're in a library!"

# game/Anorak Route.rpy:58
translate english anorak_bbd25e58:

    # mc "Desculpa, não quis te assustar."
    mc "Sorry, I didn't mean to scare you."

# game/Anorak Route.rpy:60
translate english anorak_bde8b7b9:

    # "O garoto suspirou, desapontado com a minha atitude. Ele parece prezar muito pelo silêncio da biblioteca..."
    "The boy sighed, unamused by my attitude. He seems to be very fond of the silence in the library ..."

# game/Anorak Route.rpy:62
translate english anorak_fd49ba0b:

    # "Eu consegui ouvir um risinho baixo vindo da direção dele, e ele pareceu virar de leve o rosto como se quisesse esconder o sorriso"
    "Despite that, I could hear a low chuckle coming from his direction, and he seemed to turn his face slightly, as if to hide his smile."

# game/Anorak Route.rpy:64
translate english anorak_8085fc43:

    # "Eu não poderia ver de qualquer forma, já que ele é todo enfaixado."
    "I couldn't really see it anyway, since he was all covered up."

# game/Anorak Route.rpy:66
translate english anorak_89a54983:

    # "Mesmo assim... Foi bem fofo."
    "Still ... It was pretty cute."

# game/Anorak Route.rpy:68
translate english anorak_46549bf8:

    # "Eu desviei a minha atenção para o livro do meu vizinho de mesa. Meus olhos passeiam pelas letras minúsculas da página grossa antes dele virar para a próxima página."
    "I looked over to what he was reading. My eyes wandered over the tiny letters in his book, which abruptly lost all meaning to me once he turned the page."

# game/Anorak Route.rpy:70
translate english anorak_bf7904da:

    # "Ele não parece ligar para a minha presença, lendo cada palavra do livro com atenção."
    "He didn't seem to be bothered by me trying to read alongside him."

# game/Anorak Route.rpy:72
translate english anorak_3bb61e16:

    # "A página em questão tinha a foto de um besouro que parecia ter chifres."
    "The page he had just turned to pictured a beetle possesing long, thick horns"

# game/Anorak Route.rpy:74
translate english anorak_a791407d:

    # "Não estudamos na mesma classe, então o professor dele deve estar dando uma matéria diferente. Nunca chegamos a ver esses besourinhos."
    "This is a first for me. We're not a part of the same class, so it seems like their teacher's already ahead."

# game/Anorak Route.rpy:76
translate english anorak_17662086:

    # "Confusa e com os pensamentos vindo um atrás do outro, não percebo quando ele deixa a atenção do livro e vira para me encarar."
    "While deep in thought, I failed to notice that he had stopped reading and had turned to face me."

# game/Anorak Route.rpy:78
translate english anorak_82ed955c:

    # who "Escaravelhos."
    who "Beetles."

# game/Anorak Route.rpy:80
translate english anorak_17fefa2d:

    # mc "O-oi?"
    mc "Y-yes?"

# game/Anorak Route.rpy:82
translate english anorak_43d7d69d:

    # "Eu me afasto um pouco segurando o meu livro com mais força nas mãos, fingindo estar interessada nele"
    "I pulled away from him a little, grasping my own book more tightly in my hands, pretending to have been reading it."

# game/Anorak Route.rpy:84
translate english anorak_49940c65:

    # who "Escaravelhos. É um livro de entomologia."
    who "Beetles... This is the entomology section."

# game/Anorak Route.rpy:86
translate english anorak_2739b6a8:

    # mc "Anta-... O quê?"
    mc "Ento-what?"

# game/Anorak Route.rpy:88
translate english anorak_6e978eeb:

    # "Ele revirou os olhos, suspirando. Deve achar que eu não notei pelas bandagens em seu rosto."
    "He rolled his eyes."

# game/Anorak Route.rpy:90
translate english anorak_22761be7:

    # who "Estudo de insetos."
    who "A scientific study of insects."

# game/Anorak Route.rpy:92
translate english anorak_16c653ad:

    # mc "Oh... E você se interessa por isso?"
    mc "Oh... Do you have any interest in that?"

# game/Anorak Route.rpy:94
translate english anorak_51bed45b:

    # who "Um pouco. Preciso estudar isso avulso do conteúdo das aulas."
    who "A little. I have to study this whenever I'm out of class." # I don't fully grasp the context of this sentence in Protuguese.

# game/Anorak Route.rpy:96
translate english anorak_39de79dc:

    # mc "Mesmo? Não está preocupado com as provas?"
    mc "Aren't you worried about the exam?"

# game/Anorak Route.rpy:98
translate english anorak_72dfa318:

    # who "Não muito... O conteúdo do meu pai é 'mais importante', segundo ele."
    who "Not really... My father's research is 'more important', according to him."

# game/Anorak Route.rpy:100
translate english anorak_1d987202:

    # "Ele soltou mais um suspiro e trocou de página com certo desdém."
    "He blew out another breath and turned to the next page with some disdain."

# game/Anorak Route.rpy:102
translate english anorak_1f668ddb:

    # "Eu fixo o olhar nas palavras e figuras. O texto parou de ser sobre a parte anatômica dos besouros, mudando para feitiços e magias usando eles e a energia deles."
    "I stared at the words and figures. The text no longer described beetle anatomy, instead detailing spells and their use."

# game/Anorak Route.rpy:104
translate english anorak_7477a87d:

    # mc "Parece ser interessante..."
    mc "This seems interesting..."

# game/Anorak Route.rpy:106
translate english anorak_2287d8b5:

    # who "E é. Mas o contexto nem tanto."
    who "It is, except for all the historical details."

# game/Anorak Route.rpy:108
translate english anorak_197ceb6c:

    # mc "Como assim?"
    mc "How so?"

# game/Anorak Route.rpy:110
translate english anorak_b786170a:

    # "O rapaz fecha o livro, voltando sua atenção diretamente para mim."
    "The boy closed the book, giving me his full attention."

# game/Anorak Route.rpy:112
translate english anorak_b594ae85:

    # who "Eu sou filho do faraó Pekhrari. Sou Anorak, o príncipe herdeiro."
    who "I am the son of Pharaoh Pekhrari. I am Anorak, a Crown Prince."

# game/Anorak Route.rpy:114
translate english anorak_62aea6ce:

    # a "Então tenho que estudar a cultura do meu povo - do meu reino, para poder governar."
    a "And because of that, I have to study the culture of my people - of my kingdom, in order to be able to rule over it."

# game/Anorak Route.rpy:116
translate english anorak_69d5db65:

    # a "Governar aos moldes do meu pai, que acha correto o que faz."
    a "Rule over it the same wat my father is ruling now, doing what he thinks is right."

# game/Anorak Route.rpy:118
translate english anorak_946640fd:

    # mc "E você não parece achar tanto..."
    mc "You don't seem to think he's right...?"

# game/Anorak Route.rpy:120
translate english anorak_cc9aef27:

    # a "Sim e não. Acho o modo como ele rege antiquado demais."
    a "Yes and no. I think the way he rules is too old-fashioned."

# game/Anorak Route.rpy:122
translate english anorak_ebea01df:

    # mc "Em questão de tecnologia ou cultural?"
    mc "In terms of technology or culture?"

# game/Anorak Route.rpy:124
translate english anorak_f9062f62:

    # a "Ambos. Ele não aceita que os tempos são outros, e que uma mão menos rígida seria mais adequada."
    a "Both. He doesn't seem to accept that times have changed, and that a more lax approach would be more appropriate."

# game/Anorak Route.rpy:126
translate english anorak_d5eb6f7c:

    # mc "Hmm... Mas você pode mudar isso quando for rei, não?"
    mc "Hmm ... But you can change that when you're king, can't you?"

# game/Anorak Route.rpy:128
translate english anorak_cb0aa4bc:

    # "Ele cruza os braços, pensativo."
    "He crossed his arms to think."

# game/Anorak Route.rpy:130
translate english anorak_e8a3ffa3:

    # "Parando pra analisar ele, por baixo dessas ataduras ele parece ser uma pessoa bem interessante"
    "If you take a moment to look at him, then under these bandages seems to be a very interesting person."

# game/Anorak Route.rpy:132
translate english anorak_91fc7f2a:

    # "O cabelo azul é parcialmente escondido pelas faixas, e as orelhas levemente pontudas..."
    "Blue hair partially hidden by the bands, slighly pointed ears..."

# game/Anorak Route.rpy:134
translate english anorak_ddf0c0ab:

    # "Os olhos vermelhos, fixos no livro, pensativo."
    "His red eyes, fixated on the book."

# game/Anorak Route.rpy:136
translate english anorak_fe55e98e:

    # "Anorak é bem fofo. Espero que ele aceite ir comigo no baile."
    "Anorak is very cute. I hope he goes to prom with me."

# game/Anorak Route.rpy:138
translate english anorak_d531cd13:

    # "Sua voz sai um pouco baixa, atrapalhando os meus pensamentos e acabando com o meu monólogo interno."
    "His voice came out a little deeper than before, disturbing my thoughts."

# game/Anorak Route.rpy:140
translate english anorak_b2749e31:

    # a "Eu não... Tenho certeza se quero ser governante."
    a "I'm not sure I want to be a ruler."

# game/Anorak Route.rpy:142
translate english anorak_3c2c2f9e:

    # mc "É uma decisão bem importante..."
    mc "It's a very heavy decision."

# game/Anorak Route.rpy:144
translate english anorak_83236d6a:

    # a "É sim. E eu não tenho maturidade pra pensar nisso ainda."
    a "Indeed. And I don't feel mature enough to make it yet."

# game/Anorak Route.rpy:146
translate english anorak_98343016:

    # "Anorak suspira, voltando a prestar atenção no livro."
    "Anorak sighed and returned to reading."

# game/Anorak Route.rpy:148
translate english anorak_da5d4305:

    # a "Você deveria voltar a ler o seu livro. As provas são amanhã."
    a "You should return to studying. The exams are tomorrow"

# game/Anorak Route.rpy:150
translate english anorak_eab62c19:

    # mc "Ah, a prova! Não lembrava!"
    mc "Ah, the exams! I forgot!"

# game/Anorak Route.rpy:152
translate english anorak_b5114841:

    # a "Shh. Fale baixinho."
    a "Shh. Lower your voice."

# game/Anorak Route.rpy:154
translate english anorak_02233057:

    # mc "S-sim..."
    mc "Y-yes..."

# game/Anorak Route.rpy:156
translate english anorak_9c033fa3:

    # "Com o rosto corado por elevar minha voz novamente, eu abro o livro pra esconder a minha vergonha."
    "With my face turning red from embarrassment, I buried my head in the book to hide my shame."

# game/Anorak Route.rpy:158
translate english anorak_e9aab348:

    # "Anorak sorri por baixo das faixas, focando em seu estudo. As ataduras escondem o sorriso, mas seus olhos não."
    "Anorak smiled under his bindings. The bandages might've concealed it, but you could see it in his eyes."

# game/Anorak Route.rpy:160
translate english anorak_9b8464d5:

    # "Com o passar do tempo percebi alguns olhares em minha direção, vindas do rapaz ao meu lado. Não sei dizer se eram para mim ou para o meu livro..."
    "Finally I had turned to studying. Occasionally I would catch Anorak glancing in my direction. I couldn't tell if he was looking at me or the book I was holding..."

# game/Anorak Route.rpy:162
translate english anorak_67f6e559:

    # "Eventualmente eu acabo me concentrando demais para trocar alguma palavra com ele"
    "But eventually I ended up concentrating really hard on my studies and stopped noticing."

# game/Anorak Route.rpy:164
translate english anorak_016e1e0f:

    # "E assim ficamos até a biblioteca fechar. Nos damos um breve ~tchauzinho~ e cada um vai para a sua casa."
    "And so we stayed until the library closed. We give each other a brief ~ bye-bye ~ and went our seperate ways."

# game/Anorak Route.rpy:169
translate english anorak_f766bbc8:

    # "Hoje cheguei mais cedo no colégio. Eu esperava que por ser dia de prova, os corredores ficassem mais cheios"
    "Today I arrived to school earlier than usual. I expected the halls to be more crowded, since our exams were starting today."

# game/Anorak Route.rpy:171
translate english anorak_5d587c9b:

    # "Então achei que seria uma boa idéia revisar antes das aulas começarem."
    "It's probably a good idea to revise before the exams begin."

# game/Anorak Route.rpy:173
translate english anorak_ffeb5714:

    # "Minha sala de aula é a melhor opção. Alguns alunos costumam se desesperar e correr para a biblioteca, na esperança de que algum aluno com boas notas consiga os ensinar nos ultimos segundos restantes."
    "Doing it in the classroom is probably my best option. Some of the lazier students often realize the mess they're in only right before the exams, so they all flock to the library in a last-ditch effort to learn something from high-achieving students."

# game/Anorak Route.rpy:175
translate english anorak_a73c03e7:

    # "O conteúdo é bem básico... Me impressionou que o rapaz de ontem estivesse tão preso assim na leitura."
    "The subject matter is quite basic... I'm kind of surprised the boy from yesterday was so caught up in reading."

# game/Anorak Route.rpy:177
translate english anorak_2ecff6b7:

    # "Insetos. Não é exatamente o que eu esperava estudar nesse semestre, mas o Stein disse que eles são super importantes para nós."
    "Insects. It's not exactly what I expected to be studying this semester, but Stein did say they are super important to us."

# game/Anorak Route.rpy:179
translate english anorak_b953d900:

    # "E o livro não é como os outros da biblioteca. Não são folhas e mais folhas dizendo chatices."
    "And the books in the library aren't like they are for other subjects - they're not boredom inducing."

# game/Anorak Route.rpy:181
translate english anorak_b81eb25e:

    # "Eu acho que posso acabar gostando do assunto."
    "I think I might end up liking this subject."

# game/Anorak Route.rpy:183
translate english anorak_86b6caf2:

    # "Enquanto eu caminho pelo corredor eu lembro daquele daquele rapaz. Ele não é exatamente quieto, mas é bem misterioso."
    "As I walked down the halls I remembered the guy from yesterday. He's not really that quiet, but he's quite mysterious."

# game/Anorak Route.rpy:185
translate english anorak_92e7695e:

    # "Quero muito que ele aceite ir comigo no baile..."
    "I really wish he'd go to prom with me."

# game/Anorak Route.rpy:187
translate english anorak_c3c5314b:

    # "É um pouco improvável ele não ter par, por ser príncipe. Ele já deve ter alguém para ir."
    "It's pretty unlikely that he doesn't have a partner though, since he is a prince and all. Yeah... he probably has someone to go with already."

# game/Anorak Route.rpy:189
translate english anorak_9eef9951:

    # "Um esbarro no meu braço balança os livros das minhas mãos e quase os derruba, me fazendo perder completamente a linha de pensamento."
    "A bump on my arm struck the books I was carrying out of my hands and knocked me on my behind."

# game/Anorak Route.rpy:191
translate english anorak_33f0f9ed:

    # "Enquanto eu recobrava o equilíbrio perdido após a pessoa esbarrar em mim, ela se aproximava, fazendo um gesto cauteloso"
    "As I regained my balance, the person that bumped into me approached cautiously, as to not alarm me further."

# game/Anorak Route.rpy:193
translate english anorak_95a62e22:

    # "Ela segura meu braço, ajudando a me manter em pé junto com os livros. Eu reconheço essa mão"
    "They reached out to me in an effort to help ...I recognized their hand."

# game/Anorak Route.rpy:195
translate english anorak_e94b431f:

    # "Meus olhos reconhecem algumas bandagens que cobrem o corpo da pessoa a minha frente"
    "I recognized the bandages covering the person in front of me."

# game/Anorak Route.rpy:197
translate english anorak_c0109970:

    # "De todas as pessoas para passar por mim, justo ele."
    "Of all the people that could've passed me, it was him."

# game/Anorak Route.rpy:199
translate english anorak_f1ddece7:

    # "Justo quem eu queria ver agora."
    "The one I wanted to see."

# game/Anorak Route.rpy:201
translate english anorak_e2e091d9:

    # a "Oh. Perdão, não quis incomodar você."
    a "Oh. Sorry, I didn't mean to disturb you."

# game/Anorak Route.rpy:203
translate english anorak_9d067042:

    # "Vendo o quão próximos estamos logo ele se recolhe, cruzando os braços"
    "He took a step back and crossed his arms."

# game/Anorak Route.rpy:205
translate english anorak_d9046150:

    # "Nos encontramos novamente com ele quase me matando."
    "Meeting him again turned out to be pretty painful."

# game/Anorak Route.rpy:207
translate english anorak_d60436c1:

    # mc "Não foi nada, não. Eu tô bem."
    mc "No, it's nothing. I'm fine."

# game/Anorak Route.rpy:209
translate english anorak_d44c66a2:

    # "Eu forço um sorrisinho, escondendo o susto do baque repentino"
    "I forced a slight smile, trying to hide the shock from him bumping into me."

# game/Anorak Route.rpy:211
translate english anorak_12085720:

    # "Ele suspirou aliviado, fazendo parecer que quase me derrubou de um lugar alto."
    "He sighed in relief, as if a huge weight had been lifted off his chest."

# game/Anorak Route.rpy:213
translate english anorak_6c492594:

    # a "Ótimo, isso é bom. Eu deveria ter mais cuidado..."
    a "Great, that's good. I should be more careful..."

# game/Anorak Route.rpy:215
translate english anorak_709e73a4:

    # "Anorak sorri por baixo das suas bandagens. Só consigo ver seus olhos por trás delas, diminuindo até chegar em uma linha fina."
    "Judging from his eyes, Anorak smiled."

# game/Anorak Route.rpy:217
translate english anorak_bdf65664:

    # "Ele é bem fofo."
    "He's really cute."

# game/Anorak Route.rpy:219
translate english anorak_95c60c73:

    # "Imagino o que teria atrás dessas bandagens todas..."
    "I wonder what hides behind all those bandages..."

# game/Anorak Route.rpy:221
translate english anorak_351c99e9:

    # "Anorak permanece imóvel, mas agora está olhando para algo nas minhas mãos, observando."
    "Anorak remained motionless, but he was starting at one of the books I was holding."

# game/Anorak Route.rpy:223
translate english anorak_33ff56fb:

    # "Acho que reconheceu o que eu estava lendo."
    "I think he's recognized what I've been reading."

# game/Anorak Route.rpy:225
translate english anorak_91a61340:

    # a "O livro que eu peguei ontem?"
    a "Isn't this the same book I picked up yesterday?"

# game/Anorak Route.rpy:227
translate english anorak_0e061dc5:

    # "Ele me olha nos olhos, me fazendo tremer de leve."
    "He looked me right in the eyes; it sent a chill down my spine."

# game/Anorak Route.rpy:229
translate english anorak_ec1f0c8b:

    # "Ele não é nem um pouco ameaçador, mas quando me olha diretamente..."
    "I don't think he's threatening, it's just when he looked at me directly like that..."

# game/Anorak Route.rpy:231
translate english anorak_35520313:

    # "Os olhos vermelhos dele são penetrantes."
    "It was like his red eyes were piercing through into my soul." # Maybe too poetic?

# game/Anorak Route.rpy:233
translate english anorak_c47d7139:

    # mc "S-sim... O teste é daqui a pouco."
    mc "Y-yeah... The exams will start soon."

# game/Anorak Route.rpy:235
translate english anorak_6560e2c1:

    # a "Hm. Boa sorte, acho que vai precisar."
    a "Hm. Good luck, you'll need it!"

# game/Anorak Route.rpy:237
translate english anorak_17ee6250:

    # "Ele não parece ser de muita conversa. Não é que ele tente ser seco, é só..."
    "His wish came off as a bit unattentive. It's not that he was trying to be dry, it's just..."

# game/Anorak Route.rpy:239
translate english anorak_1bc7fdac:

    # "Talvez não saiba como conversar?"
    "Maybe he doesn't know how to talk with other people?"

# game/Anorak Route.rpy:241
translate english anorak_91f120e3:

    # "Talvez seja tímido?"
    "Perhaps he's really shy?"

# game/Anorak Route.rpy:243
translate english anorak_dcd00715:

    # mc "Obrigada. É bom um pouco de sorte, mas vai ser moleza. Entomologia é divertido de ler."
    mc "Thanks. A little luck is good, but it will be easy... Entomology is fun to read." # These two sentences need to be split off somehow (with a monologue along the lines of "I tried to rekindle the conversation"), otherwise they seem connected, when they're contextually not really

# game/Anorak Route.rpy:245
translate english anorak_61154876:

    # "Os olhos do rapaz brilharam ao ouvir o que eu disse. Parecia interessado pelo caminho que a conversa estava tomando."
    "The boy's eyes lit up when he heard what I had said. It seemed he wanted to know more about me taking interest in entomology."

# game/Anorak Route.rpy:247
translate english anorak_3661ab1f:

    # a "Você não parecia conhecer muito de entomologia ontem. Esse assunto te interessa?"
    a "You didn't seem to know much about entomology yesterday. Have you gotten curious about it?"

# game/Anorak Route.rpy:249
translate english anorak_dc4e2937:

    # mc "É fascinante conhecer os bichinhos que existem por aí."
    mc "Yeah. I find discovering new types of insects incredibly interesting."

# game/Anorak Route.rpy:251
translate english anorak_e934674f:

    # mc "E escaravelhos são bonitinhos e intrigantes."
    mc "And I find beetles both cute and intriguing."

# game/Anorak Route.rpy:253
translate english anorak_b602c25a:

    # a "Escaravelhos são sagrados para a minha cultura..."
    a "Beetles are of a sacred part of my culture..."

# game/Anorak Route.rpy:255
translate english anorak_44b5bc04:

    # "Ele sorriu mais uma vez, tentando esconder os olhos fechados com o cabelo. Meu coração palpitou forte, e eu não pude deixar de esconder um rubor."
    "He smiled again, trying to conceal his eyes with his hair. My heart began pounding heavily and my face reddened."

# game/Anorak Route.rpy:257
translate english anorak_1185c188:

    # "Eu aperto um pouco mais o livro, tentando me tirar desse transe."
    "I squeezed my books closer to myself and tried to calm my excitement."

# game/Anorak Route.rpy:259
translate english anorak_48c127ef:

    # a "Que tolice..."
    a "How foolish..."

# game/Anorak Route.rpy:261
translate english anorak_06109310:

    # "Meu rosto esquentou. Ele estava falando de mim? O que ele quis dizer?"
    "I started to blush again. Was he talking about me? What did he mean?"

# game/Anorak Route.rpy:263
translate english anorak_f6be6ee0:

    # "Eu estar parada na sua frente não ajuda. Eu devo estar parecendo uma idiota."
    "Being like this while in his presence made it even worse. I must've looked pretty stupid to him."

# game/Anorak Route.rpy:265
translate english anorak_207f1514:

    # "Ele balançou a cabeça, levando uma das suas mãos para a parte de trás da sua cabeça e coçando de leve."
    "He tilted his head slighly and rubbed the back of his neck with a clumsy look on his face."

# game/Anorak Route.rpy:267
translate english anorak_2e573ca4:

    # a "...Como pude esquecer de me apresentar devidamente a você?"
    a "...I feel like I haven't properly introduced myself to you."

# game/Anorak Route.rpy:269
translate english anorak_5494679c:

    # mc "Ah. Era isso."
    mc "Ah. That's what it was."

# game/Anorak Route.rpy:271
translate english anorak_98cb8871:

    # a "Isso?"
    a "That?"

# game/Anorak Route.rpy:273
translate english anorak_0ad4277d:

    # mc "N-nada. Eu pensei que você tinha me chamado de tola."
    mc "N-never mind. I thought you called me foolish, that's all."

# game/Anorak Route.rpy:275
translate english anorak_4309a204:

    # a "Você é tola."
    a "You are so foolish."

# game/Anorak Route.rpy:277
translate english anorak_f2316b97:

    # mc "Ah."
    mc "Ah!"

# game/Anorak Route.rpy:279
translate english anorak_00834dd2:

    # "O sangue subiu tão rápido ao meu rosto, deixando ele completamente vermelho."
    "Blood rose to my face in embarrassment in an instant, making it completely red once again."

# game/Anorak Route.rpy:281
translate english anorak_fd71e1c8:

    # "Anorak solta um risinho baixo, estendendo a mão para mim."
    "Anorak chuckled to himself and extended his hand outward."

# game/Anorak Route.rpy:283
translate english anorak_5e410a11:

    # a "Eu me chamo Anorak."
    a "My name is Anorak."

# game/Anorak Route.rpy:285
translate english anorak_884619e7:

    # "Eu assenti, retribuindo com um sorriso gentil, segurando em sua mão."
    "I nodded and shook his hand with a gentle smile on my face."

# game/Anorak Route.rpy:287
translate english anorak_5f019469:

    # mc "Eu sou a Eu sou a [povname]"
    mc "I'm [povname]..."

# game/Anorak Route.rpy:289
translate english anorak_009a5fb7:

    # a "Eu sei o seu nome. Mesmo assim, é um prazer, [povname]."
    a "I know. But still, I'm pleased to meet you, [povname]."

# game/Anorak Route.rpy:291
translate english anorak_eabfef28:

    # mc "Você sabe o meu nome?"
    mc "You've heard of me before?"

# game/Anorak Route.rpy:293
translate english anorak_2c362dce:

    # a "É... Eu ouço as suas amigas te chamando assim e... Bom, te vejo depois das provas?"
    a "Yeah ... I've heard your friends calling you that and... Well, see you after the exams?"

# game/Anorak Route.rpy:295
translate english anorak_0b90639f:

    # mc "É verdade, o teste! Preciso ir, mais tarde a gente se encontra!"
    mc "Right, the exams! I gotta go, see you later!"

# game/Anorak Route.rpy:297
translate english anorak_c1838fce:

    # "Eu retomo meu caminho sem esperar por uma resposta. O Stein não ia me deixar entrar se chegasse atrasada."
    "I turned to leave, not giving Anorak his chance to say goodbye. I was in a hurry. Knowing Stein, he wouldn't let me into the classroom if he were to catch me being late."

# game/Anorak Route.rpy:299
translate english anorak_2951b546:

    # "Eu me viro para trás, já com uma certa distância do rapaz."
    "I looked back. Anorak was still standing there."

# game/Anorak Route.rpy:301
translate english anorak_fd84ba83:

    # mc "Quem sabe não podemos conversar mais sobre os insetinhos algum outro dia?"
    mc "Maybe we can talk about insects some other time?"

# game/Anorak Route.rpy:303
translate english anorak_ef344212:

    # "Anorak arqueou uma sobrancelha, um leve vermelho surgiu em suas bochechas e a sua expressão fazia parecer curioso sobre o que falei."
    "Anorak blushed and gave me a curious look. It looked like he hadn't understood what I said."

# game/Anorak Route.rpy:305
translate english anorak_8b7a8c59:

    # a "Está me convidando para alguma coisa?"
    a "Are you asking me out?"

# game/Anorak Route.rpy:307
translate english anorak_2b5ecb3e:

    # "...Realmente soou como um convite para um encontro."
    "...It really did sound like I was asking him out."

# game/Anorak Route.rpy:309
translate english anorak_158b9ad6:

    # "E é exatamente o que eu quero com ele. Quero convidar para o baile!"
    "And that is exactly what I wanted. I want him to go to prom with me!"

# game/Anorak Route.rpy:311
translate english anorak_47eb0949:

    # mc "Me encontra no pátio depois das provas. Talvez eu tenha te convidado mesmo."
    mc "Meet me in the courtyard after the exams. I have something I want to tell you."

# game/Anorak Route.rpy:313
translate english anorak_fbed8607:

    # a "Você não tem certeza, mesmo pedindo?"
    a "You can't tell me now?"

# game/Anorak Route.rpy:315
translate english anorak_2b35b6fe:

    # mc "..."
    mc "..."

# game/Anorak Route.rpy:317
translate english anorak_45dc34ec:

    # "Eu desviei o olhar, não sabendo como responder."
    "I looked away briefly because I didn't know how I should respond."

# game/Anorak Route.rpy:319
translate english anorak_39831405:

    # mc "N-não..."
    mc "N-no..."

# game/Anorak Route.rpy:321
translate english anorak_8f503702:

    # "Ele riu, assentindo com a cabeça"
    "He laughed and then nodded."

# game/Anorak Route.rpy:323
translate english anorak_72c918a2:

    # a "Certo. Até mais tarde, Núbia."
    a "Sure. See you later, Nubia."

# game/Anorak Route.rpy:325
translate english anorak_e1235eea:

    # "Anorak acenou e continuou o seu caminho."
    "Anorak waved and went on his way."

# game/Anorak Route.rpy:327
translate english anorak_e97e3028:

    # "Ele me chamou de quê? N-núbia?"
    "What did he call me? N-nubia?"

# game/Anorak Route.rpy:329
translate english anorak_2ade4026:

    # "Deve significar alguma coisa na língua dele. Espero que não seja algo ruim ou bobo..."
    "It's probably something characteristic to his kingdom. I hope it isn't something bad or silly..."

# game/Anorak Route.rpy:331
translate english anorak_92fa0760:

    # mc "A prova!"
    mc "The exams!"

# game/Anorak Route.rpy:333
translate english anorak_8a7ca2a2:

    # "Correr para a sala é a minha única opção agora. Stein já deve estar entrando."
    "I'll have to hurry and run. Stein must be in the classroom by now."

# game/Anorak Route.rpy:335
translate english anorak_d57fc71f:

    # "Agora não tenho só a prova para me preocupar..."
    "it's not just the exams I'm anxious about now..."

# game/Anorak Route.rpy:337
translate english anorak_48452b8b:

    # "Eu convidei mesmo ele pra sair!"
    "I'm really going to ask him out!"

# game/Anorak Route.rpy:339
translate english anorak_5e925dc3:

    # "Não foi um pedido concreto, então vou ter que esperar após as provas para ter certeza da resposta."
    "I don't know how he feels about me, I'll have to wait until we're done with the exams to see how it goes."

# game/Anorak Route.rpy:341
translate english anorak_1756713c:

    # "Espero que seja positiva..."
    "I hope he agrees..."

# game/Anorak Route.rpy:343
translate english anorak_d696615a:

    # "~Após a prova~"
    "~After the exams~"

# game/Anorak Route.rpy:345
translate english anorak_8c2cb179:

    # "Sai da sala ás pressas. Mal consegui arrumar meu material e já estava no corredor."
    "I hurried out of the classroom. I was barely able to pack all my stuff when I was already in the hallway."

# game/Anorak Route.rpy:347
translate english anorak_ca3d367a:

    # "Eu preciso saber da resposta dele. Preciso saber se ele não estava me fazendo de boba..."
    "I need to know his answer. I need to know if he was making a fool of me ..."

# game/Anorak Route.rpy:349
translate english anorak_f1c42ea5:

    # "Corri até o pátio, passando por várias pessoas. Ele já deve estar vindo."
    "I ran out into the courtyard, passing several people. He must be coming by now, too."

# game/Anorak Route.rpy:351
translate english anorak_612053bd:

    # "Sentado em um banco, observando as pessoas passarem. Lá estava Anorak."
    "Sat on a bech watching people, there he was."

# game/Anorak Route.rpy:353
translate english anorak_71e98572:

    # mc "Okay... Respira fundo e vai lá."
    mc "Okay... take a deep breath and go talk to him."

# game/Anorak Route.rpy:355
translate english anorak_0b66864d:

    # "Eu seguro minha bolsa com mais força, camInhando até ele."
    "I held tighter onto my bag and walked over to him."

# game/Anorak Route.rpy:357
translate english anorak_d328a2c6:

    # "Ele parecia em transe, perdido em pensamentos. Não notou quando me aproximei, sentando em seu lado."
    "He looked as if he was lost in thought, because he didn't notice me approaching. I took a seat next to him."

# game/Anorak Route.rpy:359
translate english anorak_bbb7ef69:

    # mc "Oi Anorak. Tudo bem?"
    mc "Hey, Anorak. Are you all right?"

# game/Anorak Route.rpy:361
translate english anorak_971dd478:

    # "Anorak se virou para mim, me olhando por um tempo antes de ter alguma reação."
    "Anorak turned and looked at me, not saying anything for a brief moment."

# game/Anorak Route.rpy:363
translate english anorak_b2d39858:

    # a "Oh. [povname]! Você veio!"
    a "Oh. [Povname]! You came!"

# game/Anorak Route.rpy:365
translate english anorak_1ff9340d:

    # mc "Heheh. Você estava distraído."
    mc "heheh. You were distracted."

# game/Anorak Route.rpy:367
translate english anorak_284b1182:

    # a "Estava pensando enquanto esperava você..."
    a "I was thinking about something while waiting for you."

# game/Anorak Route.rpy:369
translate english anorak_c6ed6e2d:

    # mc "Eu te fiz esperar muito?"
    mc "Did you wait long?"

# game/Anorak Route.rpy:371
translate english anorak_7c9592e1:

    # "Eu me ajeito no banco, encolhendo as pernas para perto uma da outra."
    "I adjust myself on the bench, pulling my legs in close to each other."

# game/Anorak Route.rpy:373
translate english anorak_1afa2027:

    # a "Não. Na verdade nem vi o tempo passar. O que me fez ficar assim foi o seu pedido hoje mais cedo..."
    a "No. Actually, I didn't even notice the time passing by. I was thinking about your request from earlier today."

# game/Anorak Route.rpy:375
translate english anorak_cdbab0da:

    # mc "A-ah... Eu te chamei pra sair né?..."
    mc "A-ah... me asking you out, right?"

# game/Anorak Route.rpy:377
translate english anorak_edabd01d:

    # a "Aquilo foi sincero? Você quer mesmo sair comigo?"
    a "Was that a genuine request? Do you really want to hang out with me?"

# game/Anorak Route.rpy:379
translate english anorak_4b7ec490:

    # mc "H-hmm... Digo, sim?"
    mc "Hmm.. I mean, yeah?"

# game/Anorak Route.rpy:381
translate english anorak_cf21492b:

    # "Ele se vira em minha direção e me encara nos olhos"
    "He turned towards me and looked into my eyes."

# game/Anorak Route.rpy:383
translate english anorak_deaa7461:

    # "Com os seus olhos sendo a única coisa aparente em seu rosto, eles ficam um pouco intimidadores de perto."
    "With his eyes being the only visible feature on his face, they feel a little intimidating up close."

# game/Anorak Route.rpy:385
translate english anorak_708adccd:

    # "Anorak parece estar procurando por alguma coisa no meu rosto. Parece desconfiado, ou com medo..."
    "Anorak seems to be carefully analyzing my face. He seems suspicious, or maybe afraid..."

# game/Anorak Route.rpy:387
translate english anorak_9beb3911:

    # "Ficamos assim por alguns segundos, até ele desviar o olhar e puxar uma das faixas do seu braço direito."
    "We stayed like that for a few seconds, until he looked away and pulled at one of the bandages on his right arm."

# game/Anorak Route.rpy:389
translate english anorak_ac4ba934:

    # a "Tudo bem. Se você quer... Podemos nos ver."
    a "Okay. If you want ... We can hang out."

# game/Anorak Route.rpy:391
translate english anorak_f206e72d:

    # mc "Mesmo? Quando?"
    mc "Really? When?"

# game/Anorak Route.rpy:393
translate english anorak_18c9e640:

    # "Eu esboço um sorriso, me apoiando com as mãos no banco e me aproximando dele"
    "I smiled and leaned back further into the bench, closer to him."

# game/Anorak Route.rpy:395
translate english anorak_784b19b4:

    # "Anorak não consegue esconder o pulinho, evitando me olhar."
    "Anorak jumped a little and looked away."

# game/Anorak Route.rpy:397
translate english anorak_2173be0b:

    # a "A-amanhã... Se estiver tudo bem pra você."
    a "T-tomorrow, if that's alright with you."

# game/Anorak Route.rpy:399
translate english anorak_f4bac734:

    # mc "Por mim ótimo! Me dá seu número e eu te mando uma mensagem mais tarde~"
    mc "Sounds good to me! Give me your number and I'll text you later ~"

# game/Anorak Route.rpy:401
translate english anorak_08173652:

    # a "C-certo..."
    a "Sure..."

# game/Anorak Route.rpy:403
translate english anorak_c8528cd1:

    # "Ele pega o meu celular e digita os números, salvando seu contato."
    "I gave him my cellphone and he wrote down his number."

# game/Anorak Route.rpy:405
translate english anorak_ca1d3163:

    # "Durante todo o processo ele evitou ao máximo olhar para mim"
    "While doing so he avoided looking at me as much as possible"

# game/Anorak Route.rpy:407
translate english anorak_57c5eb50:

    # "Eu consigo ver o vermelho do rosto dele se expandir para as orelhas pontudas."
    "His face was read right up to his ears."

# game/Anorak Route.rpy:409
translate english anorak_d328e2e8:

    # a "Eu... Eu preciso ir indo."
    a "I ... I need to get going."

# game/Anorak Route.rpy:411
translate english anorak_a64552f3:

    # "Anorak se levanta, estendendo a mão para mim. Seu olhar fixo em mim, ainda com as bochechas coradas."
    "Anorak got up and reached out to me. His gaze was still fixed on me, his cheecks still red."

# game/Anorak Route.rpy:413
translate english anorak_a89741b7:

    # mc "Oh... Obrigada."
    mc "Oh... thank you."

# game/Anorak Route.rpy:415
translate english anorak_c2d68a2b:

    # "Eu pego sua mão, levantando do banco."
    "I took his hand and rose from the bench."

# game/Anorak Route.rpy:417
translate english anorak_5b226089:

    # "Ele sorri pra mim, levemente deixando minha mão escapar da dele."
    "He smiled at me and relaxed his grip."

# game/Anorak Route.rpy:419
translate english anorak_f32582da:

    # a "Te vejo amanhã, [povname]?"
    a "See you tomorrow, [povname]?"

# game/Anorak Route.rpy:421
translate english anorak_6002d209:

    # mc "S-sim... Amanhã..."
    mc "Y-yeah ... Tomorrow ..."

# game/Anorak Route.rpy:423
translate english anorak_02062524:

    # "Eu sorrio de volta, vendo ele se dirigir até a entrada do colégio e desaparecer em uma charrete"
    "I smiled back and watched him go to the school entrance. He left the school in a chariot."

# game/Anorak Route.rpy:425
translate english anorak_81ff86b9:

    # "Logo eu chego em casa e jogo minhas coisas na cama. Puxando o celular para conversar no chat com as meninas, vejo o seu contato salvo no meu celular."
    "I soon arrived home myself. I threw all my belongings on the bed and took out my phone to chat with the girls. Then I noticed his phone number."

# game/Anorak Route.rpy:427
translate english anorak_78e41693:

    # "Ele salvou com um coraçãozinho..."
    "He saved it with a little heart next to it."

# game/Anorak Route.rpy:429
translate english anorak_28633a35:

    # "Não consigo conter um sorriso."
    "I can't contain my smile."

# game/Anorak Route.rpy:431
translate english anorak_77ac12e1:

    # "Depois de conversar com elas, as meninas me encorajaram a chamar ele para conversar."
    "After talking with the girls, they encouraged me to send him a message."

# game/Anorak Route.rpy:433
translate english anorak_722e430e:

    # "Combinamos de nos encontrar na frente do museu amanhã. É um lugar calmo e com várias coisas interessantes para vermos."
    "We agreed to meet in front of the museum tomorrow. It is a calm place with many interesting things to see."

# game/Anorak Route.rpy:435
translate english anorak_0826a9bd:

    # "Tenho a impressão de que não vou conseguir dormir hoje."
    "I have the impression that I will not be able to sleep today."
