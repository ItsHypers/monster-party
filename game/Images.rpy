﻿init:
    
    image gal01 = MaxScale("/gallery/Angel.jpg")
    image gal02 = MaxScale("/gallery/bars.jpg")
    image gal03 = MaxScale("/gallery/cool_guy.jpg")
    image gal04 = MaxScale("/gallery/Crows.jpg")
    image gal05 = MaxScale("/gallery/demon_kitten.jpg")
    image gal06 = MaxScale("/gallery/Guy and Hawk.jpg")
    image gal07 = MaxScale("/gallery/guy.jpg")
    image gal08 = MaxScale("/gallery/heimdall.jpg")
    image gal09 = MaxScale("/gallery/Hot Wizards.jpg")
    image gal10 = MaxScale("/gallery/Otter 02.jpg")
    image gal11 = MaxScale("/gallery/otter.jpg")
    image gal12 = MaxScale("/gallery/lizard.jpg")
    image gal13 = MaxScale("/gallery/Samuri.jpg")
    image gal14 = MaxScale("/gallery/voilet eyes in bath.jpg")
    image gal15 = MaxScale("/gallery/Warrior.jpg")
    image gal16 = MaxScale("/gallery/Winter Anime Guy.jpg")
    image gal17 = MaxScale("/gallery/Wolf Boy.jpg")

    image black = "#000"

    image lockedthumb = MinScale("/gallery/lockedthumb.png", maxthumbx, maxthumby)

    image thumb01 = MinScale("/gallery/Angel.jpg", maxthumbx, maxthumby)
    image thumb02 = MinScale("/gallery/bars.jpg", maxthumbx, maxthumby)
    image thumb03 = MinScale("/gallery/cool_guy.jpg", maxthumbx, maxthumby)
    image thumb04 = MinScale("/gallery/Crows.jpg", maxthumbx, maxthumby)
    image thumb05 = MinScale("/gallery/demon_kitten.jpg", maxthumbx, maxthumby)
    image thumb06 = MinScale("/gallery/Guy and Hawk.jpg", maxthumbx, maxthumby)
    image thumb07 = MinScale("/gallery/guy.jpg", maxthumbx, maxthumby)
    image thumb08 = MinScale("/gallery/heimdall.jpg", maxthumbx, maxthumby)
    image thumb09 = MinScale("/gallery/Hot Wizards.jpg", maxthumbx, maxthumby)
    image thumb10 = MinScale("/gallery/Otter 02.jpg", maxthumbx, maxthumby)
    image thumb11 = MinScale("/gallery/otter.jpg", maxthumbx, maxthumby)
    image thumb12 = MinScale("/gallery/lizard.jpg", maxthumbx, maxthumby)
    image thumb13 = MinScale("/gallery/Samuri.jpg", maxthumbx, maxthumby)
    image thumb14 = MinScale("/gallery/voilet eyes in bath.jpg", maxthumbx, maxthumby)
    image thumb15 = MinScale("/gallery/Warrior.jpg", maxthumbx, maxthumby)
    image thumb16 = MinScale("/gallery/Winter Anime Guy.jpg", maxthumbx, maxthumby)
    image thumb17 = MinScale("/gallery/Wolf Boy.jpg", maxthumbx, maxthumby)


#image mc normal:
#    "mc normal.png"
#image side k normal:
#    LiveCrop((130,0,300,400), "mc normal")

image k normal:
    "k normal.png"
image side k normal:
    LiveCrop((130,0,300,400), "k normal")

image k happy:
    "k happy.png"
image side k happy:
    LiveCrop((130,0,300,400), "k happy")

image st normal:
    "st normal.png"
image side st normal:
    LiveCrop((175,0,300,400), "st normal")

#image sk normal:
#    "sk normal.png"
#image side sk normal:
#    LiveCrop((80,80,300,400), "sk normal")

image me normal:
    "me normal.png"
image side me normal:
    LiveCrop((80,80,300,400), "me normal")

#image ky normal:
#    "ky normal.png"
#image side ky normal:
#    LiveCrop((175,0,300,400), "ky normal")

#image a normal:
#    "a normal.png"
#image side a normal:
#    LiveCrop((175,0,300,400), "a normal")

image a2 normal:
    "a2 normal.png"
image side a2 normal:
    LiveCrop((120,0,300,400), "a2 normal")

#image a1 normal:
#  "a1 normal.png"
#image side a1 normal:
#    LiveCrop((175,0,300,400), "a1 normal")

image who normal:
     "who normal.png"
image side who normal:
    LiveCrop((120,0,300,400), "who normal")