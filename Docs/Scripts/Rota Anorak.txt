Rota Anorak


Resumo: Para conquistar o Anorak � preciso ter empatia com os problemas dele. Incorajar ele a se abrir contigo, aconselhar e fazer ele perceber que tudo bem ele sentir o que sente e ser quem ele �.

8 dias, 2 dates nos dias 6 e 7 (sendo o 8 dia o dia da festa), 2 finais (Bom (onde eles ficam juntos) e Ruim (onde eles continuam como amigos)

Roupas: Uniforme, uniforme sem faixa no rosto, roupa casual 1, roupa casual 2, roupa do baile, roupa vivendo junto com MC (pro final bom)

Rumo da rota: A MC vai ter coragem de chegar no Anorak pra conversar com ele, durante os primeiros 5 dias no col�gio. No dia 2 ela vai ter a op��o de abandonar a rota dele e escolher outra pessoa. No dia 5 ela chama ele pra sair, e dependendo de como a rota vai,
ele aceita ou n�o. Se n�o aceitar, vai pro final das amigas, onde elas v�o juntas. A Merrii diz que n�o vai poder ir com o rapaz que convidou ela. (MC, Merrii, Skelly e o Jorgin)
Se ele aceitar, o dia 6 vai ser um encontro e o dia 7 vai ser o segundo encontro (mais �ntimo e um quase-beijo talvez? >:3c)
O dia 8 vai ser o baile.

A hist�ria vai ser sobre o Anorak ter problemas pra lidar com o pai e ele vai ter que aprender a liderar o povo dele da maneira dele, a� vai questionar v�rias coisas nele, v�rias coisas sobre o pai e etc.

_________________



Anorak Route - Day 1

mc "Aquele menino que a Merrii mencionou ontem parece interessante"

mc "E eu preciso estudar pra prova, posso pegar o conte�do por l�."

"Eu pego a minha mochila e vou em dire��o � biblioteca, passando pelos corredores de sala de aula."

"~Biblioteca~"

"Procurando o assunto na estante, percebo que a cole��o � vasta demais. Vai demorar um tempo at� achar o que preciso..."

"N�o � r�gido demais uma escola ter estantes organizadas em ordem alfab�tica?..."

"Eu n�o teria tanta disposi��o pra organizar desse jeito. Talvez conseguisse com a ajuda das minhas amigas..."

"Ou ir�amos conversar o dia todo e deixar a bagun�a completamente de lado."

mc "N�o � hora de sair pensando em organiza��es de estante, [povname]. Eu preciso procurar aquele maldito livro para estudar ou estou encrencada no teste." 

a "Precisa de alguma ajuda?" 

"Uma voz sussurrada atr�s de mim me paralisou por alguns segundos. Pensei que estaria sozinha nesse corredor!"

"Eu falei comigo mesma em voz alta? Urgh! Eu preciso parar com esse costume ou as pessoas v�o achar que eu sou maluca."

"Me virei para encontrar a pessoa que me fez a pergunta. Um garoto segurando um livro de capa marrom e t�tulo escrito em detalhes dourados me encarava s�rio, esperando uma suposta resposta."

"Eu devo estar demorando demais para falar com ele, julgando pelo olhar penetrante vindo do mesmo."

mc "Uh- n-na verdade n�o. Mas obrigada pela ajuda..."

"Ele assentiu e se virou em dire��o � mesa perto de outras estantes, sentando em uma das cadeiras."

"E com a mesma express�o que me respondeu calado, abriu o livro direto na metade, como se estivesse marcando mentalmente a p�gina que havia parado."

"Coincidentemente, o assunto do livro do rapaz parecia ser o que eu precisava."

"Procurando por um livro parecido nas estantes, achei um semelhante. E abrindo aproximadamente na parte em que ele estava, me deparei com o assunto."

mc "Hmm..."

"Ele parecia concentrado no que estava lendo, como se fosse uma obriga��o ler aquilo. Talvez o assunto do teste dele n�o seja t�o interessante assim."

mc "Sobre o que � esse seu livro??"

"O menino deu um saltinho da cadeira e suas faixas se esticaram como se tamb�m tivessem se assustado"

"Ele fez um sinal de sil�ncio em frente a sua boca, um "shh" mudo com certa irrita��o."

a "Estamos em uma biblioteca."

mc "Desculpa, n�o quis te assustar."

"O garoto suspirou, desapontado com a minha atitude. Ele parece prezar muito pelo sil�ncio da biblioteca..."

"Eu consegui ouvir um risinho baixo vindo da dire��o dele, e ele pareceu virar de leve o rosto como se quisesse esconder o sorriso"

"Eu n�o poderia ver de qualquer forma, j� que ele � todo enfaixado."

"Mesmo assim... Foi bem fofo."

"Eu desviei a minha aten��o para o livro do meu vizinho de mesa. Meus olhos passeiam pelas letras min�sculas da p�gina grossa antes dele virar para a pr�xima p�gina."

"Ele n�o parece ligar para a minha presen�a, lendo cada palavra do livro com aten��o."

"A p�gina em quest�o tinha a foto de um besouro que parecia ter chifres."

"N�o estudamos na mesma classe, ent�o o professor dele deve estar dando uma mat�ria diferente. Nunca chegamos a ver esses besourinhos."

"Confusa e com os pensamentos vindo um atr�s do outro, n�o percebo quando ele deixa a aten��o do livro e vira para me encarar."

a "Escaravelhos."

mc "O-oi?"

"Eu me afasto um pouco segurando o meu livro com mais for�a nas m�os, fingindo estar interessada nele"

a "Escaravelhos. � um livro de entomologia."

mc "Anta-... O qu�?"

"Ele revirou os olhos, suspirando. Deve achar que eu n�o notei pelas bandagens em seu rosto."

a "Estudo de insetos."

mc "Oh... E voc� se interessa por isso?"

a "Um pouco. Preciso estudar isso avulso do conte�do das aulas."

mc "Mesmo? N�o est� preocupado com as provas?"

a "N�o muito... O conte�do do meu pai � "mais importante", segundo ele."

"Ele soltou mais um suspiro e trocou de p�gina com certo desd�m."

"Eu fixo o olhar nas palavras e figuras. O texto parou de ser sobre a parte anat�mica dos besouros, mudando para feiti�os e magias usando eles e a energia deles."

mc "Parece ser interessante..."

a "�. Mas o contexto nem tanto."

mc "Como assim?"

"O rapaz fecha o livro, voltando sua aten��o diretamente para mim."

a "Eu sou filho do fara� _. Sou o pr�ncipe herdeiro."

a "Ent�o tenho que estudar a cultura do meu povo- do meu reino, para poder governar."

a "Governar aos moldes do meu pai, que acha correto o que faz."

mc "E voc� n�o parece achar tanto..."

a "Sim e n�o. Acho o modo como ele rege antiquado demais."

mc "Em quest�o de tecnologia ou cultural?"

a "Ambos. Ele n�o aceita que os tempos s�o outros, e que uma m�o menos r�gida seria mais adequado."

mc "Hmm... Mas voc� pode mudar isso quando for rei, n�o?"

"Ele cruza os bra�os, pensativo."

"Parando pra analisar ele, por baixo dessas ataduras ele parece ser uma pessoa bem interessante"

"O cabelo azul � parcialmente escondido pelas faixas, e as orelhas levemente pontudas..."

"Os olhos vermelhos, fixos no livro, pensativo."

"Anorak � bem fofo. Espero que ele aceite ir comigo no baile."

"Sua voz sai um pouco baixa, atrapalhando os meus pensamentos e acabando com o meu mon�logo interno."

a "Eu n�o... Tenho certeza se quero ser governante."

mc "� uma decis�o bem importante..."

a "� sim. E eu n�o tenho maturidade pra pensar nisso ainda."

"Anorak suspira, voltando a prestar aten��o no livro."

a "Voc� deveria voltar a ler o seu livro. As provas s�o amanh�."

mc "Ah, a prova! N�o lembrava!"

a "Shh. Fale baixinho."

mc "S-sim..."

"Com o rosto corado por elevar minha voz novamente, eu abro o livro pra esconder a minha vergonha."

"Anorak sorri por baixo das faixas, focando em seu estudo. As ataduras escondem o sorriso, mas seus olhos n�o."

"Com o passar do tempo percebi alguns olhares em minha dire��o, vindas do rapaz ao meu lado. N�o sei dizer se eram para mim ou para o meu livro..."

"Espero que eu consiga achar coragem para chamar ele pro baile!"

"Eventualmente eu acabo me concentrando demais para trocar alguma palavra com ele"

"E assim ficamos at� a biblioteca fechar. Nos damos um breve "tchauzinho" e vamos cada um para a sua casa." 


#Anorak Route - Day 2 

"Hoje cheguei mais cedo no col�gio. Eu esperava que por ser dia de prova, os corredores ficassem mais cheios"

"Ent�o achei que seria uma boa id�ia revisar antes das aulas come�arem."

"Minha sala de aula � a melhor op��o. Alguns alunos costumam se desesperar e correr para a biblioteca, na esperan�a de que algum aluno com boas notas consiga os ensinar nos ultimos segundos restantes."

"O conte�do � bem b�sico, mas me impressionou que o rapaz de ontem estivesse t�o preso assim na leitura."

"Insetos. N�o � exatamente o que eu esperava estudar nesse semestre, mas o Stein disse que eles s�o de suma import�ncia para n�s."

"E o livro n�o � como os outros da biblioteca. N�o s�o folhas e mais folhas dizendo chatices."

"Eu acho que posso acabar gostando do assunto."

"Enquanto eu caminho pelo corredor eu lembro daquele daquele rapaz. Ele n�o � exatamente quieto, mas � bem misterioso."

"Quero muito que ele aceite ir comigo no baile..."

"� um improv�vel ele n�o ter par, por ser pr�ncipe. Ele j� deve ter algu�m para ir."

"Um esbarro no meu bra�o balan�a os livros das minhas m�os e quase os derruba, me fazendo perder completamente a linha de pensamento."

"Enquanto eu recobrava o equil�brio perdido ap�s a pessoa esbarrar em mim, ela se aproximava, fazendo um gesto cauteloso"

"Ele segura meu bra�o, ajudando a me manter em p� junto com os livros. Eu reconhe�o essa m�o"

"Meus olhos reconhecem algumas bandagens que cobrem o corpo da pessoa a minha frente"

"De todas as pessoas para passar por mim, justo ele."

"Justo quem eu queria ver agora."

a "Oh. Perd�o, n�o quis incomodar voc�."

"Vendo o qu�o pr�ximos estamos logo ele se recolhe, cruzando os bra�os"

"Nos encontramos novamente com ele me ajudando."

mc "N�o foi nada, n�o. Eu t� bem."

"Eu for�o um sorrisinho, escondendo o susto do baque repentino"

"Ele suspirou aliviado, fazendo parecer que quase me derrubou de um lugar alto."

a "�timo, isso � bom. Voc� deveria ter mais cuidado."

"Anorak sorri por baixo das suas bandagens. S� consigo ver seus olhos por tr�s delas, fazendo o sorriso se espelhar nos seus olhos fechados."

"Ele � bem fofo."

"Imagino o que teria atr�s dessas bandagens todas..."

"Anorak permanece im�vel, mas agora est� olhando para algo nas minhas m�os, observando."

"Acho que reconheceu o que eu estava lendo."

a "O livro que pegou ontem?"

"Ele me olha nos olhos, me fazendo tremer de leve."

"Ele n�o � nem um pouco amea�ador, mas quando me olha diretamente..."

"Os olhos vermelhos dele s�o penetrantes."

mc "S-sim... O teste � daqui a pouco."

a "Hm. Boa sorte, acho que vai precisar."

"Ele n�o parece ser de muita conversa. N�o � que ele tente ser seco, � s�..."

"Talvez n�o saiba como conversar?"

"Talvez seja t�mido?"

"Ele n�o parece me odiar."

mc "Obrigada. � bom um pouco de sorte, mas vai ser moleza. Entomologia � divertido de ler."

"Os olhos do rapaz brilharam ao ouvir o que eu disse. Parecia interessado pelo caminho que a conversa estava tomando."

a "Voc� n�o parecia conhecer muito de entomologia ontem. Esse assunto te interessa?"

mc "� fascinante conhecer os bichinhos que existem por a�."

mc "E escaravelhos s�o bonitinhos e intrigantes."

a "Escaravelhos s�o sagrados para a minha cultura..."

"Ele sorriu mais uma vez, tentando esconder os olhos fechados com o cabelo. Meu cora��o palpitou forte, e eu n�o pude deixar de esconder um rubor."

"Eu aperto um pouco mais o livro, tentando me tirar desse transe. Ele tem um ar de mist�rio t�o intrigante..."

a "Que tolice..."

"Meu rosto esquentou. Ele estava falando de mim? O que ele quis dizer?"

"Eu estar parada na sua frente n�o ajuda. Eu devo estar parecendo uma idiota."

"Ele balan�ou a cabe�a, levando uma das suas m�os para a parte de tr�s da sua cabe�a e co�ando de leve."

a "...Como pude esquecer de perguntar o seu nome novamente?"

mc "Ah. Era isso."

a "Isso?"

mc "N-nada. Eu pensei que voc� tinha me chamado de tola."

a "Voc� � tola."

mc "Ah."

"O sangue subiu t�o r�pido ao meu rosto, deixando ele completamente vermelho."

"Anorak solta um risinho baixo, estendendo a m�o para mim."

a "Me chamo Anorak."

"Eu assenti, retribuindo com um sorriso gentil, segurando em sua m�o."

mc "Eu sou a [povname]..."

a "� um prazer, [povname]. Te vejo depois das provas?"

mc "� verdade, o teste!"

mc "Preciso ir, de verdade!"

"Eu retomo meu caminho sem esperar por uma resposta. O Stein n�o ia me deixar entrar se chegasse atrasada."

"Eu me viro para tr�s, j� com uma certa dist�ncia do rapaz."

mc "Quem sabe n�o podemos conversar mais sobre os insetinhos algum outro dia?"

"Anorak arqueou uma sobrancelha, um leve vermelho surgiu em suas bochechas e a sua express�o fazia parecer curioso sobre o que falei."

a "Est� me convidando para alguma coisa?"

"...Realmente soou como um convite para um encontro."

"E � exatamente o que eu quero com ele. Quero convidar para o baile!"

mc "Me encontra no p�tio depois das provas. Talvez eu tenha te convidado mesmo."

a "Voc� n�o tem certeza, mesmo pedindo?"

mc "..."

"Eu desviei o olhar, me virando em dire��o ao meu corredor"

mc "N-n�o..."

"Ele riu, assentindo com a cabe�a"

a "Certo. At� mais tarde, N�bia."

"Anorak acenou e continuou o seu caminho."

"Ele me chamou de qu�? N-n�bia?"

"Deve significar alguma coisa na l�ngua dele. Espero que n�o seja algo ruim ou bobo..."

mc "A prova!"

"Correr para a sala � a minha �nica op��o agora. Stein j� deve estar entrando."

"Agora n�o tenho s� a prova para me preocupar..."

"Eu convidei mesmo ele pra sair!"

"Ele n�o me deu uma resposta concreta, ent�o vou ter que esperar ap�s as provas."

"Espero que seja positiva..."

"~Ap�s a prova :') ~"

"Sai da sala �s pressas. Mal consegui arrumar meu material e j� estava no corredor."

"Eu preciso saber da resposta dele. Preciso saber se ele n�o estava me fazendo de boba..."

"Corri at� o p�tio, passando por v�rias pessoas. Ele j� deve estar vindo."

"Sentado em um banco, observando as pessoas passarem. L� estava Anorak."

mc "Okay... Respira fundo e vai l�."

"Eu seguro minha bolsa com mais for�a, camnhando at� ele."

"Ele parecia em transe, perdido em pensamentos. N�o notou quando me aproximei, sentando em seu lado."

mc "Oi Anorak. Tudo bem?"

"Anorak se virou para mim, me olhando por um tempo antes de ter alguma rea��o."

a "Oh. [povname]! Voc� veio!"

mc "Heheh. Voc� estava distra�do."

a "Estava pensando enquanto esperava voc�..."

mc "Eu te fiz esperar muito?"

"Eu me ajeito no banco, encolhendo as pernas para perto uma da outra."

a "N�o. Na verdade nem vi o tempo passar. O que me fez ficar assim foi o s
eu pedido hoje mais cedo..."

mc "A-ah... Eu te chamei pra sair n�?..."

a "Aquilo foi sincero? Voc� quer mesmo sair comigo?"

mc "H-hmm... Digo, sim?"

"Ele se vira em minha dire��o e me encara nos olhos"

"Com os seus olhos sendo a �nica coisa aparente em seu rosto, eles ficam um pouco intimidadores de perto."

"Anorak parece estar procurando por alguma coisa no meu rosto. Parece desconfiado, ou com medo..."

"Ficamos assim por alguns segundos, at� ele desviar o olhar e puxar uma das faixas do seu bra�o direito."

a "Tudo bem. Se voc� quer... Podemos nos ver."

mc "Mesmo? Quando?"

"Eu esbo�o um sorriso, me apoiando com as m�os no banco e me aproximando dele"

"Anorak n�o consegue esconder o pulinho, evitando me olhar."

a "A-amanh�... Se estiver tudo bem pra voc�."

mc "Por mim �timo! Me d� seu n�mero e eu te mando uma mensagem mais tarde~" 

a "C-certo..."

"Ele dita os n�meros e eu salvo no meu celular"

"Durante todo o processo ele evitou ao m�ximo olhar para mim"

"Eu consigo ver o vermelho do rosto dele se expandir para as orelhas pontudas."

a "Eu... Eu preciso ir indo."

"Anorak se levanta, estendendo a m�o para mim. Seu olhar fixo em mim, ainda com as bochechas coradas."

mc "Oh... Obrigada."

"Eu pego sua m�o, levantando do banco."

"Ele sorri pra mim, levemente deixando minha m�o escapar da dele."

a "Te vejo amanh�, [povname]?"

mc "S-sim... Amanh�..."

"Eu sorrio de volta, vendo ele se dirigir at� a entrada do col�gio e desaparecer em uma charrete"

"Tenho a impress�o de que n�o vou conseguir dormir hoje."









 













