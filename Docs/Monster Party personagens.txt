Monster Party:

A MC tem uma semana para escolher um par pro baile. No total ser�o personagens 9 (Kurusu, Anorak, Hanael, Gomi, Damien, Stein, Doggo, Neko e Vamp) 


*queria fazer customizavel mas n�o sei se rola

*se n�o for custom, ela vai ser uma aranha >:> <3



Personagens:
_____________________
MC:

� a personagem principal. Ela � um pouquinho boba e acredita no melhor das pessoas. Ama seus amigos do fundo do cora��o.
Ela vai se graduar e precisa achar um par para o baile ~


Ficha: 
	Altura: 1.65m
	Anivers�rio: _
	Cor preferida: Roxo
	Hobbies: Gosta de doces e de escrever
_____________________
Kurusu:

� amigo de inf�ncia da MC. Reservado e t�mido. Tenta se mostrar sem interesses rom�nticos, embora tenha uma no��o de que tem sentimentos por ela. Nas rotas (fora a rota dele), ele � apenas amigo
e apoia ela sempre. 
Ele � filho da Dona Morte e herdou o poder dela. Embora a m�e tenha total controle, ele n�o consegue. Tudo o que toca morre ou dissolve, ele trata isso como uma maldi��o.
Tem medo de amar a MC tanto pela maldi��o da m�e dele, quanto por acabar com a amizade deles.
*� encontrado em todo o col�gio, em especial na sala de aula*
Ficha: 
	Altura: 1.85m
	Anivers�rio: 29/10 (Escorpi�o)
	Cor preferida: Roxo
	Hobbies: Dormir e ouvir m�sica	

_____________________

Anorak: 

� uma m�mia. Gosta de ler e geralmente s� fica na biblioteca da escola. N�o sabe como interagir, ent�o s� sorri e responde com a clareza que conseguir.
O pai dele, fara�, quer que ele herde o reino. Ent�o vive na cola dele, mandando ele estudar coisas que seriam uteis pra ele quando governar. Anorak n�o tem o m�nimo
de interesse nisso e tem problemas em se relacionar com o pai. Gosta muito da m�e dele, ela ensina ele a se vestir e usar acess�rios. O pai � rigoroso (gosta muito dele, s� se preocupa com o futuro do reino) e a m�e � querida com ele.
Quer se tornar amigo da MC porque ela � animada com os amigos, e ele gosta disso.
*� encontrado na biblioteca e na cantina*

Ficha: 
	Altura: 1.79m
	Anivers�rio: 20/03 (Peixes)
	Cor preferida: Azul turquesa
	Hobbies: Coletar acess�rios e coisas que brilham, ler
_____________________

Hanael: 

Ele � um anjinho. Os alunos do col�gio gostam dele e ele � f�cil de conversar. Sempre com um sorriso e pronto pra ajudar � todos.
Ama esportes. 
O pai dele � chefe dos arcanjos, e ele n�o acha que consegue chegar aos p�s do pai. Quer seguir o legado dele e ascender como entidade, mas n�o confia em si mesmo.
*� encontrado na cantina, no gin�sio e no telhado do col�gio*

Ficha: 
	Altura: 1.70 m
	Anivers�rio: 06/07 (C�ncer)
	Cor preferida: Cores past�is, rosa em especial
	Hobbies: Esportes em geral
_____________________

Damien: 

� um fantasma. Morreu salvando seu cachorro de se afogar. Damien � extremamente t�mido e medroso e se esconde quando fica com vergonha, ou para n�o notarem a presen�a dele. As vezes ele possui algu�m pra ver como o cachorro dele est�.
� colega da MC. Se abre com a MC por ela ser gentil e paciente com ele. 

*� encontrado na sala de aula e com sorte no telhado*

Ficha: 
	Altura: 1.87m
	Anivers�rio: 04/09 (Virgem)
	Cor preferida: Vermelho
	Hobbies: Observar as coisas vivas e colegas, passear pela cidade
_____________________

Gomi: 
� um Oni. Vive pregando pe�as nos outros (Hanael em especial, j� que ele odeia que o Gomi mencione o qu�o baixinho ele �) e geralmente vive de um lado pro outro da escola.
S� quer fazer amigos :(((
*� encontrado nos corredores, no telhado, no gin�sio, na cantina e as vezes na sala de aula*

Ficha: 
	Altura: 1.82m
	Anivers�rio: 14/05 (Touro)
	Cor preferida: Verde
	Hobbie: Pregar pe�as, incomodar, comer, sair pela cidade e jogar bolas na aur�ola do Hanael >:3c
_____________________

Inu: 
� dog, irm�o g�meo do neko. Gosta de conversar com todo mundo e sempre tem uma caixinha de doces caninos com ele.
N�o costuma estudar muito, e sempre incomoda o irm�o pra conseguir as anota��es da aula. Queria jogar no gin�sio com o irm�o, mas n�o � bom
em esportes e acaba se atrapalhando.
*� encontrado na cantina, telhado e gin�sio*

Ficha: 
	Altura: 1.75m
	Anivers�rio: 23/01 (Aqu�rio)
	Cor preferida: Branco
	Hobbies: Jogar conversa fora, doces e observar os treinamentos do col�gio
____________________
Neko: 
� cat, irm�o g�meo do doggo. � quietinho e gosta de correr. N�o gosta que o irm�o seja t�o meloso com ele, mas ama a comanhia dele (mesmo tendo problemas pra demonstrar isso).
� aplicado nos estudos porque n�o tem paci�ncia pra fazer recupera��es. Quanto mais cedo terminar a tarefa, melhor pra ele.
Cozinha para o irm�o porque sabe que ele n�o tem capacidade de se cuidar sozinho. Al�m de amar ver ele feliz.
*� encontrado na cantina, biblioteca e no gin�sio*

Ficha: 
	Altura: 1.75m
	Anivers�rio: 23/01 (Aqu�rio)
	Cor preferida: Preto
	Hobbies: Praticar no time de basquete, correr e cozinhar
____________________
Vamp:
Umx vamp. Elu gosta do fato de ser nonbinary e t� sempre pelos corredores, conversando com os amigos. � goth e sai de noite pra rol�s. Usa uma capa com gorro, por n�o poder pegar sol.
� gentil e animado, flerta e acaba sempre de cora��o quebrado.
Se aproxima da MC por esbarrar nela um dia e as amigas serem legais com elu.
*� encontrado no corredor e na cantina*

Ficha: 
	Altura: 1.74m
	Anivers�rio: 11/06
	Cor preferida: Vinho
	Hobbies: Roupas, sair pra shopping e usar a internet


___________________
Stein:

Professor Frankenstein. Vive perdendo partes do corpo e isso � c�mico e aterrorizante pra alguns. � professor da MC
� gentil e compreens�vel com todos os alunos, embora a maioria ache ele estranho demais.
Carrega balinhas de a��car pra ajudar ele a se manter ativo. Usa a internet pra se manter em contato com a cultura jovem dos alunos.
*Encontrado na cantina, na staff room e na sala de aula*

Ficha: 
	Altura: 2.06m
	Anivers�rio: 12/12 (Sagit�rio)
	Cor preferida: Cinza
	Hobbies: Balinhas de a��car, estudar insetos pequenos
___________________
Merrii:
� uma ovelha. � uma das melhores amigas da MC junto com a Skelly e � gentil. Por ser t�mida, as vezes as pessoas se aproveitam da falta de iniciativa dela,
e a Skelly ajuda a espantar. 

Ficha: 
	Altura: 1.60m
	Anivers�rio: 25/03 (�ries) 
	Cor preferida: Rosa e roxo
	Hobbies: Cozinhar e dan�ar
____________________
Skelly:
� uma esqueleto. � uma das melhores amigas da MC junto com a Merrii e � porra louca. N�o aceita desaforo e se esfor�a nas aulas, mesmo tendo dificuldade em algumas mat�rias.
Namora Kyte desde que entrou na escola. 

Ficha: 
	Altura: 1.78m
	Anivers�rio: 02/08 (Le�o)
	Cor preferida: Laranja
	Hobbies: Jogar com o namorado, ir em shows e passear no shopping
____________________
Kyte:
� um zumbi. Namorado da Skelly, � amigo da MC junto com o Kurusu. � paz e amor e t� sempre na dele. N�o liga de falhar nas provas, e sempre d� um jeito de se virar nas recupera��es.
Pretende pedir Skelly em casamento no baile (awn <3)

Ficha: 
	Altura: 1.80m
	Anivers�rio: 31/12
	Cor preferida: Capric�rnio
	Hobbies: Jogar, ouvir m�sica, estar em contato com a natureza

___________________

Vampirinha com anemia ahnsjfroahsduoiga >:)c





